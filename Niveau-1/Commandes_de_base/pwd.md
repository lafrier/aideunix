
## Commande `pwd`

### Fonction

`pwd` pour *present working directory*.
Cette commande donnera le *chemin absolu* du *Répertoire courant* c'est à dire celui sous lequel on se situe sur l'arbre. 

### Notion de Chemin absolu

Il s'agit du trajet parcouru sur l'arbre, depuis le départ de l'arbre jusqu'au répertoire où on est actuellement.
*Un répertoire est caractérisé sur un arbre par son chemin absolu*, c'est sa "carte d'identité"

Supposons qu'on appelle Racine le *répertoire* d'où on *part* sur l'arbre. Ce *répertoire* a un sous-répertoire `Rep1` qui
lui-même a un sous-répertoire `Rep2`

Si, à l'aide de commandes à voir plus loin, on est parti de `Racine` et arrivé à `Rep2`, la commande pwd donnera:
    /...Racine/Rep1/Rep2

(`/` est le véritable départ de l'arbre, les 3 points suivants représentent des répertoires sans intéret pour le moment)

`/...Racine/Rep 1/Rep 2/` est *le chemin absolu* du Répertoire sous lequel on se trouve c'est à dire `Rep2`.

Ce Répertoire est le *répertoire courant*, `Rep2` dans l'exemple précédent.

### Symboles du répertoire courant, de son antécédent immédiat puis de l'antécédent de l'antécédent précédent etc

En reprenant l'exemple pécédent: 

Le répertoire courant, ici `Rep2`, est représenté par un `.`. Si on doit écrire un autre répertoire après on mettra un `/`

Le répertoire directement immédiat à `Rep2`, ici `Rep1`, est représenté par `..`. Comme ci-dessus si on doit écrire un autre répertoire après on mettra un `/`.

Le répertoire directement immédiat à `Rep1`, ici `Racine`, est représenté par `../../`  
	
On verra des exemples d'emploi de ces symboles dans des exemples d'écritue de chemins plus loin.

*Note*: On écrit un `/` entre 2 répertoires

<noman> Dans la figure \ref{fig:essaipwd}, on voit un exemple d'arbre.

<noman> ![Exemple d'arbre\label{fig:essaipwd}]($AIDEUNIX/Niveau-1/Figures/essaipwd.jpg)

### Utilisation

La commande `pwd` s'utilise seule.



