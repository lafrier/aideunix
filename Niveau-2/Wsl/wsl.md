# Name

wsl

# Installation

## Vérification

Sous windows 10 appuyer sur la touche windows + R et saisir `winver` puis `OK`. Vérifier que la version est 2004 ou supérieure, avec un build 19041 ou supérieur.

## Préparation de `powershell`

Sous windows 10, dans la fenêtre de recherche, tapper "powershell".

En premier, on trouve le logiciel "Windows PowerShell".

Cliquer dessus avec le bouton de droite et sélectionner "Epingler à la barre des tâches".

Sur la barre des tâches apparaît l'icone de PowerShell. Cliquer dessus avec le bouton de droite et cliquer encore avec le bouton de droite sur "Windows PowerShell".

Cliquer sur "Propriétés" puis sur "Avancé".

Cocher la case "Exécuter en tant qu'administrateur".

Quitter ces fenêtre en répondant "OK" à chaque fois que la question est posée.

Cliquer sur l'icone pour lancer PowerShell et accepter de travailler avec les droits d'administrateur.

## Installation de `wsl 1`

Sous PowerShell, lancer la commande suivante :

    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

## Mettre à jour la dernière version de windows

On utilise le menu paramètres, on fait la mise à jour et on redémarre.

## Activer la machine virtuelle

    dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart


## Mettre à jour le noyau

Télécharger le fichier suivant

    https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

et l'exécuter.

## Définir WSL 2 comme version par défaut

    wsl --set-default-version 2

## Aller chercher ubuntu

Se placer sur le disque voulu, dans le répertoire voulu, aller chercher l'archive, la renommer et l'extraire :

    cd D:
    Invoke-WebRequest -Uri https://aka.ms/wslubuntu2004 -OutFile Ubuntu.appx -UseBasicParsing
    move .\Ubuntu.appx .\Ubuntu2004.zip
    Expand-Archive .\Ubuntu2004.zip

## Initialiser ubuntu

Avec le File Explorer, aller dans Ubuntu2004 et executer ubuntu2004.exe

Mettre à jour

    sudo apt-get update
    sudo apt-get upgrade
    sudo apt install vim
    sudo apt install vim-gtk
    sudo apt install terminator

## Serveur graphique

Sous windows, télécharger VcXsrv (https://sourceforge.net/projects/vcxsrv/) et l'installer.

Exécuter XLaunch depuis la barre des taches, puis sélectionner "One large window" et mettre 0 comme display number. Laisser tout le reste par défaut et sauver la configuration avant de sortir.

Définir le serveur sous ubuntu :

    lafrier$  nano ~/.bashrc
    nano:
      #export DISPLAY=localhost:0.0
      #export DISPLAY=$(grep -oP "(?<=nameserver ).+" /etc/resolv.conf):0
      export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
      export LIBGL_ALWAYS_INDIRECT=1

















Dans `/usr/share/dbus-1/session.conf`, remplacer la ligne

    <listen>unix:tmpdir=/tmp</listen>

par

    <listen>tcp:host=localhost,port=0</listen>

soit

    lafrier$  sudo cp /usr/share/dbus-1/session.conf /usr/share/dbus-1/session.conf.orig
    lafrier$  sudo nano /usr/share/dbus-1/session.conf
    nano:
      <listen>tcp:host=localhost,port=0</listen>

# Ubuntu

Sous Windows Store, aller chercher la dernière version d'ubuntu et l'installer.

Chercher "ubuntu" avec la fenêtre de recherche.

Cliquer dessus avec le bouton de droite et sélectionner "Epingler à la barre des tâches".

Pour lancer ubuntu, avec la barre des tâches, lancer d'abord PowerShell puis lancer Ubuntu.

La première fois donner un nom d'utilisateur et un mot de passe.

Les documents de windows sont sous /mnt/c





# Linux sous Windows

Passer en mode développeur

Sous "activer les foncontinnalité Windows" cocher "sous-système Windows pour Linux"

Lancer PowerShell et saisir

bash

Cela indique l'adresse https://aka.ms/wslstore

Y aller et installer ce que l'on veut (e.g. Debian)

# Utilisation

Lancer PowerShell et saisir

bash

# Installation de base

sudo apt-get update

sudo apt-get install xorg mesa-utils task-desktop task-french-desktop

sur le windows installer xming

sur le windows lancer xlaunch

export DISPLAY=:0

et par exemple :

xterm

adresse typique (pour le disque C: de windows) :

/mnt/c

