# Language markdown

Ecriture. Plus simple que latex.

# Exemples

## Code source dans le texte

Entre accents graves.

`toto_tata.sh`

## Code source à la ligne

Début de ligne après 4 blancs

    bash toto_tata.sh

## Passer à a ligne

Dans les itemize emboités, on ne peut pas sauter une ligne.

Dans ce cas, rajouter 2 blancs en fin de ligne.

## Listes

1. First ordered list item
2. Another item
  * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.
