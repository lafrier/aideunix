
## Commande `ls`

### Fonction

`ls` pour "list".
La commande `ls` liste le contenu des *répertoires* 

### Utilisation

On indique le *chemin relatif* du répertoire choisi.

- cas 1: *Lister le répertoire courant* (celui qui serait donné par la commande `pwd`)

Supposons que le répertoire courant soit *Jardinage*

    - `ls` donnera la liste des fichiers et sous-répertoires de `Jardinage`

    - `ls -l` donnera des informations sur chaque élément de ce répertoire, la liste des fichiers et celles des sous-répertoires sera plus claire.

    - `ls -lh` donnera la "taille" des fichiers et sous-répertoires de `Jardinage`

- cas 2: *Lister un répertoire distant*

Supposons que le *chemin relatif* du répertoire choisi soit `../../Documents/toto` les 3 commandes précédentes deviennent:

    - `ls ../../Documents/toto`

    - `ls -l ../../Documents/toto`
  
    - `ls -lh ../../Documents/toto`

*Remarque* : Dans l'écriture des commandes `ls -l` et `ls -lh` il y a 2 "blancs", l'un entre `ls` et `-l` (ou `-lh`), l'autre avant le chemin relatif.

