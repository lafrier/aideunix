# NOM

transmission

# Description

Outil d'échange P2P utilisant le protocole bittorent.

# Utilisation

Recopier le fichier .torrent dans le répertoire `/var/lib/transmission/config/torrents` :

    root#  cp <adresse>/<nom>.torrent /var/lib/transmission/config/torrents/.

Lancer le daemon :

    root#  rc-service transmission-daemon start

Suivre le téléchargement sur l'interface `http://localhost:9091/transmission/web/`

Arrêter le daemon en fin de téléchargement :

    root#  rc-service transmission-daemon stop

# Les magnets

Copier le lien, par exemple :

    magnet:?xt=urn:btih:e15450af36d3aa823e5bbfb400df05ddd642c540&dn=The Pillow Book 1996 1080p BluRay FLAC x264-PublicHD&tr=udp://retracker.hotplug.ru:2710/announce&tr=udp://tracker.birkenwald.de:6969/announce&tr=http://vps02.net.orel.ru:80/announce&tr=https://tracker.nanoha.org:443/announce&tr=http://tracker.files.fm:6969/announce&tr=https://tracker6.lelux.fi:443/announce&tr=udp://tracker.beeimg.com:6969/announce&tr=udp://opentracker.i2p.rocks:6969/announce&tr=http://t.overflow.biz:6969/announce&tr=https://tracker.nitrix.me:443/announce&tr=https://tracker.tamersunion.org:443/announce&tr=udp://tracker2.dler.org:80/announce&tr=https://tracker.imgoingto.icu:443/announce&tr=udp://blokas.io:6969/announce&tr=udp://discord.heihachi.pw:6969/announce&tr=udp://fe.dealclub.de:6969/announce&tr=udp://ln.mtahost.co:6969/announce&tr=udp://vibe.community:6969/announce&tr=udp://engplus.ru:6969/announce&tr=udp://mail.realliferpg.de:6969/announce&tr=udp://movies.zsw.ca:6969/announce&tr=udp://mts.tvbit.co:6969/announce&tr=udp://tracker.v6speed.org:6969/announce&tr=udp://sd-161673.dedibox.fr:6969/announce&tr=udp://47.ip-51-68-199.eu:6969/announce&tr=udp://daveking.com:6969/announce&tr=http://rt.tace.ru:80/announce&tr=udp://code2chicken.nl:6969/announce&tr=udp://us-tracker.publictracker.xyz:6969/announce

Ensuite le recopier après la commande `transmission-remote -a `

    root#  transmission-remote -a magnet:?xt=urn:btih:e15450af36d3aa823e5bbfb400df05ddd642c540&dn=The Pillow Book 1996 1080p BluRay FLAC x264-PublicHD&tr=udp://retracker.hotplug.ru:2710/announce&tr=udp://tracker.birkenwald.de:6969/announce&tr=http://vps02.net.orel.ru:80/announce&tr=https://tracker.nanoha.org:443/announce&tr=http://tracker.files.fm:6969/announce&tr=https://tracker6.lelux.fi:443/announce&tr=udp://tracker.beeimg.com:6969/announce&tr=udp://opentracker.i2p.rocks:6969/announce&tr=http://t.overflow.biz:6969/announce&tr=https://tracker.nitrix.me:443/announce&tr=https://tracker.tamersunion.org:443/announce&tr=udp://tracker2.dler.org:80/announce&tr=https://tracker.imgoingto.icu:443/announce&tr=udp://blokas.io:6969/announce&tr=udp://discord.heihachi.pw:6969/announce&tr=udp://fe.dealclub.de:6969/announce&tr=udp://ln.mtahost.co:6969/announce&tr=udp://vibe.community:6969/announce&tr=udp://engplus.ru:6969/announce&tr=udp://mail.realliferpg.de:6969/announce&tr=udp://movies.zsw.ca:6969/announce&tr=udp://mts.tvbit.co:6969/announce&tr=udp://tracker.v6speed.org:6969/announce&tr=udp://sd-161673.dedibox.fr:6969/announce&tr=udp://47.ip-51-68-199.eu:6969/announce&tr=udp://daveking.com:6969/announce&tr=http://rt.tace.ru:80/announce&tr=udp://code2chicken.nl:6969/announce&tr=udp://us-tracker.publictracker.xyz:6969/announce
