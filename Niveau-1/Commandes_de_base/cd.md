
## Commande `cd`

### Fonction

`cd` pour "change directory".
La commande `cd` permet de *changer de répertoire*

### Utilisation

On se déplace sur l'arbre jusqu'au répertoire voulu *en suivant un chemin continu* depuis le répertoire sous lequel on est (donné par la commande `pwd`).
Ce *chemin doit être écrit après la commande `cd`* s'il est nécessaire de l'écrire.

En reprenant l'exemple choisi dans le texte sur la commande `pwd`, le chemin du répertoire courant est `/...Racine/Rep1/Rep2/`
(*Rappel* : les 3 points après le `/` de départ représentent des répertoires dont on ne parle parle pas ici).

Dans les *cas suivants*, on se situe sous le Répertoire courant `Rep2`

- *cas 1*: on veut aller dans un sous-répertoire `Rep3` de `Rep2è puis de là dans un sous répertoire `Rep4`de `Rep3`
L'écriture de `cd` sera `cd Rep3/Rep4/`.

- *cas 2*: On veut atteindre un autre sous-répertoire `Repa` de `Rep1` puis de `Repa` on veut atteindre `Repb` sous-répertoire de `Repa`.
En partant de `Rep2` *on doit revenir* à `Rep1` pour rétablir le chemin. L'écriture de `cd` sera donc : `cd ../Repa/Repb/`
(`../` est l'écriture d'une remontée de 1 niveau).

De même, si on veut atteindre un autre sous-répertoire `RepA` de `Racine` puis un sous-répertoire `RepB` de `RepA` en partant toujours de `Rep2`, on doit remonter
de 2 niveaux pour arriver sous `Racine` et continuer le chemin pour se trouver sous `RepA` puis sous `RepB`. L'écriture de `cd` sera `cd ../../RepA/RepB/`

### Chemin relatif d'un répertoire

Dans les cas étudiés dans le paragraphe précédent, les chemins qu'on a dû écrire sont appelés des *chemins relatifs*, le premier est celui de `Rep4`,
le deuxième celui de `Repb`, le troisième celui de `RepB`.

Le chemin relatif d'un répertoire dépend donc de l'endroit où on se trouve sur l'arbre. Il est établi *à partir du répertoire courant* (donné par `pwd`).
*Ce chemin relatif est variable*.

<noman> ![Arborescence correspondant aux chemins décrits dans le cas 1 et le cas 2]($AIDEUNIX/Niveau-1/Figures/cd-cheminrelatif.jpg)

