#!/bin/bash

#passphrase pour chiffrer. Il existe aussi des options pour chiffrer avec une clé ssh voir le man
# il est possible aussi de ne pas chiffer avec l'option --no-encryption
export PASSPHRASE=passphrase

#commande pour la sauvegarde vers le nastar
# pré-réquis: il faut l'agent ssh avec la clé ssh vers le nastar dispo
# on sauvegarde le répertoir /home/piaud  vers /volume1/users/piaud/backup sur le nastar avec le protocole scp
# --exclude "**" signifie qu'on exclue tout les répertoires
# --include '/home/piaud/Documents' siginifie qu'on ne sauvegarde que ce répertoire
duplicity --include '/home/piaud/Documents' --exclude "**" /home/piaud/  scp://piaud@130.120.98.151//volume1/users/piaud/backup

# probablament que la commande suivante suffirait :
# duplicity /home/piaud/Documents scp://piaud@130.120.98.151//volume1/users/piaud/backup
# mais c'était pour avoir un exemple avec include/exclude

echo -n "Backup done " >> /home/piaud/Documents/duplicity/log.txt
date >> /home/piaud/Documents/duplicity/log.txt


# Pour lister les fichiers
# duplicity list-current-files scp://piaud@130.120.98.151//volume1/users/piaud/backup 

# Pour restorer un fichier
# duplicity --file-to-restore file_src scp://piaud@130.120.98.151//volume1/users/piaud/backup file_dst
