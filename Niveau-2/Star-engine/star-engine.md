
## Nom

Star-Engine

## Description

Star-Engine est un environement de développement pour des ingénieurs et des chercheurs qui souhaitent faire appel à la méthode de Monte Carlo, notament face à des problèmes de simulation numérique difficiles en termes de couplage phénoménologique et de description géométrique.

## Avant l'installation

Il est bon d'avoir d'abord installé `git`, `git-lfs`, `gcc`, `cmake`, `netcdf`, `openmpi` et `asciidoc`.

## Installation de la bibliothèque

On peut télécharger les sources à partir du site de Méso-Star (`https://www.meso-star.com/projects/star-engine/star-engine.html`). Typiquement, vous pouvez les installer sous `/opt` (mais vous pouvez aussi les installer sous un compte utilisateurs, e.g. `/home/lafrier`) :

    /opt/Star-Engine-0.9.0-Sources
    /opt/Star-Engine-0.9.0-GNU-Linux64

## Installer un projet utilisant le Star-Engine

On choisi l'exemple du projet "phare" de Star-Engine, `star-4v_s` : un tout petit programme permettant de calculer par Monte Carlo un invariant de la physique du transport, dont on connaît la solution exacte même en géométrie complexe. On teste donc, en peu de lignes, beaucoup d'éléments essentiels du Star-Engine.

Admettons que l'on souhaite installer `star-4v_s` sous le répertoire `/home/lafrier/simulations` :

    lafrier$  cd /home/lafrier/simulations
    lafrier$  git clone https://gitlab.com/meso-star/star-4v_s.git
    lafrier$  cd star-4v_s
    lafrier$  mkdir build
    lafrier$  cd build
    lafrier$  cmake -DCMAKE_PREFIX_PATH=/opt/Star-Engine-0.9.0-GNU-Linux64/ ../cmake/
    lafrier$  make

A ce stade `star-4v_s` est installé et compilé. Pour l'utiliser, il faut une géométrie. On va chercher le Stanford bunny :

    lafrier$  wget http://www.prinmath.com/csci5229/OBJ/bunny.zip
    lafrier$  unzip bunny.zip
    lafrier$  rm bunny.zip
    lafrier$  ./s4vs ~/Gros-volumes-local/Geometries/bunny.obj 10000

Vous devriez obtenir le résultat suivant

    obj 10000
    Obj loaded in 94 msecs 405 usecs 363 nsecs.
    Computation time: 5 msecs 380 usecs 216 nsecs
    4V/S = 1.03775 ~ 1.0358 +/- 0.00692039
    #failures = 0/10000

## Installer `htrdr`

A travers le projet High-Tune, on peut installer `htrdr` qui est un exemple d'utilisation du Star-Engine beaucoup plus évolué que `star-4v_s`. Il s'agit de synthétiser une image d'un champ nuageux issu d'une simulation numérique de type LES. Admettons que l'on souhaite installer `htrdr` sous le répertoire `/home/lafrier/simulations` :

    lafrier$  cd /home/lafrier/simulations
    lafrier$  git clone -b High-Tune-0.3.0 https://gitlab.com/meso-star/star-engine.git High-Tune-0.3.0
    lafrier$  cd High-Tune-0.3.0
    lafrier$  mkdir build
    lafrier$  cd build
    lafrier$  cmake ../cmake
    lafrier$  make

Attention ! Pour executer `htrdr`, il faut d'abord régler les variables d'environnement de votre shell courant. Cela se fait avec la commande

    lafrier$  source /home/lafrier/simulations/High-Tune-0.3.0/local/etc/high_tune.profile

Ensuite on peut lancer une première exécution (qui ne renverra que des informations sur l'utilisation de `htrdr`) :

    lafrier$  /home/lafrier/simulations/High-Tune-0.3.0/build/htrdr -v

Pour ce projet, il y a en fait quatre executables (et pas seulement `htrdr`). On peut consulter leurs manuels avec les commandes `man` associées :

    lafrier$  man htrdr
    lafrier$  htrdr-image
    lafrier$  htcp
    lafrier$  htmie

Pour pouvoir tester réellement `htrdr`, il faut des données nuageuse. On peut en trouver de la façon suivante (ici on les installe sous `/home/lafrier/Gros-Volumes`) :

    lafrier$  cd /home/lafrier/Gros-Volumes
    lafrier$  wget https://www.meso-star.com/projects/high-tune/downloads/High-Tune-Starter-Pack-0.2.0.tar.gz
    lafrier$  tar -zxvf High-Tune-Starter-Pack-0.2.0.tar.gz
    lafrier$  rm High-Tune-Starter-Pack-0.2.0.tar.gz

On peut alors faire le test suivant (qui prend beaucoup de temps !) :

    lafrier$  cd /home/lafrier/Gros-Volumes/High-Tune-Starter-Pack-0.2.0
    lafrier$  bash ht-run.sh scenes/DZVAR2

Ce calcul produit le fichier `DZVAR2_1280x720x256.txt`. On peut le traduire au format `ppm`

    lafrier$  htpp -o DZVAR2.ppm DZVAR2_1280x720x256.txt

et le visualiser, par exemple avec `feh` :

    lafrier$  feh DZVAR2.ppm


## Installer Stardis




## La suite c'est de vielles notes ...




On peut les placer 

git clone git@gitlab.com:meso-star/star-engine.git

cd star-engine

mkdir build

cd build

cmake ../cmake

make

source ../local/etc/star-engine.profile

### Installation de star-4v_s pour tester

wget https://gitlab.com/meso-star/star-4v_s

cd star-4v_s

mkdir build

cd build

cmake -DCMAKE_PREFIX_PATH=/home/etudiant/star-engine/local ../cmake

source ../../star-engine/local/etc/star-engine.profile

make

./s4vs bunny.obj 10000

### La bibliothèque

#### Normalisation des vecteurs direction

Toutes les fonctions et structures renvoient des vecteurs direction unitaires, _SAUF_ s3d_scene_view_trace_ray qui renvoie un hit dont la normale, hit.normal, n'est pas uniaire. 

### Les raccourcis

f3_normalize(vecteur_unitaire, vecteur_donné_en_entrée)

### Le debug

#### Exemple

cgdb ./spheres

run 1 geom.obj path.obj

up

up

up

print *dir@3

breakpoint 2292

run

step

next

### Lors de l'installation

cmake ../cmake/ -DCMAKE_PREFIX_PATH=/home/lafrier/Transfert-local/Cora/EDSTAR/Star-Engine-0.6.0-GNU-Linux64

mkdir build ; cd build

make

### Pour stardis-solver

Créer un nouvel exemple (nouvelle géométrie, nouvelles données)

cp test_sdis_solve_probe3.c test_sdis_solve_probe_new.c

emacs ../cmake/CMakeLists.txt &

dupliquer toutes les lignes concernant test_sdis_solve_probe3.c avec test_sdis_solve_probe_new.c

cd ../build

cmake --build .

### Autres

source /home/lafrier/Transfert-local/Cora/EDSTAR/star-engine-0.5.1/star-engine/src/star-engine.profile

source /home/lafrier/Transfert-local/Cora/EDSTAR/star-engine-premier-4v_s/star-engine/src/star-engine.profile

git log --pretty=full --decorate=full

export LD_LIBRARY_PATH=/home/lafrier/Transfert-local/Cora/EDSTAR/star-engine-0.5.1/star-engine/local/lib:$LD_LIBRARY_PATH

gcc ../src/s4vs.c ../src/s4vs_realization.c -I/home/lafrier/Transfert-local/Cora/EDSTAR/star-engine-0.5.1/star-engine/local/include -L/home/lafrier/Transfert-local/Cora/EDSTAR/star-engine-0.5.1/star-engine/local/lib -lrsys -lm -ls3d -lsmc -lssp -ls3daw

ldd a.out



