# NAME

rclone

# CONFIGURATION

lafrier$  rclone config

n/s/q> n

name> drivelaplace

Storage> 13

client_id>

client_secret>

scope> 1

root_folder_id>

service_account_file>

y/n> n

y/n> y

y/n> n

y/e/d> y

e/n/d/r/c/s/q> q

# LISTER

## Lister les répertoires sur le cloud

rclone lsd drivelaplace:

## Lister tous les fichiers sur le cloud (ou dans un répertoire)

rclone ls drivelaplace:

rclone ls drivelaplace:Partage

# SYNCHRONISER

## du cloud vers le local

! Le répertoire local ./Partage sera effacé et remplacé par drivelaplace:Partage

rclone sync drivelaplace:Partage/ ./Partage/

## du local vers le cloud

! Le répertoire drivelaplace:Partage sur le cloud sera effacé et remplacé par ./Partage

rclone sync ./Partage/ drivelaplace:Partage/




