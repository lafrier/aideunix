#!/bin/bash
cat  ${@:2:$#} | sed -e '/solid/d' > $1.stl
echo "solid $1" | cat - $1.stl > _tmp.stl && mv _tmp.stl $1.stl
echo "endsolid $1" >> $1.stl
