// On choisit le OpenCASCADE comme kernel geometrique
SetFactory("OpenCASCADE");

//on inclut le fichier où sont définis les macro CreateMedium, CreateBoundary et 
// CreateMesh. Ces macros permettent de générer un stl par shape et un fichier
// stl par face externe ce qui permettra de décrire les conditions aux limites.
// Les fichiers stl ainsi générés sont "conformes" au sens où 2 shapes qui
// partagent une interface seront maillées de façon strictement identiques.
// Les macros n'ont pas d'arguments, les variables sont donc globales.
// Ces macros nécessitent que 2 variables soient définies : 
//      - SHAPES_LIST qui est la listes des shapes (volumes ou solides) de bases.
//      - PREFIX_STL qui sera le préfix des noms de fichiers stl générés.

Include "star-stlmeshing.geo";

// Exemple de 3 boites collés et un cylindre qui seront les shapes de base.
Box(1) = {0, 0, 0, 1, 1, 1};
Box(2) = {0, 0, 1, 1, 1, 1};
Box(3) = {0.25, 1, 0.25, 0.5, 0.5, 0.5};
Cylinder(4) = {-1, 0.5, 0.5, 1, 0, 0, 0.25, 2*Pi};

// On définit la SHAPES_LIST
SHAPES_LIST[] = {1, 2, 3, 4};

// On définit le préfix des fichiers stl
PREFIX_STL = "cubes_cylindre";

// La macro CreateMedium fait une fragmentation booléenne des shapes de base.
// Lorsqu'elles se touchent, cela permet de marquer l'empreinte d'une shape sur 
// l'autre. C'est ce qui permet de faire des maillages "conformes" lorsque des
// shapes se touchent.
Call CreateMedium;

// La macro CreateBoundary extrait toute les "faces" du contour du modèle
// globale. C'est un préalable pour obtenir un fichier stl par face externe.
Call CreateBoundary;

// Cette macro maille les mediums créé par CreateMedium et les faces externes
// créées par CreateBoundary. 
// Les maillages des mediums sont sauvés avec le nom :
// PREFIX_STL_Medium**.stl
// Les maillages des faces externes (ou frontières) sont sauvés avec le nom :
// PREFIX_STL_Bound**.stl
// Cette macro nécessite le programme admesh qui "mouline" les fichiers stl pour
// réorienter les triangles comme il faut.
// Il faut également csplit car gmsh créé un seul maillage pour toute la scène. 
// Mais dans le format stl il y la notion de groupe (solid ...endsolid). csplit
// permet de spliter le stl globale pour obtenir un fichier par mediums et par
// faces externes.
Call CreateMesh;
