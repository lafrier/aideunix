# `gmsh`

Outil de saisie de géométrie, puis de maillage.

# Utilisation à partir de scripts

## Le moteur géométrique

Pour les noms des scripts, on utilise l'extension `.geo` et la première ligne d'un script définit le moteur géométrique. On utilise `opencascade` :

    lafrier$  nano exemple.geo
    nano:
      SetFactory("OpenCASCADE");

## Quelques géométries simples

Attention ! Il est important qu'il n'y ait pas de point de coordonnée `(0,0,0)` car par la suite ces `0` peuvent être remplacés par `4.9984e-15` (et de façon non systématique), ce qui posent problème pour certaines utilisations (notamment dans `stardis`). 

### Box

    lafrier$  nano exemple.geo
    nano:
      Box(1) = {0, 0, 0, 1, 1, 1};



# Utilisation de l'interface

# Anciennes notes (voir le répertoire `old`)

## Une surface

Créer un fichier de géométrie, `test-1.geo` :

    nano test-1.geo
    nano:
    lc = 1.e-0;
    Point(1) = {0, 0, 0, lc};
    Point(2) = {.1, 0,  0, lc};
    Point(3) = {.1, .3, 0, lc};
    Point(4) = {0,  .3, 0, lc};
    Line(1) = {1, 2};
    Line(2) = {3, 2};
    Line(3) = {3, 4};
    Line(4) = {4, 1};
    Curve Loop(1) = {4, 1, -2, 3};
    Plane Surface(1) = {1};

La grandeur `lc` donne la longueur caracteristique du maillage. Ici elle est grande devant la surface donc il y aura peu de mailles.

Pour fabriquer un fichier `test-1.stl` à partir de ce fichier de géométrie :

    gmsh test-1.geo -2 -o test-1.stl

## Translater une surface

    nano test-1.geo
    nano:
    t1 = {0.5, 0, 0};
    p5 = Translate {t1[0], t1[1], t1[2]} { Duplicata{ Point{1}; } };
    p6 = Translate {t1[0], t1[1], t1[2]} { Duplicata{ Point{2}; } };
    p7 = Translate {t1[0], t1[1], t1[2]} { Duplicata{ Point{3}; } };
    p8 = Translate {t1[0], t1[1], t1[2]} { Duplicata{ Point{4}; } };
    Line(5) = {p5, p6};
    Line(6) = {p7, p6};
    Line(7) = {p7, p8};
    Line(8) = {p8, p5};
    Curve Loop(2) = {p8, p5, -p6, p7};
    Plane Surface(2) = {2};

## Lire les informations sur les points

    Point(1) = {1,2,3,1};
    c[] = Point{1};

Le vecteur `c` contient les coordonnées x, y, z du point.
## Voir les normales

Dans `Tools->Option->Geometry->Visibility` mettre `100` dans la case `normal`.
