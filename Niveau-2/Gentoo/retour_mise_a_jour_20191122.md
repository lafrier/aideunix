 * Messages for package sys-fs/eudev-3.2.9:

  * 
   * As of 2013-01-29, eudev-3.2.9 provides the new interface renaming functionality,
    * as described in the URL below:
     * https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames
      * 
       * This functionality is enabled BY DEFAULT because eudev has no means of synchronizing
        * between the default or user-modified choice of sys-fs/udev.  If you wish to disable
	 * this new iface naming, please be sure that /etc/udev/rules.d/80-net-name-slot.rules
	  * exists: touch /etc/udev/rules.d/80-net-name-slot.rules
	   * 

	    * Messages for package sys-kernel/linux-firmware-20191108:

	     * Your configuration for sys-kernel/linux-firmware-20191108 has been saved in 
	      * "/etc/portage/savedconfig/sys-kernel/linux-firmware-20191108" for your editing pleasure.
	       * You can edit these files by hand and remerge this package with
	        * USE=savedconfig to customise the configuration.
		 * You can rename this file/directory to one of the following for
		  * its configuration to apply to multiple versions:
		   * ${PORTAGE_CONFIGROOT}/etc/portage/savedconfig/
		    * [${CTARGET}|${CHOST}|""]/${CATEGORY}/[${PF}|${P}|${PN}]
		     * If you are only interested in particular firmware files, edit the saved
		      * configfile and remove those that you do not want.

		       * Messages for package dev-libs/boost-1.71.0:

		        * Boost.Regex is *extremely* ABI sensitive. If you get errors such as
			 * 
			  *   undefined reference to `boost::re_detail_107100::cpp_regex_traits_implementation
			   *     <char>::transform_primary[abi:cxx11](char const*, char const*) const'
			    * 
			     * Then you need you need to recompile Boost and all its reverse dependencies
			      * using the same toolchain. In general, *every* change of the C++ toolchain
			       * requires a complete rebuild of the boost-dependent ecosystem.
			        * 
				 * See for instance https://bugs.gentoo.org/638138

				  * Messages for package sys-devel/gcc-9.2.0-r2:

				   * If you have issues with packages unable to locate libstdc++.la,
				    * then try running 'fix_libtool_files.sh' on the old gcc versions.
				     * You might want to review the GCC upgrade guide when moving between
				      * major versions (like 4.2 to 4.3):
				       * https://wiki.gentoo.org/wiki/Upgrading_GCC

				        * Messages for package sys-kernel/gentoo-sources-4.19.82:

					 * If you are upgrading from a previous kernel, you may be interested
					  * in the following document:
					   *   - General upgrade guide: https://wiki.gentoo.org/wiki/Kernel/Upgrade

					    * Messages for package sys-process/cronie-1.5.4-r1:

					     * You should restart cronie daemon or else you might experience segfaults
					      * or cronie not working reliably anymore.

					       * Messages for package app-text/texlive-core-2019-r4:

					        * 
						 * If you have configuration files in /etc/texmf to merge,
						  * please update them and run /usr/sbin/texmf-update.
						   * 
						    * If you are migrating from an older TeX distribution
						     * Please make sure you have read:
						      * https://wiki.gentoo.org/wiki/Project:TeX/Tex_Live_Migration_Guide
						       * in order to avoid possible problems

						        * Messages for package app-text/ghostscript-gpl-9.26:

							 * 
							  * Directory symlink(s) may need protection:
							   * 
							    * 	/usr/share/ghostscript/9.26/Resource/CMap
							     * 
							      * Use the UNINSTALL_IGNORE variable to exempt specific symlinks
							       * from the following search (see the make.conf man page).
							        * 
								 * Searching all installed packages for files installed via above symlink(s)...
								  * 
								   * The above directory symlink(s) are all safe to remove. Removing them now...
								    * 

								     * Messages for package dev-vcs/git-2.23.0-r1:

								      * Please read /usr/share/bash-completion/completions/git for Git bash command
								       * completion.
								        * Please read /usr/share/git/git-prompt.sh for Git bash prompt
									 * Note that the prompt bash code is now in that separate script
									  * These additional scripts need some dependencies:
									   *   git-quiltimport  : dev-util/quilt
									    *   git-instaweb     : || ( www-servers/lighttpd www-servers/apache www-servers/nginx )

									     * Messages for package app-admin/sudo-1.8.28_p1-r2:

									      * 
									       * sudo uses the /etc/ldap.conf.sudo file for ldap configuration.
									        * 
										 * To use the -A (askpass) option, you need to install a compatible
										  * password program from the following list. Starred packages will
										   * automatically register for the use with sudo (but will not force
										    * the -A option):
										     * 
										      *  [*] net-misc/ssh-askpass-fullscreen
										       *      net-misc/x11-ssh-askpass
										        * 
											 * You can override the choice by setting the SUDO_ASKPASS environmnent
											  * variable to the program you want to use.

											   * Messages for package dev-db/mariadb-10.2.29:

											    * MySQL MY_DATADIR is /var/lib/mysql
											     * MySQL datadir found in /var/lib/mysql
											      * A new one will not be created.
											       * This install includes the PAM authentication plugin.
											        * To activate and configure the PAM plugin, please read:
												 * https://mariadb.com/kb/en/mariadb/pam-authentication-plugin/
												  * If you are upgrading major versions, you should run the
												   * mysql_upgrade tool.
												    * This version of mariadb reorganizes the configuration from a single my.cnf
												     * to several files in /etc/mysql/mariadb.d.
												      * Please backup any changes you made to /etc/mysql/my.cnf
												       * and add them as a new file under /etc/mysql/mariadb.d with a .cnf extension.
												        * You may have as many files as needed and they are read alphabetically.
													 * Be sure the options have the appropriate section headers, i.e. [mysqld].

													  * Messages for package www-client/firefox-bin-70.0.1:

													   * 
													    * Note regarding Trusted Recursive Resolver aka DNS-over-HTTPS (DoH):
													     * Due to privacy concerns (encrypting DNS might be a good thing, sending all
													      * DNS traffic to Cloudflare by default is not a good idea and applications
													       * should respect OS configured settings), "network.trr.mode" was set to 5
													        * ("Off by choice") by default.
														 * You can enable DNS-over-HTTPS in Firefox-bin's preferences.

														  * Messages for package app-office/libreoffice-bin-6.2.8.2:

														   * If you plan to use the Base application you should enable java or you will get crashes and missing features.

														    * Messages for package sys-fs/eudev-3.2.9:

														     * 
														      * You need to restart eudev as soon as possible to make the
														       * upgrade go into effect:
														        * 	/etc/init.d/udev --nodeps restart
															 * 
															  * For more information on eudev on Gentoo, writing udev rules, and
															   * fixing known issues visit: https://wiki.gentoo.org/wiki/Eudev

															    * Messages for package sys-fs/lvm2-2.02.184-r5:

															     * Notice that "use_lvmetad" setting is enabled with USE="udev" in
															      * /etc/lvm/lvm.conf, which will require restart of udev, lvm, and lvmetad
															       * if it was previously disabled.
															        * Make sure the "lvm" init script is in the runlevels:
																 * # rc-update add lvm boot
																  * 
																   * Make sure to enable lvmetad in /etc/lvm/lvm.conf if you want
																    * to enable lvm autoactivation and metadata caching.
																    >>> Auto-cleaning packages...

																    >>> No outdated packages were found on your system.

																     * Regenerating GNU info directory index...
																      * Processed 110 info files.

																      !!! existing preserved libs:
																      >>> package: dev-libs/boost-1.71.0
																       *  - /usr/lib64/libboost_system.so.1.65.0
																        *      used by /usr/lib64/libnghttp2_asio.so.1.0.0 (net-libs/nghttp2-1.39.2)
																	 *  - /usr/lib64/libboost_thread.so.1.65.0
																	  *      used by /usr/lib64/libnghttp2_asio.so.1.0.0 (net-libs/nghttp2-1.39.2)
																	  >>> package: dev-libs/mpfr-4.0.2
																	   *  - /usr/lib64/libmpfr.so.4
																	    *  - /usr/lib64/libmpfr.so.4.1.6
																	     *      used by /usr/libexec/gcc/x86_64-pc-linux-gnu/8.3.0/cc1 (sys-devel/gcc-8.3.0-r1)
																	      *      used by /usr/libexec/gcc/x86_64-pc-linux-gnu/8.3.0/cc1plus (sys-devel/gcc-8.3.0-r1)
																	       *      used by /usr/libexec/gcc/x86_64-pc-linux-gnu/8.3.0/f951 (sys-devel/gcc-8.3.0-r1)
																	        *      used by /usr/libexec/gcc/x86_64-pc-linux-gnu/8.3.0/lto1 (sys-devel/gcc-8.3.0-r1)
																		Use emerge @preserved-rebuild to rebuild packages using these libraries

																		 * IMPORTANT: config file '/etc/conf.d/dmcrypt' needs updating.
																		  * See the CONFIGURATION FILES and CONFIGURATION FILES UPDATE TOOLS
																		   * sections of the emerge man page to learn how to update config files.
																		    * After world updates, it is important to remove obsolete packages with
																		     * emerge --depclean. Refer to `man emerge` for more information.

