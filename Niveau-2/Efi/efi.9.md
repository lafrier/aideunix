# NAME

efi

# FICHIER D'AMMORCAGE

## Sous HP

Le bios va chercher l'ammorcage dans EFI\windows\windows64.efi

Pour lui donner la bonne adresse :

 - cocher Custom_Boot dans les options de boot

 - mettre add dans le Custom_boot_adress_definition

 - remonter Custom_boot en premier dans les EFI_boot_order
