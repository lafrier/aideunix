# remmina

Prendre la main sur un ordinateur distant.

# Utilisation

Pour se connecter à une machine distante avec le protocole xrdp, il faut créer un serveur xrdp sur cette machine. Ensuite, on lance remmina et on donne le nom et le mot de passe.

