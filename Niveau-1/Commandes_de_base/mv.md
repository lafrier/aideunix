
## Commande `mv`

### Fonction

`mv` pour "move".
La commande `mv` permet

- soit de changer le nom d'un ficher ou celui d'un répertoire.

- soit de changer le répertoire auquel appartient le fichier choisi ou celui auquel appartient le sous-répertoire choisi.

### Utilisation

Dans l'écriture de la commande `mv` il y a toujours 2 parties, la première se rapporte au fichier ou Répertoire qu'on veut modifier, 
la deuxième se rapporte à la modification qu'on veut apporter.

Ces 2 parties sont séparées par un "blanc". Il y aura donc 2 "blancs" dans cette écriture.
Chaque partie est un *chemin relatif*. Il faut donc savoir sous quel Répertoire courant on se trouve.

<noman> ![Arborescence illustrant la position du fichier fich.doc \label{fig:mv-arbex1et2}]($AIDEUNIX/Niveau-1/Figures/mv-arbex1et2.jpg)

`fichx.doc` est *dans* le répertoire `RepE` (voir Figure \ref{fig:mv-arbex1et2})

*Vocabulaire* :
Pour un *fichier* on dit qu'il est *dans* un *sous-répertoire* ou un *répertoire* (il est possible de parler de répertoire à la place de sous-répertoire).
Pour un *sous-répertoire* on dit qu'il est *sous* un *répertoire*. Ceci explique qu'un fichier n'a pas d'antécédent. 
Dans l'exemple précédent `RepE` est le "nom" du sous-répertoire qui a pour "antécédent" `RepA`.

#### Changement de nom du fichier `fichx.doc` en `fichy.doc`

On ne veut pas changer le sous-répertoire auquel il appartient (ici `RepE`)

Répertoire courant sous lequel on se trouve: `RepD`.

    mv ../../../RepE/fichx.doc ../../../RepE/fichy.doc 	

#### Changement de répertoire dans lequel se situe `fichx.doc`	
	
`fichx.doc` est actuellement dans `RepE`. On veut que `fichx.doc` soit un fichier de `RepC`.
 
Répertoire courant sous lequel on se trouve: `RepD`.

    mv ../../../RepE/fichx.doc ./RepC/
											
#### Changement de nom d'un répertoire

Dans les exemples suivants on suivra l'arborescence Figure \ref{fig:mv-arbex3et4}.

<noman> ![Arborescence correspondant aux exemples 3 et 4 \label{fig:mv-arbex3et4}]($AIDEUNIX/Niveau-1/Figures/mv-arbex3et4.jpg)

On veut changer le nom de `Poele_godin` en `Poele_tintin`.
Répertoire sous lequel on se trouve: `Jardinage`.

    mv ../Notices_appareils/Poele_godin ../Notices_appareils/Poele_tintin

#### Changement de répertoire sous lequel se trouve `Poele_tintin`

`Poele_tintin` est actuellement sous `Notices_appareils`. On veut le déplacer sous `Cabane_jardin`.

Répertoire courant: `Jardinage`.

    mv ../Notices_appareils/Poele_tintin ./Cabane_jardin/Poele_tintin. 
 
