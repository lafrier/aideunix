 
## Commande `mkdir`

### Fonction

`mkdir` pour "make directory".
La commande `mkdir` permet de créer un sous-répertoire d'un répertoire donné.

### Utilisation

Lorsqu'on utilise `mkdir`, après le "blanc" qui suit toute commande, on écrit le *chemin relatif* du sous-répertoire qu'on veut créer.

A partir de l'arborescence suivante on va créer différents exemples de sous-répertoires.


<noman> ![Arborescence qui sera utilisée dans le cas 1 et le cas 2]($AIDEUNIX/Niveau-1/Figures/mkdir-cherel.jpg)

- Cas 1: Répertoire courant: `Nouveaux_documents`

    - On veut céer un sous-répertoire `Jojo` de `Nouveaux_documents`  
    `mkdir ./Jojo`  
    *Rappels*: `.` symbolise le répertoire courant, `/` sépare 2 répertoires. Le symbole du répertoire courant peut ne pas être écrit. On aurait alors:  
    `mkdir Jojo`

    - On veut céer un sous-répertoire `Jojette` de `Anciens_documents`   
    `mkdir ../Anciens_documents/Jojette`

    - On veut céer un sous-répertoire `Jojinette` de `Documents` 
    `mkdir ../Anciens_documents/Documents/Jojinette`

- Cas 2: Répertoire courant: `FamilleA`

    - On veut céer un sous-répertoire `Jojinou` de `FamilleB`  
    `mkdir ../FamilleB/Jojinou`

    - On veut céer un sous-répertoire `PetitJojinou` de `PetitsEnfantsB`  
    `mkdir ../FamilleB/PetitsEnfantsB/PetitJojinou`

