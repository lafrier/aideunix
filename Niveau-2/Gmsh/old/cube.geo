lx = 1.e+0;
ly = 1.e+0;
lz = 1.e+0;
lc = 1.e+1;

p1 = 1;
p2 = 2;
p3 = 3;
p4 = 4;
p5 = 5;
p6 = 6;
p7 = 7;
p8 = 8;

Point(p1) = {-lx/2, -ly/2, -lz/2, lc};
Point(p2) = {+lx/2, -ly/2, -lz/2, lc};
Point(p3) = {+lx/2, +ly/2, -lz/2, lc};
Point(p4) = {-lx/2, +ly/2, -lz/2, lc};
Point(p5) = {-lx/2, -ly/2, +lz/2, lc};
Point(p6) = {+lx/2, -ly/2, +lz/2, lc};
Point(p7) = {+lx/2, +ly/2, +lz/2, lc};
Point(p8) = {-lx/2, +ly/2, +lz/2, lc};

l1 = 1;
l2 = 2;
l3 = 3;
l4 = 4;
l5 = 5;
l6 = 6;
l7 = 7;
l8 = 8;
l9 = 9;
l10 = 10;
l11 = 11;
l12 = 12;

Line(l1) = {p1, p2};
Line(l2) = {p2, p3};
Line(l3) = {p3, p4};
Line(l4) = {p4, p1};

Line(l5) = {p5, p6};
Line(l6) = {p6, p7};
Line(l7) = {p7, p8};
Line(l8) = {p8, p5};

Line(l9) = {p1, p5};
Line(l10) = {p2, p6};
Line(l11) = {p3, p7};
Line(l12) = {p4, p8};

cl1 = 1;
cl2 = 2;
cl3 = 3;
cl4 = 4;
cl5 = 5;
cl6 = 6;

Curve Loop(cl1) = {l1, l2, l3, l4};
Curve Loop(cl2) = {l5, l6, l7, l8};
Curve Loop(cl3) = {l1, l10, -l5, -l9};
Curve Loop(cl4) = {l2, l11, -l6, -l10};
Curve Loop(cl5) = {l3, l12, -l7, -l11};
Curve Loop(cl6) = {l4, l9, -l8, -l12};

ps1 = 1;
ps2 = 2;
ps3 = 3;
ps4 = 4;
ps5 = 5;
ps6 = 6;

Plane Surface(ps1) = {cl1};
Plane Surface(ps2) = {-cl2};
Plane Surface(ps3) = {-cl3};
Plane Surface(ps4) = {-cl4};
Plane Surface(ps5) = {-cl5};
Plane Surface(ps6) = {-cl6};


