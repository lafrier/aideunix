# tigervnc

Serveur et client vnc.

# Serveur

    lafrier$  vncpasswd
    Would you like to enter a view-only password (y/n)? n
    lafrier$  x0vncserver -passwordfile ~/.vnc/passwd -display :0

# Client

    lafrier$  vncviewer 192.168.1.36:5900

# Mail Vincent 2

Bonjour à tous,

Richard, je t'avais envoyé un mail qui décrit ce que tu vis en ce moment avec TigerVNC. L'écran noir, l'utilisation de x0vncserver, la sécurisation de la connection via un tunenl SSH, etc. Je crois que tout y étais. Quoiqu'il en soit, le lancement d'un serveur X propre à chaque utilisateur ou le partage d'une même session est je crois proposé par à peu près tous les serveur VNC.

Bref ! En résumé, avec TigerVNC, côté serveur, il faut d'abord générer un mot de passe :

  user@server $ vncpasswd

  On peut alors décider de lancer un serveur VNC qui aura son propre serveur X :

    user@server $ vncserver -passwordfile ~/.vnc/passwd

    Mais ce n'est pas ce que tu recherches :

      user@server $ vncserver -list
        TigerVNC server sessions:
	  X DISPLAY #    PROCESS ID
	    :1        27224
	      user@server $ vncserver -kill :1 # Stoppe le serveur VNC

	      Donc l'alternative consiste à lancer un serveur VNC qui partagera un serveur X existant :

	        user@server $ x0vncserver -passwordfile ~/.vnc/passwd -display :0

		Côté client on se rappellera que le protocole VNC n'est pas protégé. On commencera donc par établir un tunnel SSH avec le serveur.

		  user@client $ ssh -L 5900:localhost:5900 user@server

		  Ci dessus 5900 est le port du serveur VNC lancé par la commande x0vncserver. A noter que ce port est 5901 avec la commande vncserver. La formule pour retrouver ce port est triviale : 5900 + id du serveur X.  'user' et 'server' sont respectivement l'identifiant et l'IP ou le nom du serveur VNC sur lequel on souhaite se connecter en SSH.

		  Ne reste plus alors qu'à se connecter au serveur VNC via le tunnel SSH :

		    user@client $ vncviewer localhost:5900

		    Dans la fenêtre qui se lance et vous demande le mot de passe du serveur, vous pourrez ignorer le message qui dit que la connexion n'est pas sécurisée puisqu'elle l'est via le tunnel SSH.

		    Vincent 

# Mail Vincent 1

Bonjour Richard,

Je mets Benjamin en copie car ça peut l'intéresser.

J'ai continué un peu hier soir à regarder comment prendre la main sur une machine GNU/Linux avec retour graphique. J'avais un peu regardé au début du confinement notamment pour partager un écran et il me semblait bien qu'il n'y avait pas besoin d'un monstre comme remmina & co. Un simple serveur VNC suffit.

Cette fois je suis allé jusqu'au bout de la démarche. J'ai donc installé TigerVNC sur mon ordinateur fixe en lui ajoutant le flag server.

  root@desktop $ echo "net-misc/tigervnc server" >> /etc/portage/package.use/tigervnc
    root@desktop $ emerge -av net-misc/tigervnc

    Sur le portable j'ai fait de même sauf que je ne l'ai pas compilé avec support du serveur. Comme je souhaite prendre la main sur le fixe à partir de mon portable, et non l'inverse, seul le fixe à besoin du serveur VNC.

      root@laptop $ emerge -av net-misc/tigervnc

      Sur le fixe j'ai alors lancé le serveur en tant que simple utilisateur.  Lors du premier lancement, la commande vncserver demande de définir le mot de passe à rentrer pour se connecter audit serveur.

        user@desktop $ vncserver

	Sur le portable je me suis enfin connecté au serveur VNC de mon fixe.

	  user@laptop $ vncviewer 192.168.1.7:5901

	  192.168.1.7 est l'IP du serveur VNC dans mon réseau local et 5901 le port par défaut qu'écoute le serveur VNC.

	  Si tu reproduis ces seules étapes tu obtiendras, suite à ta connexion au serveur VNC, un écran sobrement noir. C'est normal. Aucun gestionnaire d'affichage n'est lancé lorsque tu te connectes au serveur VNC. Pour lancer le gestionnaire de fenêtres de ton choix, il te faut modifier le fichier ".vnc/xstartup" sur le serveur VNC.

	    user@desktop $ vim ~/.vnc/xstartup

	    Tu peux à la place de xinitrc lancer xfce, openbox, etc. Autre chose, tu verras dans la fenêtre graphique qui te demande le mot de passe du serveur VNC, que la connexion n'est pas sécurisée. Pour contourner le pbr, rien de plus simple. Sur le fixe, j'ai également un serveur SSH qui tourne. Il me suffit donc d'utiliser un tunnel SSH pour sécuriser la connexion au serveur VNC.

	      user@laptop $ ssh -L 5901:localhost:5901 user@desktop
	        user@laptop $ # Dans un autre terminal
		  user@laptop $ vncviewer localhost:5901

		  Tu peux désormais ignorer le message qui te dit que la connexion n'est pas sécurisée puisqu'elle l'est via le tunnel SSH.
		  A noter qu'en l'état il n'y a pas bcp de valeur ajoutée à passer par VNC puisque SSH suffit généralement pour administrer une machine distante. Reste que VNC permet de partager un même serveur d'affichage ! Mais pour ce faire il faut passer par la commande x0vncserver en lieu et place de vncserver

		    user@desktop $ # Stopper le serveur précédent
		      user@desktop $ vncserver -list
		        TigerVNC server sessions:
			  X DISPLAY #    PROCESS ID
			      :1        15748
			        user@desktop $ vncserver -kill :1
				  user@desktop $ # Lancer un nouveau serveur VNC
				    user@desktop $ x0vncserver -passwordfile ~/.vnc/passwd -display :0

				    Tu remarqueras alors que le port du serveur VNC n'est plus 5901 mais 5900. Hormis ce port, la procédure pour se connecter à ce nouveau serveur est scrupuleusement la même.

				      user@laptop $ # Tunnel SSH pour sécuriser la connexion VNC
				        user@laptop $ ssh -L 5900:localhost:5900 user@desktop
					  user@laptop $ # Dans un autre terminal
					    user@laptop $ vncviewer localhost:5900

					    Et voilà !

					    Je ne sais pas si ça répond à ta question concernant l'administration à distance de l'ordinateur de ta mère. Ici un simple SSH est bien plus léger. Je comprends cependant le bénéfice énorme de pouvoir partager un même affichage pour ne pas faire l'administration à l'aveugle de l'utilisateur. Mais ici TMate est suffisant. J'ai compris qu'il y avait des pbr avec WSL et TMate mais il est possible qu'on puisse se passer de TMate. Faut que je regarde plus en détail mais déjà il est plus que certain que SSH est encore et toujours la base de tout accès distant.  Première étape donc : installer un serveur SSH et s'y connecter.

					    Vincent 
