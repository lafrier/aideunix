# crossdev

Créer un compilateur pour une autre architecture que celle de la machine courante.

## Lister les architectures disponibles

    root#  crossdev -t help

Pour raspberry en 64 bit, on retient `aarch64`.

## Créer un compilateur

Pour raspberry en 64 bit :

    root#  crossdev --ov-output /var/db/repos/localrepo-crossdev -t aarch64-unknown-linux-gnu

Pour voir le compilateur créé :

    lafrier$  gcc-config -l

Typiquement cette commande renvoie la liste suivante :

     [1] aarch64-unknown-linux-gnu-10.2.0 *
     [2] x86_64-pc-linux-gnu-9.3.0 *

