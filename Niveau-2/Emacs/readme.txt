Pour enlever la page d'accueil :
Clicker sur Customize Startup
Sur Inhibit Startup Screen, appuyer sur Toggle pour avoir on
Apply and Save


C- : touche Ctrl
M- : touche Alt

Undo : C-x u

Pour annuler une commande partiellement tappée : C-g

Pour quitter Emacs : C-x C-c

Pour changer la taille de la police : C-x C-+ ou C-x C--


Pour ouvrir un fichier : C-x C-f

Pour sauvegarder un fichier : C-x C-s

Pour sauvegarder un fichier sous un autre nom : C-x C-w

Pour splitter le buffer en n parties : C-x n

Pour aller en fin de ligne : C-e
Pour aller en début de ligne : C-a

Pour aller en fin de buffer : M->
Pour aller en début de buffer : M-<

Pour enregistrer une sélection : M-w
Pour la copier : C-y

Pour changer de buffer
C-x <LEFT>
C-x <RIGHT>

Pour connaitre la version d'emacs
emacs --version

=================================================
Org-mode :

Ouvrir un fichier avec extension .org passe automatiquement en mode org-mode

Sinon pour passer en org-mode :
Alt X org-mode

Pour mettre un TODO
Shift Right-arrow

Pour mettre un DONE
Shift Left-arrow

Pour écrire un "checkbox"
passer à la ligne et se mettre au même niveau d'indentation que le texte du outline concerné, puis tapper
 - [ ] titreDuCheckbox
Mettre le curseur entre les crochet et Ctrl-C-C pour checker

Pour avoir le bilan d'un "checkbox"
A la fin de ligne du "outline" correspondant, rajouter [/] ou [%]
Mettre le curseur sur le / et Ctrl-C-C pour avoir le bilan

Pour ajouter un "deadline" à un "outline"
Ctrl-C-D

Associer un "tag" à un "outline"
Ctrl-C-C

Pour autoriser l'agenda
Ctrl-C-[

Pour voir les options de l'agenda
Alt-X org-agenda
n essayer
m essayer
q pour quiter
Ctrl-X-0 pour retourner à la fenêtre principale si besoin

Pour exporter en html (crée un fichier .html)
C-c C-e h h     (org-html-export-to-html)
Pour aussi visualiser le html
C-c C-e h o

Pour exporter en latex (crée un fichier .tex)
C-c C-e l l
Pour aussi produire le pdf et le visualiser
C-c C-e l o

#+TITLE: Titre
#+OPTIONS: toc:nil
*bold*, /italic/, _underlined_, =verbatim= and ~code~, and, if you must, ‘+strike-through+’

#+ATTR_HTML: :width 20% :height 20%
#+CAPTION: This is the caption for the next figure link (or table)
#+NAME:   fig:SED-HR4049
[[./fig-09.jpeg]]


=================================================
Org-mode comme tableur :

S-A fleches pour ajouter ou enlever des lignes ou des colonnes

C-c C-c sur la ligne #+TBLFM: pour appliquer les formules

|              | Pétra et Luc | Cécile et Richard |
|--------------+--------------+-------------------|
| Dépenses     |              |                   |
|              |              |                   |
| Dettes       |            2 |                55 |
|              |            3 |                81 |
| Total dettes |            5 |               136 |
|              |              |                   |
#+TBLFM: @6$3=vsum(@4$3..@5$3) :: @6$2=vsum(@4$2..@5$2)
