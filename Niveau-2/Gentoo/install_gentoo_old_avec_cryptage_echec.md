# LIVE CD SUR UNE CLE

## Telechargement

livecdvd-amd64-multilib-20160704.iso

## Ecriture sur la cle

La cle est /dev/sdc

root#  dd if=/path/to/livecdvd-amd64-multilib-20160704.iso of=/dev/sdc bs=8192k

root#  sync

## Apres le boot sur la cle

Ouverture d'un terminal

user(*)# setxkbmap fr

user(*)# sudo su -

root(*)# mkdir /mnt/data

root(*)# mount /dev/sdb1 /mnt/data

root(*)# ping gnu.org

Rq: Si le ping ne fonctionne pas, utiliser nmtui pour activer une connexion wifi

# PARTITIONNEMENT

## Effacement du partitionnement precedent

root#  parted -a optimal /dev/sda

(parted)  mklabel gpt

(parted)  unit mib

## Creation de la partition grub

(parted)  mkpart primary 1 3

(parted)  name 1 grub

(parted)  set 1 bios_grub on

(parted)  print

## Creation de la partition boot EFI

(parted)  mkpart primary 3 515

(parted)  name 2 boot

(parted)  set 2 boot on                                                    

(parted)  print  

## Creation d'un volume lvm pour le reste du disque

(parted)  mkpart primary 515 -1

(parted)  name 3 lvm

(parted)  set 3 lvm on 

(parted)  print

(parted)  quit

# CRYPTAGE ET PARTITIONNEMENT DU VOLUME LVM

## Cryptage et ouverture

root#  cryptsetup luksFormat -c aes-xts-plain64:sha256 -s 256 /dev/sda3

root(*)#  cryptsetup luksOpen /dev/sda3 lvm

root#  lvm pvcreate /dev/mapper/lvm

root#  vgcreate vg0 /dev/mapper/lvm

## Partitionnement

root#  lvcreate -L 40G -n swap vg0

root#  lvcreate -L 80G -n root vg0

root#  lvcreate -l 100%FREE -n home vg0

# FORMATAGE

## Premiere partition pour rien

root(*)#  mkfs.ext4 /dev/sda1

## Partition boot EFI

root(*)#  mkfs.fat -F 32 /dev/sda2

## Partition swap

root#  mkswap /dev/mapper/vg0-swap

## Partition root

root#  mkfs.ext4 /dev/mapper/vg0-root

## Partition home

root#  mkfs.ext4 /dev/mapper/vg0-home

# MONTAGE

## Partition swap

root(*)#  swapon -v /dev/mapper/vg0-swap

## Partition root

root(*)#  mount /dev/mapper/vg0-root /mnt/gentoo

## Partition home

root#  mkdir /mnt/gentoo/home

root(*)#  mount /dev/mapper/vg0-home /mnt/gentoo/home

# REGLAGE DE LA DATE

root#  ntpd -q -g

# TELECHARGEMENT DE L'ARCHIVE

root#  cd /mnt/gentoo

root#  wget http://distfiles.gentoo.org/releases/amd64/autobuilds/20190925T214502Z/stage3-amd64-20190925T214502Z.tar.xz

root#  tar xpvf stage3-amd64-20190925T214502Z.tar.xz --xattrs-include='*.*' --numeric-owner

# OPTIONS DE COMPILATION

## Recherche des bonnes options

Je ne m'en suis pas sorti ...

## Modification de /mnt/gentoo/etc/portage/make.conf

root#  vim /mnt/gentoo/etc/portage/make.conf

vim:

  CFLAGS="-O2 -pipe -march=native"

  CXXFLAGS="${CFLAGS}"

  FCFLAGS="${CFLAGS}"

  FFLAGS="${CFLAGS}"

# CHROOT

## Choisir le miroir

root#  mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf

root#  mkdir --parents /mnt/gentoo/etc/portage/repos.conf

root#  cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

## Le DNS

root(*)#  cp --dereference /etc/resolv.conf /mnt/gentoo/etc/

## Montages

root(*)#  mount --types proc /proc /mnt/gentoo/proc

root(*)#  mount --rbind /sys /mnt/gentoo/sys

root(*)#  mount --make-rslave /mnt/gentoo/sys

root(*)#  mount --rbind /dev /mnt/gentoo/dev

root(*)#  mount --make-rslave /mnt/gentoo/dev 

root(*)#  test -L /dev/shm && rm /dev/shm && mkdir /dev/shm

root(*)#  mount -t tmpfs -o nosuid,nodev,noexec shm /dev/shm

root(*)#  chmod 1777 /dev/shm

## Entree dans le nouvel environnement

root(*)#  chroot /mnt/gentoo /bin/bash

root(*)#  source /etc/profile

root(*)#  export PS1="(chroot) ${PS1}"

root(*)#  ping gnu.org

## Montage de la partition boot

root(*)#  mount /dev/sda2 /boot

## Configuration de Portage

root#  emerge-webrsync

root#  eselect news list

root#  eselect news read

root#  eselect profile list

root#  emerge --ask --verbose --update --deep --newuse @world

## Timezone

root#  echo "Europe/Paris" > /etc/timezone

root#  emerge --config sys-libs/timezone-data

## Locales

root#  nano /etc/locale.gen

nano:

  fr_FR.UTF-8 UTF-8

root#  locale-gen

root#  eselect locale list

root#  eselect locale set 4

root#  nano /etc/env.d/02locale

nano:

  LC_COLLATE="C"

root#  env-update && source /etc/profile && export PS1="(chroot) ${PS1}"


# CONFIGURATION DU MONTAGE DES DISQUES

root#  blkid

==================================

/dev/sda1: UUID="44254076-2891-4884-8727-78121f1f0e86" TYPE="ext4" PARTLABEL="grub" PARTUUID="65a21095-a6ff-44b3-a200-fe6b0641bf17"

/dev/sda2: UUID="B3CB-4572" TYPE="vfat" PARTLABEL="boot" PARTUUID="9e4807fe-cd29-4ace-baea-83ef5cbca7f0"

/dev/sda3: UUID="8c4a5cf0-9a93-4ed6-aa56-884eec1ae894" TYPE="crypto_LUKS" PARTLABEL="lvm" PARTUUID="adcc76c8-0352-4dbe-b1b6-2d292b707c31"

/dev/sdb1: UUID="b35b8412-42a1-42b3-85d9-062774770f10" TYPE="ext4" PARTUUID="0004fbc4-01"

/dev/mapper/lvm: UUID="SQMxrA-VvQp-rF8r-9u5k-M9c4-07t4-hAkKhu" TYPE="LVM2_member"

/dev/mapper/vg0-swap: UUID="9662f1f0-bf00-4c8a-b091-33f8dfbe9804" TYPE="swap"

/dev/mapper/vg0-root: UUID="74fb6e9d-d0bf-47d4-a667-2ce16cd6463f" TYPE="ext4"

/dev/mapper/vg0-home: UUID="f5871726-5289-47f1-a5a1-6aa835b6938a" TYPE="ext4"

==================================

Creer les points de montages, puis editer /etc/fstab

root#  nano /etc/fstab

nano:

  UUID=B3CB-4572                               /boot           vfat            noauto,noatime        0 2

  UUID=9662f1f0-bf00-4c8a-b091-33f8dfbe9804    none            swap            sw                    0 0

  UUID=74fb6e9d-d0bf-47d4-a667-2ce16cd6463f    /               ext4            noatime,discard       0 1

  UUID=f5871726-5289-47f1-a5a1-6aa835b6938a    /home           ext4            noatime,discard       0 2

  UUID=b35b8412-42a1-42b3-85d9-062774770f10    /mnt/data       ext4            noatime               0 2

  tmpfs                                        /tmp            tmpfs           size=4Gb              0 0

  tmpfs                                        /run            tmpfs           size=100M             0 0

  shm                                          /dev/shm        tmpfs           nodev,nosuid,noexec   0 0

  /dev/cdrom                                   /mnt/cdrom      auto            noauto,user           0 0


# CONFIGURATION DU NOYAU

## Installation des sources

root#  emerge --ask sys-kernel/gentoo-sources

## Installation des outils systeme

root#  emerge --ask sys-apps/pciutils

root#  emerge --ask sys-apps/usbutils

root#  emerge --ask sys-kernel/linux-firmware 

root#  emerge --ask sys-fs/cryptsetup

root#  emerge --ask lvm2

## Recupperer les informations sur le materiel et les drivers tels que vus par la cle bootable

root#  lspci -k

===========================================

00:00.0 Host bridge: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor DRAM Controller (rev 06)

Subsystem: Hewlett-Packard Company Xeon E3-1200 v3/4th Gen Core Processor DRAM Controller

00:01.0 PCI bridge: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor PCI Express x16 Controller (rev 06)

Kernel driver in use: pcieport

00:14.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family USB xHCI

Kernel driver in use: xhci_hcd

00:16.0 Communication controller: Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1 (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family MEI Controller

00:16.3 Serial controller: Intel Corporation 8 Series/C220 Series Chipset Family KT Controller (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family KT Controller

Kernel driver in use: serial

00:19.0 Ethernet controller: Intel Corporation Ethernet Connection I217-LM (rev 04)

Subsystem: Hewlett-Packard Company Ethernet Connection I217-LM

Kernel driver in use: e1000e

00:1a.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2 (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family USB EHCI

Kernel driver in use: ehci-pci

00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset High Definition Audio Controller

Kernel driver in use: snd_hda_intel

00:1c.0 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d4)

Kernel driver in use: pcieport

00:1c.6 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #7 (rev d4)

Kernel driver in use: pcieport

00:1d.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family USB EHCI

Kernel driver in use: ehci-pci

00:1f.0 ISA bridge: Intel Corporation QM87 Express LPC Controller (rev 04)

Subsystem: Hewlett-Packard Company QM87 Express LPC Controller

00:1f.2 SATA controller: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]

Kernel driver in use: ahci

00:1f.3 SMBus: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller (rev 04)

Subsystem: Hewlett-Packard Company 8 Series/C220 Series Chipset Family SMBus Controller

Kernel driver in use: i801_smbus

01:00.0 VGA compatible controller: NVIDIA Corporation GK104GLM [Quadro K3100M] (rev a1)

Subsystem: Hewlett-Packard Company GK104GLM [Quadro K3100M]

Kernel driver in use: nouveau

01:00.1 Audio device: NVIDIA Corporation GK104 HDMI Audio Controller (rev a1)

Subsystem: Hewlett-Packard Company GK104 HDMI Audio Controller

Kernel driver in use: snd_hda_intel

3b:00.0 PCI bridge: Pericom Semiconductor Device 2404 (rev 05)

Kernel driver in use: pcieport

3c:01.0 PCI bridge: Pericom Semiconductor Device 2404 (rev 05)

Kernel driver in use: pcieport

3c:02.0 PCI bridge: Pericom Semiconductor Device 2404 (rev 05)

Kernel driver in use: pcieport

3c:03.0 PCI bridge: Pericom Semiconductor Device 2404 (rev 05)

Kernel driver in use: pcieport

3d:00.0 Network controller: Intel Corporation Wireless 7260 (rev 6b)

Subsystem: Intel Corporation Dual Band Wireless-AC 7260

Kernel driver in use: iwlwifi

5f:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS5249 PCI Express Card Reader (rev 01)

Subsystem: Hewlett-Packard Company RTS5249 PCI Express Card Reader


===========================================

root#  lsmod

lsmod:

===========================================

Module                  Size  Used by                                                                                    

algif_skcipher          8333  0                                                                                           

af_alg                  7159  1 algif_skcipher                                                                             

ipv6                  291888  26                                                                                           

arc4                    2112  2                                                                                            

iwlmvm                175537  0                                                                                            

mac80211              346581  1 iwlmvm                                                                                     

binfmt_misc             6632  1                                                                                            

x86_pkg_temp_thermal     5345  0                                                                                          

intel_powerclamp        8338  0                                                                                           

coretemp                5884  0                                                                                           

kvm_intel             159209  0                                                                                          

nouveau              1319993  8

kvm                   298938  1 kvm_intel

irqbypass               3455  1 kvm

snd_hda_codec_hdmi     34595  1

hp_wmi                  7145  0

mxm_wmi                 2077  1 nouveau

sparse_keymap           3836  1 hp_wmi

snd_hda_codec_realtek    64017  1

snd_hda_codec_generic    47672  1 snd_hda_codec_realtek

btusb                  26201  0

btrtl                   3752  1 btusb

btbcm                   6872  1 btusb

snd_hda_intel          22430  4

uvcvideo               64661  0

btintel                 8701  1 btusb

videobuf2_vmalloc       4481  1 uvcvideo

ttm                    76657  1 nouveau

snd_hda_codec          86071  4 snd_hda_codec_realtek,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_hda_intel

crct10dif_pclmul        5269  0

bluetooth             290130  5 btbcm,btrtl,btusb,btintel

crc32_pclmul            2918  0

crc32c_intel           12967  0

joydev                  9069  0

input_leds              3054  0

ghash_clmulni_intel     3933  0

videobuf2_memops        1980  1 videobuf2_vmalloc

iwlwifi               115808  1 iwlmvm

drm_kms_helper        107490  1 nouveau

videobuf2_v4l2         13951  1 uvcvideo

pcspkr                  2094  0

serio_raw               4762  0

videodev              123502  2 uvcvideo,videobuf2_v4l2

videobuf2_core         25818  2 uvcvideo,videobuf2_v4l2

snd_hda_core           44369  5 snd_hda_codec_realtek,snd_hda_codec_hdmi,snd_hda_codec_generic,snd_hda_codec,snd_hda_intel

cfg80211              221714  3 iwlwifi,mac80211,iwlmvm

snd_hwdep               5785  1 snd_hda_codec

rfkill                 12435  6 cfg80211,hp_wmi,bluetooth

drm                   304875  11 ttm,drm_kms_helper,nouveau

snd_pcm                79063  4 snd_hda_codec_hdmi,snd_hda_codec,snd_hda_intel,snd_hda_core

wmi                     8928  3 hp_wmi,mxm_wmi,nouveau

e1000e                145765  0

i2c_i801               10796  0

thermal                 9500  0

snd                    64359  14 snd_hwdep,snd_hda_codec_hdmi,snd_pcm,snd_hda_codec_generic,snd_hda_codec,snd_hda_intel

ie31200_edac            3640  0

edac_core              45563  1 ie31200_edac

processor              26136  4

aesni_intel           158765  2

lrw                     4006  1 aesni_intel

glue_helper             4963  1 aesni_intel

ablk_helper             2845  1 aesni_intel

cryptd                  8905  4 ghash_clmulni_intel,aesni_intel,ablk_helper

aes_x86_64              7915  1 aesni_intel

iscsi_tcp               8863  0

libiscsi_tcp           14338  1 iscsi_tcp

libiscsi               39895  2 libiscsi_tcp,iscsi_tcp

scsi_transport_iscsi    82684  2 iscsi_tcp,libiscsi

vmxnet3                36049  0

virtio_net             18731  0

tg3                   130772  0

libphy                 36892  1 tg3

sky2                   43921  0

r8169                  60733  0

pcnet32                30351  0

mii                     4846  2 r8169,pcnet32

igb                   124603  0

ptp                    10627  3 igb,tg3,e1000e

pps_core                7313  1 ptp

i2c_algo_bit            5403  2 igb,nouveau

dca                     6514  1 igb

e1000                  90876  0

bnx2                   66331  0

atl1c                  29626  0

fuse                   73787  1

zfs                  2632683  0

zunicode              316308  1 zfs

zcommon                38795  1 zfs

znvpair                48093  2 zfs,zcommon

spl                    67243  3 zfs,zcommon,znvpair

zavl                    5009  1 zfs

nfs                   138649  0

lockd                  57396  1 nfs

grace                   2571  1 lockd

sunrpc                202691  2 nfs,lockd

btrfs                 798022  0

zlib_deflate           19082  2 spl,btrfs

multipath               5522  0

linear                  3287  0

raid10                 34066  0

raid1                  25215  0

raid0                   5989  0

dm_raid                15945  0

raid456                71264  1 dm_raid

async_raid6_recov       1880  1 raid456

async_memcpy            1880  1 raid456

async_pq                4732  1 raid456

async_xor               3643  2 async_pq,raid456

async_tx                2764  5 async_pq,raid456,async_xor,async_memcpy,async_raid6_recov

xor                    10372  2 btrfs,async_xor

raid6_pq               96203  4 async_pq,raid456,btrfs,async_raid6_recov

dm_snapshot            27173  0

dm_bufio               18787  1 dm_snapshot

dm_crypt               16594  1

dm_mirror              12069  0

dm_region_hash         10098  1 dm_mirror

dm_log                  8411  2 dm_region_hash,dm_mirror

dm_mod                 86120  17 dm_raid,dm_log,dm_mirror,dm_bufio,dm_crypt,dm_snapshot

firewire_core          51706  0

hid_sunplus             1822  0

hid_sony               13627  0

hid_samsung             3170  0

hid_pl                  2644  0

hid_petalynx            2303  0

hid_monterey            1886  0

hid_microsoft           3673  0

hid_gyration            2427  0

hid_ezkey               1751  0

hid_cypress             2143  0

hid_chicony             2727  0

hid_cherry              1886  0

hid_belkin              1967  0

hid_a4tech              2103  0

sl811_hcd              11962  0

usb_storage            51025  1

aic94xx                65279  0

libsas                 57895  1 aic94xx

lpfc                  550556  0

qla2xxx               521168  0

megaraid_sas           95188  0

megaraid_mbox          25720  0

megaraid_mm             8133  1 megaraid_mbox

megaraid               36517  0

aacraid                75762  0

sx8                    12354  0

DAC960                 63203  0

hpsa                   74913  0

cciss                  46510  0

3w_9xxx                30140  0

3w_xxxx                21364  0

3w_sas                 19032  0

mptsas                 34153  0

mptfc                  11161  0

scsi_transport_fc      46748  3 lpfc,qla2xxx,mptfc

mptspi                 11827  0

mptscsih               21093  3 mptfc,mptsas,mptspi

mptbase                63323  4 mptfc,mptsas,mptspi,mptscsih

atp870u                16936  0

dc395x                 27696  0

qla1280                20672  0

dmx3191d               10401  0

sym53c8xx              63693  0

gdth                   74460  0

advansys               44102  0

initio                 15050  0

BusLogic               20437  0

arcmsr                 32381  0

aic7xxx               104318  0

aic79xx               107773  0

scsi_transport_spi     20297  5 mptspi,sym53c8xx,aic79xx,aic7xxx,dmx3191d

sg                     23717  0

pdc_adma                5891  0

sata_inic162x           7120  0

sata_mv                24780  0

ata_piix               25755  0

ahci                   26978  3

libahci                24185  1 ahci

sata_qstor              5758  0

sata_vsc                4619  0

sata_uli                3519  0

sata_sis                4133  0

sata_sx4                8528  0

sata_nv                19873  0

sata_via                8779  0

sata_svw                4936  0

sata_sil24             10930  0

sata_sil                8093  0

sata_promise           10478  0

pata_sl82c105           4285  0

pata_via                9454  0

pata_jmicron            2919  0

pata_marvell            3468  0

pata_sis               12355  1 sata_sis

pata_netcell            2776  0

pata_pdc202xx_old       5091  0

pata_triflex            3786  0

pata_atiixp             5223  0

pata_amd               11383  0

pata_ali               10376  0

pata_it8213             4154  0

pata_pcmcia            10313  0

pcmcia                 33206  1 pata_pcmcia

pcmcia_core            14891  1 pcmcia

pata_ns87415            3768  0

pata_ns87410            3446  0

pata_serverworks        6296  0

pata_artop              5653  0

pata_it821x             9591  0

pata_hpt3x2n            6095  0

pata_hpt3x3             3697  0

pata_hpt37x            12553  0

pata_hpt366             5583  0

pata_cmd64x             7638  0

pata_efar               4315  0

pata_rz1000             3302  0

pata_sil680             5398  0

pata_radisys            3586  0

pata_pdc2027x           7130  0

pata_mpiix              3459  0

===========================================

## La configuration manuelle

N'a pas fonctionne ...

Pour memoire:

cd /usr/src/linux

make menuconfig

Toujours choisir la compilation et pas le module (en selectionnant deux fois avec TAB)

Pour chercher quelque chose : / suivi du nom en majuscule

Penser a faire ce qu'il faut pour dm-crypt et lvm

Penser a verifier que tous les drivers listes par lspci -k sont bien selectionnes

Et sinon, suivre les conseils

cd /usr/src/linux

make && make modules_install

make install

Ca n'a pas fonctionne ...

## La configuration automatique

root#  nano /etc/portage/package.use/utils-linux

nano:

  sys-apps/util-linux static-libs

root#  mkdir /etc/portage/package.license

root#  nano /etc/portage/package.license/linux-firmware

nano:

  sys-kernel/linux-firmware linux-fw-redistributable no-source-code

root#  emerge --ask sys-kernel/genkernel

Commenter /boot dans /etc/fstab

root#  nano -w /etc/fstab

nano:

  #/dev/sda2               /boot           vfat            noauto,noatime    0 2

root#  genkernel --menuconfig all

outil-de-configuration:

  mettre ce qu'il faut pour dm-crypt et pour lvm

  mettre un peu tout ce qui est conseille pour la configuration manuelle

Decommenter /boot dans /etc/fstab

root#  nano -w /etc/fstab

nano:

  /dev/sda2               /boot           vfat            noauto,noatime    0 2

root#  genkernel --luks --lvm --no-zfs all

# CONFIGURATION SYSTEME

## Les parametres reseau

root#  nano -w /etc/conf.d/hostname

nano:

  hostname="cpat20"

root#  emerge --ask --noreplace net-misc/netifrc

root#  emerge --ask net-misc/dhcpcd



root#  nano -w /etc/conf.d/net

nano:

  config_eth0="dhcp"

root#  cd /etc/init.d

root#  ln -s net.lo net.eth0

root#  rc-update add net.eth0 default

root#  nano -w /etc/hosts

nano:

  127.0.0.1       cpat20.ups-tlse.fr     cpat20     localhost

## Root

root#  passwd

## Init and boot

root#  nano -w /etc/rc.conf

Je n'ai rien change

## Clavier

root#  nano -w /etc/conf.d/keymaps

nano:

  keymap="fr"

## Heure systeme

root#  nano -w /etc/conf.d/hwclock

Je n'ai rien change

## System logger

root#  emerge --ask app-admin/sysklogd

root#  rc-update add sysklogd default

## Cron

root#  emerge --ask sys-process/cronie

root#  rc-update add cronie default

## File indexing

root#  emerge --ask sys-apps/mlocate

## Filesystem tools

root#  emerge --ask sys-fs/e2fsprogs

root#  emerge --ask sys-fs/xfsprogs

root#  emerge --ask sys-fs/reiserfsprogs

root#  emerge --ask sys-fs/jfsutils

root#  emerge --ask sys-fs/dosfstools

root#  emerge --ask sys-fs/btrfs-progs

## Client DHCP

root#  emerge --ask net-misc/dhcpcd

## Client PPPoE

root#  emerge --ask net-dialup/ppp

## Wifi

root#  emerge --ask net-wireless/iw net-wireless/wpa_supplicant

# CONFIGURER LE BOOTLOADER

## Monter /boot

root#  mount /boot

## Grub2

root#  nano /etc/portage/make.conf

nano:

  GRUB_PLATFORMS="efi-64"

root#  emerge --ask --verbose sys-boot/grub:2

emerge:

  Verifier que efi-64 apparait bien dans GRUB_PLATFORMS="efi-64 ..." avant de repondre Yes

root#  grub-install --target=x86_64-efi --efi-directory=/boot --removable

root#  grub-mkconfig -o /boot/grub/grub.cfg

## Ajouter le service lvm

rc-update add lvm default

## Reboot

root #exit

cdimage ~#cd

cdimage ~#umount -l /mnt/gentoo/dev{/shm,/pts,}

cdimage ~#umount -R /mnt/gentoo

cdimage ~#reboot

# FAIRE CE QU'IL FAUT POUR LE SSD

Add to /etc/default/grub trim command:

    GRUB_CMDLINE_LINUX="...root_trim=yes"

edit /etc/lvm/lvm.conf LVM

    issue_discards = 1

Et certainement faire quelque chose pour le dire a grub ...

