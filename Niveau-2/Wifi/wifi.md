# NAME

wifi

## Nouvelle connexion wifi

Dans l'exemple qui suit, `0` sera remplacé par le numéro de la nouvelle connexion wifi.

    root#  wpa_cli
    wpa_cli:
      > scan
      OK
      <3>CTRL-EVENT-SCAN-RESULTS
      > scan_results
      bssid / frequency / signal level / flags / ssid
      00:00:00:00:00:00 2462 -49 [WPA2-PSK-CCMP][ESS] MYSSID
      11:11:11:11:11:11 2437 -64 [WPA2-PSK-CCMP][ESS] ANOTHERSSID
      > add_network
      0
      > set_network 0 ssid "MYSSID"
      > set_network 0 psk "passphrase"
      > enable_network 0
      <2>CTRL-EVENT-CONNECTED - Connection to 00:00:00:00:00:00 completed (reauth) [id=0 id_str=]
      > save_config
      OK
      > quit

    root#  rc-service wpa_supplicant restart
    root#  ping gnu.org
