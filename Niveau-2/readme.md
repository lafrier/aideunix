# Projet documentation

Documentation de second niveau pour une pratique informatique sous Unix 

# Environnement

Créer une variable d'environnement `DOCUMENTATION` qui pointe vers le répertoire `documentation`. Rajouter cette `$DOCUMENTATION` au `MANPATH`. Rajouter `$DOCUMENTATION/bin` au PATH. Par exemple dans le fichier ~/.bashrc, rajouter

    export DOCUMENTATION=~/Transfert-local/Informatique/Formation/documentation
    export PATH=$PATH:$DOCUMENTATION/bin
    export MANPATH=$MANPATH:$DOCUMENTATION
