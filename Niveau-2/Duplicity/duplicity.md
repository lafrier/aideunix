# Commande duplicity

Permet de faire une suavegarde incrémentale.

# Utilisation de base

## Pour sauvegarder

lafrier$  duplicity --no-encryption /mnt/data/lafrier/Cloud/drivecolette file:///mnt/data/lafrier/Gros-volumes-perso-local/Colette/Sauvegarde

## Pour lister les fichiers sauvegardés

lafrier$  duplicity --no-encryption list-current-files file:///mnt/data/lafrier/Gros-volumes-perso-local/Colette/Sauvegarde

## Pour récupérer un fichier

lafrier$  duplicity --no-encryption --file-to-restore toto/pwd.pdf file:///mnt/data/lafrier/Gros-volumes-perso-local/Colette/Sauvegarde ~/tmp/pwd.pdf

# Utilisation avancée

Voir le fichier `backup_to_nas_.sh` de Benjamin

et le commentaire suivant :

Et voici mon crontab

`00 13 * * 1-5 /home/piaud/Documents/duplicity/backup_to_nas_.sh`

