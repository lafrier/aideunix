
## Commande `find`

### Fonction

Cette commande permettra de *trouver* le répertoire ou le fichier qu'on a "perdu" dans
l'arbre.
  
### Utilisation

On doit indiquer le chemin relatif du répertoire sous lequel on souhaite démarrer
  la recherche puis le nom ou une partie du nom du répertoire ou du fichier qu'on
 recherche.

#### Analyse d'un exemple

    colette:~/Racine$ find ./ -name "*taxe carbone*"

D'après la ligne précédente le répertoire courant est `Racine`.
Dans l'écriture de la commande `find` est indiqué que la recherche doit commencer
 à partir du répertoire courant. Ensuite `"-name"` introduit la partie qu'on connait
du nom du fichier ou répertoire recherché.

*Remarques*:

1. Dans l'écriture de cette commande il y a 3 "blancs", un après `find`,
 un autre avant et un autre après `-name`.

2. Les étoiles qui encadrent le nom "taxe carbone" indiquent qu'il peut y avoir 
des mots avant et après le nom donné.

3. Le nom donné est encadré par `"`

#### Analyse du résultat de cet exemple. 
 
    colette:~/Racine$ ./Anciens_documents/Documents/Politique/Escroquerie à la taxe carbone.odt

En partant du répertoire courant on doit suivre la chaine de répertoires
`Anciens_documents/documents/Politique`. Dans le répertoire `Politique` on trouvera le fichier
`Escroquerie à la taxe carbone.odt`
