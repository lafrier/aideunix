# ffmpeg

Pour capturer, extraire, combiner des sources/pistes son et/ou video.

# Format des commanndes

Après `ffmpeg` chaque commande contient

 - éventuellement une liste d'options globales
 - une liste d'options concernant le premier flux d'entrée (typiquement le premier fichier d'entrée)
 - l'indicateur `-i` suivi du nom du premier flux d'entrée
 - éventuellement une liste d'options concernant le second flux d'entrée
 - l'indicateur `-i` suivi du nom du second flux d'entrée
 - ...
 - une liste d'options concernant le premier flux de sortie (typiquement le premier fichier de sortie)
 - le nom du premier flux de sortie (sans aucun indicateur)
 - éventuellement une liste d'options concernant le second flux de sortie
 - le nom du second flux de sortie
 - ...

# Afficher la structure d'un fichier

    lafrier$  ffmpeg -i <nom-du-fichier>

# Un exemple typique

On dispose d'une video prise par un téléphone au format `mp4` : le fichier s'appelle `input-video.mp4`. On dispose également d'une bande son au format `wav` obtenue à l'aide d'un outil de prise de son indépendant du téléphone : le fichier s'appelle `input-audio.wav`. On souhaite produire un fichier au format `mp4` combinant la video de `input-video.mp4` et le son de `input-audio.wav` : le nom du fichier obtenu sera `output.mp4`.

## Extraire la video du fichier `input-video.mp4`

    lafrier$  ffmpeg -i input-video.mp4 -map 0:v -c:v copy video-seule.mp4

Cela produit un fichier `video-seule.mp4` qui ne contient plus que la video (pas de son).

## Etirer la bande son pour ajuster sa longeur à celle de la video

Il se trouve que les deux appareils n'enregistrent pas exactement à la même vitesse. Il ne s'agit pas seulement d'un problème de synchronisation : si on synchronise les deux fichier en début de prise, alors l'audio est légèrement en retard sur la vidéo en fin de prise. Il faut donc légèrement étirer la prise de son. Le facteur d'étirement souhaité est 1.000436.

    lafrier$  ffmpeg -i input-audio.wav -filter:a "atempo=1.000436" audio-etire.wav

## Synchroniser la video et l'audio

La prise de son a été lancée 13.230 seconde après le début de la vidéo. Donc il faut donc décaller l'audio lors de la synchronisation avec la video.

    lafrier$  ffmpeg -itsoffset 00:00:13.230 -i audio-etire.wav -i video-seule.mp4 -vcodec copy output.mp4

# Couper une vidéo en deux partie

On suppose qu'on a un fichier vidéo `toto.mkv` et un fichier de sous-titres `toto-FRE.srt`. On repère la date de coupure, par exemple `01:00:00`. On ouvre le fichier de sous-titre, et on trouve la date du dernier sous-titre avant la coupure et on choisit une date de coupure légèrement avant, par exemple `00:59:39`. On lance alors les deux coupures :

    lafrier$  ffmpeg -i toto.mkv -map 0:v -map 0:a -map 0:s -c:v copy -c:a copy -c:s copy -t 01:00:00 toto-part1.mkv
    lafrier$  ffmpeg -ss 00:59:39 -i toto.mkv -map 0:v -map 0:a -map 0:s -c:v copy -c:a copy -c:s copy toto-part2.mkv
    lafrier$  ffmpeg -i toto-FRE.srt -c:s -t 01:00:00 copy toto-part1-FRE.srt
    lafrier$  ffmpeg -ss 00:59:39 -i toto-FRE.srt -c:s copy toto-part2-FRE.srt

Il se peut que les sous-titres de la seconde partie soient décallés. Dans ce cas, au besoin on recrée les sous-titres de la seconde partie pour qu'ils soient en avance, en changeant la date du `-ss`, par exemple avec `-ss 00:59:40`. Puis on fabrique un nouveau fichier de sous-titre en retardant celui qu'on vient de créer avec l'option `-itoffset`. Par exemple

    lafrier$  ffmpeg -itsoffset 0.4 -i toto-part2-FRE.srt -c:s srt toto-part2-correct-FRE.srt

# Enregistrer le micro

Trouver le nom du micro avec la commande

    pacmd list-sources

Par exemple `alsa_input.pci-0000_00_1b.0.analog-stereo`.

Capturer le son du micro choisi et enregistrer la capture dans le fichier `in.wav` :

    ffmpeg -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo in.wav

# Enregistrer le son entendu dans les écouteurs

Trouver le nom des écouteurs avec la même commande que pour le micro :

    pacmd list-sources

Par exemple `alsa_output.pci-0000_00_1b.0.analog-stereo.monitor`.

Capturer le son des écouteurs et enregistrer la capture dans le fichier `out.wav` :

    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor out.wav

# Enregistrer un mix du micro et des écouteurs

    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo -filter_complex "[0][1]amix[out]" -map "[out]" mix.wav

# Enregistrer le micro dans un fichier, les écouteurs dans un autre fichier, un mix dans un troisième fichier

    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 0 out.wav -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo -map 1 in.wav -filter_complex "[0][1]amix[out]" -map "[out]" mix.wav

# Couper une vidéo

ffmpeg -i output3.mkv -t 00:04:20 -c:v copy -c:a copy output-cut.mkv

ffmpeg -i output3.mkv -t 00:04:20 -c:v libx264 -c:a libfaac output-cut.mkv

Avec ss pour la date de début et t pour la durée à partir de cette date :

ffmpeg -i input.mp4 -ss 00:02:00 -t 00:07:28 part1.mp4 

ffmpeg -ss 00:00:30.0 -i input.wmv -c copy -t 00:00:10.0 output.wmv
ffmpeg -ss 30 -i input.wmv -c copy -t 10 output.wmv

# Rajouter une piste de sous-titre

ffmpeg -i <nom-du-fichier>.mkv -f srt -i <nom-du-fichier>.srt -map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy -c:s srt out.mkv 

ffmpeg -i "input.mkv" -i subtitles.srt -c copy -c:s srt -metadata:s:s:1 language=eng -map 0 -map 1 -map_metadata 0 output.mkv

ffmpeg -i input.mkv -ss 00:00:00 -t 00:01:35 -c copy -map 0:0 -map 0:1 -map 0:2 -qscale:0:V 0 -y out0.mkv

ffmpeg  -f concat -i mylist.txt  -c copy output.mkv


