# NOM

`mupdf` : afficher un fichier au format pdf

# UTILISATION

Pour ouvrir `toto.pdf` :

    mupdf toto.pdf

Pour recharger le fichier :

    r

Pour aller à la page 123

    123g

Pour aller à la première page

    g

Pour aller à la dernière page

    G
