
# Comment j'ai changé de système d'exploitation

Après le changement de mon ordinateur et plusieurs autres interventions
j'avais perdu toutes mes habitudes de gestion des fichiers que
je créais (courriers écrits avec mon clavier) tableaux excel etc.
Où étaient les dossiers et sous-dossiers dans lesquels je les avais
sauvegardés?
Par chance j'ai dans mon entourage des personnes dont
l'utilisation d'un ordinateur est le quotidien.
Devant mon désaroi, ils sont arrivé à l'idée que le système
d'exploitation Unix serait plus rationnel que le système Windows
que j'utilisais jusqu'alors.
Dans leurs échanges sur les comparaisons entre ces deux systèmes,
j'ai entendu le mot *arbre*. Je n'avais jamais pensé que le classement
de mes dossiers et fichiers était fait à partir d'un arbre.
Dans une première étape le système Unix allait d'abord me permettre
de *faire le relevé* de mon arbre actuel. 
Pour ce faire il fallait d'abord qu'une personne très compétente 
*installe une version du système Unix* sur mon ordinateur. 
J'ai dû ensuite commencer à utiliser les commandes de base dont 
il est question ci-après.

# Organisation d'un classement

## Notion d'arbre
						
Un *classement* s'organise sous la forme d'un arbre
(analogie avec un arbre généalogique).

Le départ de cet arbre est représenté par un seul slash (`/`).
De ce `/` part une première *branche* qui donne naissance à
une ou plusieurs autres branches etc.

Le point d'attache d'une branche à une autre est appelé un *noeud*.

Chacune de ces branches conduit

- à *1 fichier*

- ou à *1 répertoire* qui pourra contenir à son tour à la fois
des fichiers ou des répertoires.

## Fichiers et répertoires

Les fichiers sont des documents qui ne peuvent conduire à rien d'autre.
L'image d'un fichier pourrait être une *feuille* de l'arbre.
Un répertoire est un *noeud* de l'arbre. Ce répertoire peut conduire
à d'autres répertoires qui seront des *sous-répertoires* du précédent.
Il peut aussi conduire à des "feuilles" qui sont des *fichiers*.

Si `RepK` est un sous-répertoire de `Rep1`, alors `RepK` pourrait lui
aussi conduire à des fichiers et des sous-répertoires de `RepK`.

Dans un classement les noms des fichiers et répertoires sont écrits
*sans intervalle*. Ex: Si on décide d'appeler un répertoire
`"Affaire GIRARD George"`, on peut écrire ce nom

- soit `AffaireGirardGeorge`,

- soit `AffaireGIRARDGeorge`,

- soit `Affaire_GIRARD_George`,

- soit d'autres combinaisons de ce type.                
			
De plus	ces noms ne doivent contenir ni virgule, ni point, ni accent, etc.
Donc ils ne contiennent que des lettres minuscules ou majuscules,
des chiffres et des `_`. D'autres caractères sont possibles, mais il faut
faire attention car certains caractères sont réservés (ils sont
interprétés par le système d'une façon particulière) et celà peut être
source de confusion par la suite.

*Complément dans l'éciture d'un fichier :*
A l'exception des commandes dont on parlera plus loin, le nom de chaque
fichier a une *extension* du genre `.md`, `.pdf` ou `.jpg`.
Par exemple `AffaireGIRARDGeorge.pdf`

Ces extensions permettent de savoir quelle *commande* utiliser pour lire le fichier.

## Commandes de base

Lorsqu'on se trouve en présence d'un arbre on dispose de *commandes*
qui permettent de

- *gérer* des fichiers et répertoires existants :

    - `pwd` pour connaître le Répertoire dans lequel on se trouve
    sur l'arbre,

    - `cd` pour changer de Répertoire,
 
    - `ls` pour lister le contenu d'un Répertoire,

    - `mv` pour déplacer un fichier ou répertoire dans un autre
    répertoire ou changer leur nom,

- *créer* un nouveau Répertoire :

    - `mkdir`

- *touver le chemin* qu'on doit suivre pour trouver un
"fichier ou un répertoire perdu" :

    - `find`

Toutes ces commandes
 
- sont écrites en minuscules,

- sont des fichiers sans extension,

- sont utilisées suivies d'un "blanc" si nécessaire, notamment
lorsqu'on souhaitera faire appel à une option qui précisera la commande.

Pour chacune il y aura *une page man* qui expliquera son utilisation.

## Commandes spécifiques aux fichiers 

Ces commandes ont les mêmes propriétés que les précédentes mais
leur écriture se termine par le nom d'un fichier. 
Elles permettent d'*écrire des fichiers-textes*, de les *éditer*
et de les *lire*.

### Ecrire ou modifier un fichier-texte

Pour écrire ou modifier un fichier-texte (fichier écrit avec les cacatères du clavier) nous avons besoin d'un logiciel qui transforme notre
ordinateur en une "machine à écrire", ce que nous saisissons au clavier
étant enregistré dans un fichier. Un tel logiciel s'appelle un
*éditeur de texte*. 

La commande `nano` nous permet d'utiliser un éditeur de texte, 
très sommaire, mais qui existe sur tous les environnements unix.
Dans l'exemple qui suit, nous utilisons `nano` en lui indiquant
(après un blanc) que ce que nous saisissons au clavier doit être
enregistré dans un fichier appelé `FacturesEntretienAppart2019.md`.

    nano FacturesEntretienAppart2019.md

Si le fichier `FacturesEntretienAppart2019.md` existe déjà, nous pourrons
le modifier. S'il n'existe pas, il sera créé avec le contenu que nous
saisirons. On peut remarquer que dans cet exemple le fichier a une
extension `.md` ce qui signifie que nous comptons utiliser le format
"Mark Down".

### Créer et lire une page `man` à partir d'un fichier Mark Down

Admettons que nous ayons utilisé `nano` pour écrire un fichier `toto.md`
qui contient une description de la façon de se servir de la commande
`toto` (ce n'est qu'un exemple ... cette commande n'éxiste pas).

On voudrait alors créer une page `man` (une page de "manuel") à partir
du contenu de ce fichier (écrit au format Mark Down).
Pour cela on utilise la commande suivante :

    mkman toto.md

Si cette commande ne renvoie pas de message d'erreur, alors cela veut dire
qu'une page `man` a été effectivement créée pour la commande `toto`.

Pour consulter cette page `man` on utilse la commande suivante :

    man 9 toto

Le chiffre `9` entre `man` et `toto` signifie que nous voulons lire
la page `man` créée par nous-même, et non pas celle qui existe déjà
pour la commande `toto` (toutes les commandes existantes ont une page
de manuel). Si nous avions exécuté la commande `man toto` (sans le `9`),
nous aurions accédé à cette page de référence.
Attention ! Ne pas mettre l'extension `.md` quand on lit le manuel.

### Créer et lire une page `pdf` à partir d'un fichier Mark Down

Toujours en partant de notre fichier `toto.md`, on peut souhaiter créer
un fichier au format `pdf` grâce auquel on pourra voir l'effet des codes
Mark Down que l'on a choisi d'utiliser pour mettre en forme notre
document (titres, italiques, listes, etc).

Pour cela on utilise la commande suivante :

    mkpdf toto.md

Si cette commande ne renvoie pas de message d'erreur, alors cela veut dire 
qu'un fichier `toto.pdf` a été créé.

Pour ouvrir ce fichier `pdf`, on peut utiliser le logiciel `evince` avec la commande suivante :

    evince toto.pdf

### Lire un fichier-texte sans possibilité de modification

- `more`

- `less`

