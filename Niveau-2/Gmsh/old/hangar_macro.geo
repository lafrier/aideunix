// MACROS
// ------

// macro parallelepipede

Macro Parallelepipede
  
  Point(Np+1) = {-lx/2, -ly/2, -lz/2, lc};
  Point(Np+2) = {+lx/2, -ly/2, -lz/2, lc};
  Point(Np+3) = {+lx/2, +ly/2, -lz/2, lc};
  Point(Np+4) = {-lx/2, +ly/2, -lz/2, lc};
  Point(Np+5) = {-lx/2, -ly/2, +lz/2, lc};
  Point(Np+6) = {+lx/2, -ly/2, +lz/2, lc};
  Point(Np+7) = {+lx/2, +ly/2, +lz/2, lc};
  Point(Np+8) = {-lx/2, +ly/2, +lz/2, lc};

  Line(Nc+1) = {Np+1, Np+2};
  Line(Nc+2) = {Np+2, Np+3};
  Line(Nc+3) = {Np+3, Np+4};
  Line(Nc+4) = {Np+4, Np+1};

  Line(Nc+5) = {Np+5, Np+6};
  Line(Nc+6) = {Np+6, Np+7};
  Line(Nc+7) = {Np+7, Np+8};
  Line(Nc+8) = {Np+8, Np+5};

  Line(Nc+9) = {Np+1, Np+5};
  Line(Nc+10) = {Np+2, Np+6};
  Line(Nc+11) = {Np+3, Np+7};
  Line(Nc+12) = {Np+4, Np+8};

  Curve Loop(Nc+13) = {Nc+1, Nc+2, Nc+3, Nc+4};
  Curve Loop(Nc+14) = {Nc+5, Nc+6, Nc+7, Nc+8};
  Curve Loop(Nc+15) = {Nc+1, Nc+10, -(Nc+5), -(Nc+9)};
  Curve Loop(Nc+16) = {Nc+2, Nc+11, -(Nc+6), -(Nc+10)};
  Curve Loop(Nc+17) = {Nc+3, Nc+12, -(Nc+7), -(Nc+11)};
  Curve Loop(Nc+18) = {Nc+4, Nc+9, -(Nc+8), -(Nc+12)};

  Plane Surface(Ns+1) = { -(Nc + 13) };
  Plane Surface(Ns+2) = { +(Nc + 14) };
  Plane Surface(Ns+3) = { +(Nc + 15) };
  Plane Surface(Ns+4) = { +(Nc + 16) };
  Plane Surface(Ns+5) = { +(Nc + 17) };
  Plane Surface(Ns+6) = { +(Nc + 18) };

  Plane Surface(Ns+7) = { +(Nc + 13) };
  Plane Surface(Ns+8) = { -(Nc + 14) };
  Plane Surface(Ns+9) = { -(Nc + 15) };
  Plane Surface(Ns+10) = { -(Nc + 16) };
  Plane Surface(Ns+11) = { -(Nc + 17) };
  Plane Surface(Ns+12) = { -(Nc + 18) };

  Np = Np+8;
  Nc = Nc+18;
  Ns = Ns+12;

Return

// PARAMETRES
// ----------

Largeur = 1.e+0;
Profondeur = 1.e+0;
Hauteur = 1.e+0;
Epaisseur = 1.e-1;
N_fenetres = 4;

// INITIALISATION DES COMPTEURS
// ----------------------------

Np = 0;
Nc = 0;
Ns = 0;

// PHYSICAL SURFACES
// -----------------

BETON = Ns+1;
Physical Surface("BETON", BETON) = {};
VERRE = Ns+2;
Physical Surface("VERRE", VERRE) = {};
AIR_INT = Ns+3;
Physical Surface("AIR_INT", AIR_INT) = {};
S_BETON_INT = Ns+4;
Physical Surface("S_BETON_INT", S_BETON_INT) = {};
S_BETON_EXT_AIR = Ns+5;
Physical Surface("S_BETON_EXT_AIR", S_BETON_EXT_AIR) = {};
S_BETON_EXT_TERRE = Ns+6;
Physical Surface("S_BETON_EXT_TERRE", S_BETON_EXT_TERRE) = {};
S_VERRE_INT = Ns+7;
Physical Surface("S_VERRE_INT", S_VERRE_INT) = {};
S_VERRE_EXT = Ns+8;
Physical Surface("S_VERRE_EXT", S_VERRE_EXT) = {};

Ns = Ns + 7;

// SAISIE AVEC NUMEROTATION MANUELLE
// ---------------------------------

// parallelepipede interieur (sans la facade sud) 

lc = 1.e+1;
lx = Largeur;
ly = Profondeur;
lz = Hauteur;

ip_parallelepipede_interieur = Np;
ic_parallelepipede_interieur = Nc;
is_parallelepipede_interieur = Ns;

Call Parallelepipede;

Physical Surface(BETON) += {is_parallelepipede_interieur+1};
Physical Surface(BETON) += {is_parallelepipede_interieur+2};
Physical Surface(BETON) += {is_parallelepipede_interieur+4};
Physical Surface(BETON) += {is_parallelepipede_interieur+5};
Physical Surface(BETON) += {is_parallelepipede_interieur+6};

Physical Surface(AIR_INT) += {is_parallelepipede_interieur+7};
Physical Surface(AIR_INT) += {is_parallelepipede_interieur+8};
Physical Surface(AIR_INT) += {is_parallelepipede_interieur+10};
Physical Surface(AIR_INT) += {is_parallelepipede_interieur+11};
Physical Surface(AIR_INT) += {is_parallelepipede_interieur+12};

Physical Surface(S_BETON_INT) += {is_parallelepipede_interieur+1};
Physical Surface(S_BETON_INT) += {is_parallelepipede_interieur+2};
Physical Surface(S_BETON_INT) += {is_parallelepipede_interieur+4};
Physical Surface(S_BETON_INT) += {is_parallelepipede_interieur+5};
Physical Surface(S_BETON_INT) += {is_parallelepipede_interieur+6};

// parallelepipede exterieur (sans la facade sud)

lc = 1.e+1;
lx = Largeur + Epaisseur;
ly = Profondeur + Epaisseur;
lz = Hauteur + Epaisseur;

ip_parallelepipede_exterieur = Np;
ic_parallelepipede_exterieur = Nc;
is_parallelepipede_exterieur = Ns;

Call Parallelepipede;

Physical Surface(BETON) += {is_parallelepipede_exterieur+7};
Physical Surface(BETON) += {is_parallelepipede_exterieur+8};
Physical Surface(BETON) += {is_parallelepipede_exterieur+10};
Physical Surface(BETON) += {is_parallelepipede_exterieur+11};
Physical Surface(BETON) += {is_parallelepipede_exterieur+12};

Physical Surface(S_BETON_EXT_TERRE) += {is_parallelepipede_exterieur+7};
Physical Surface(S_BETON_EXT_AIR) += {is_parallelepipede_exterieur+8};
Physical Surface(S_BETON_EXT_AIR) += {is_parallelepipede_exterieur+10};
Physical Surface(S_BETON_EXT_AIR) += {is_parallelepipede_exterieur+11};
Physical Surface(S_BETON_EXT_AIR) += {is_parallelepipede_exterieur+12};

// fenetres rectangle interieur 

lc = 1.e+1;
lx = Largeur;
ly = Profondeur;
lz = Hauteur;

ip_fenetres_rectangle_interieur = Np;
ic_fenetres_rectangle_interieur = Nc;
is_fenetres_rectangle_interieur = Ns;

For i In {1:N_fenetres}
  Point(Np+1) = {-lx/2 + (2*i-1)*lx/(2*N_fenetres+1), -ly/2, -lz/2 + lz/3, lc};
  Point(Np+2) = {-lx/2 + (2*i)*lx/(2*N_fenetres+1), -ly/2, -lz/2 + lz/3, lc};
  Point(Np+3) = {-lx/2 + (2*i)*lx/(2*N_fenetres+1), -ly/2, -lz/2 + 2*lz/3, lc};
  Point(Np+4) = {-lx/2 + (2*i-1)*lx/(2*N_fenetres+1), -ly/2, -lz/2 + 2*lz/3, lc};
  Line(Nc+1) = {Np+1, Np+2};
  Line(Nc+2) = {Np+2, Np+3};
  Line(Nc+3) = {Np+3, Np+4};
  Line(Nc+4) = {Np+4, Np+1};
  Curve Loop(Nc+5) = {Nc+1, Nc+2, Nc+3, Nc+4};
  Plane Surface(Ns+1) = {Nc+5};
  Plane Surface(Ns+2) = {-(Nc+5)};
  Physical Surface(VERRE) += {Ns+1};
  Physical Surface(S_VERRE_INT) += {Ns+1}; 
  Physical Surface(AIR_INT) += {Ns+2};
  Np = Np + 4;
  Nc = Nc + 5;
  Ns = Ns + 2;
EndFor

// facade sud mur interieur

is_facade_sud_int = Ns;

liste_fenetres = {ic_parallelepipede_interieur + 15};
For i In {1:N_fenetres}
  liste_fenetres = {liste_fenetres[], ic_fenetres_rectangle_interieur + i*5};
EndFor
Plane Surface(Ns+1) = liste_fenetres[];
Plane Surface(Ns+2) = -liste_fenetres[];

Physical Surface(BETON) += {Ns+1};
Physical Surface(AIR_INT) += {Ns+2};
Physical Surface(S_BETON_INT) += {Ns+1};

Ns = Ns + 2;

// SAISIE AVEC NUMEROTATION AUTOMATIQUE

// fenetres rectangle exterieur

//For i In {1:N_fenetres}
  //out[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{is_fenetres_rectangle_interieur + (i-1)*2 + 2}; };
  //tmp[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{is_fenetres_rectangle_interieur + 2}; };
  //Printf("top surface = %g %g", tmp[0], is_fenetres_rectangle_interieur + 2);
  //out[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{is_fenetres_rectangle_interieur + 4}; };
  //Printf("top surface = %g %g", out[0], is_fenetres_rectangle_interieur + 4);
  //out3[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{is_fenetres_rectangle_interieur + 6}; };
  //Printf("top surface = %g %g", out3[0], is_fenetres_rectangle_interieur + 6);
  //out4[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{is_fenetres_rectangle_interieur + 8}; };
  //Printf("top surface = %g %g", out4[0], is_fenetres_rectangle_interieur + 8);
  //Physical Surface(VERRE) += {out[0]};
  //Physical Surface(S_VERRE_EXT) += {out[0]};
//EndFor

// facade sud mur exterieur



// fenetres

//liste_fenetres = {ic_parallelepipede_exterieur + 15};
//For i In {1:N_fenetres}
//  out[] = Extrude{0,-Epaisseur,0}{ Curve{ic_fenetres_rectangle_interieur + i*5}; };
//  liste_fenetres = {liste_fenetres[], out[???]};
//EndFor
//Plane Surface(Ns+1) = liste_fenetres[];

//out[] = Extrude{0,-Epaisseur,0}{ Curve{ic_fenetres_rectangle_interieur + 4}; };
//Printf("top curve = %g", out[0]);
//Printf("surface = %g", out[1]);
//Printf("side curves = %g and %g", out[2], out[3]);


//out[] = Extrude{0,-0.2*Epaisseur,0}{ Surface{21}; };
//Printf("top surface = %g", out[0]);
//Printf("volume = %g", out[1]);
//Printf("side surfaces = %g %g %g %g %g %g %g %g", out[2], out[3], out[4], out[5], out[6], out[7], out[8], out[9]);
//out2[] = Boundary{ Surface{out[0]}; }
//Printf("bords = %g", out2[0]);




//+
Hide "*";
//+
Show {
Point{17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};
Curve{37,38,39,40,42,43,44,45,47,48,49,50,52,53,54,55};
Surface{32,34,36,38};
}

