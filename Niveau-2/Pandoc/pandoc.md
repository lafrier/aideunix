# Commande pandoc

Permet de convertir des textes d'un format à un autre

# Configuration

lafrier$  mkdir -p ~/.pandoc/templates
lafrier$  pandoc -D latex > ~/.pandoc/templates/default.latex

# Les options de latex

lafrier$  pandoc --variable=lang:fr --variable=fontsize:12pt --variable=papersize:a4 --template=/home/lafrier/.pandoc/templates/default.latex --latex-engine=xelatex toto.md -o toto.pdf
