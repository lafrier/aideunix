# NOM

xrandr

# Exemples

Pour voir les différents écrants disponibles :

    lafrier$  xrandr

Avant toute tentative, commencer par relancer l'interface graphique. Ensuite, supposons que la commande `xrandr` renvoie les informations suivantes :

    Screen 0: minimum 320 x 200, current 1920 x 1080, maximum 16384 x 16384
    eDP-1 connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 344mm x 194mm
      3840x2160     60.00 +  59.98    59.97  
      3200x1800     59.96    59.94  
      2880x1620     59.96    59.97  
      2560x1600     59.99    59.97  
      2560x1440     59.99    59.99    59.96    59.95  
      2048x1536     60.00  
      1920x1440     60.00  
      1856x1392     60.01  
      1792x1344     60.01  
      2048x1152     59.99    59.98    59.90    59.91  
      1920x1200     59.88    59.95  
      1920x1080     60.01    59.97    59.96*   59.93  
      1600x1200     60.00  
      1680x1050     59.95    59.88  
      1400x1050     59.98  
      1600x900      59.99    59.94    59.95    59.82  
      1280x1024     60.02  
      1400x900      59.96    59.88  
      1280x960      60.00  
      1440x810      60.00    59.97  
      1368x768      59.88    59.85  
      1280x800      59.99    59.97    59.81    59.91  
      1280x720      60.00    59.99    59.86    59.74  
      1024x768      60.04    60.00  
      960x720       60.00  
      928x696       60.05  
      896x672       60.01  
      1024x576      59.95    59.96    59.90    59.82  
      960x600       59.93    60.00  
      960x540       59.96    59.99    59.63    59.82  
      800x600       60.00    60.32    56.25  
      840x525       60.01    59.88  
      864x486       59.92    59.57  
      700x525       59.98  
      800x450       59.95    59.82  
      640x512       60.02  
      700x450       59.96    59.88  
      640x480       60.00    59.94  
      720x405       59.51    58.99  
      684x384       59.88    59.85  
      640x400       59.88    59.98  
      640x360       59.86    59.83    59.84    59.32  
      512x384       60.00  
      512x288       60.00    59.92  
      480x270       59.63    59.82  
      400x300       60.32    56.34  
      432x243       59.92    59.57  
      320x240       60.05  
      360x202       59.51    59.13  
      320x180       59.84    59.32  
    DP-1 disconnected (normal left inverted right x axis y axis)
    DP-2 disconnected (normal left inverted right x axis y axis)
    DP-3 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 0mm x 0mm
      1920x1080     60.00*+  50.00    59.94    30.00    25.00    24.00    29.97    23.98  
      1920x1080i    60.00    50.00    59.94  
      1600x1200     60.00  
      1280x1024     75.02    60.02  
      1440x900      59.90  
      1280x800     119.91    59.91  
      1152x864      75.00  
      1280x720      60.00    50.00    59.94  
      1024x768     119.99    75.03    70.07    60.00  
      1024x576      59.97  
      832x624       74.55  
      800x600       72.19    75.00    60.32  
      720x576       50.00  
      720x480       60.00    59.94  
      640x480       75.00    72.81    66.67    60.00    59.94  
      720x400       70.08  

Normalement au lancement de `startx` les deux écrans `eDP-1` et `DP-3` devraient être visibles. On peut alors essayer de seulement positionner le second sur le premier :

    lafrier$  xrandr --output DP-3 --pos 0x0

On peut aussi essayer des choses du genre :

    lafrier$  xrandr --output eDP-1 --mode 1024x768 --output DP-3 --pos 0x0


