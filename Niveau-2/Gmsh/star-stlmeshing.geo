/* Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

Macro CreateMedium
    _frag() = BooleanFragments{ Volume{SHAPES_LIST()}; }{};    
    _next_id = news;
    For i In {0:#_frag() - 1}
        Physical Surface(_next_id + i) =  Unique(Abs(CombinedBoundary{ Volume{_frag(i)}; }));
    EndFor 
Return


Macro CreateBoundary
    _bound() = Unique(Abs(Boundary{ Volume{SHAPES_LIST()}; }));
  
    For i In {0:#_bound() - 1}
          _edge() += Unique(Abs(Boundary{Surface{_bound(i)};}));     
    EndFor

    _fuse_id = newv;
    BooleanUnion(_fuse_id) = { Volume{SHAPES_LIST(0)};}{Volume{SHAPES_LIST(1):SHAPES_LIST(#SHAPES_LIST() -1 )};};      
    _fuse_frag_id = newv;
    BooleanFragments(_fuse_frag_id) = { Volume{_fuse_id};}{ Curve{_edge(0):_edge(#_edge() - 1)}; };    
    
    _bound2() = Unique(Abs(Boundary{ Volume{_fuse_frag_id}; }));
      
    _next_id = news;
    For i In {0:#_bound2() - 1}
        Physical Surface(_next_id + i) = {_bound2(i)};
    EndFor    
Return


Macro CreateMesh
    Mesh.StlOneSolidPerSurface = 2;
    Mesh.StlAngularDeflection = 0.3;
    Mesh.StlLinearDeflection = 0.01;
    Save "./_tmpfile.stl";
    SystemCall StrCat("csplit -f ", StrCat(PREFIX_STL,"_Bound"), " -b %0d.stl -z _tmpfile.stl /^solid/ {*}");
    SystemCall StrCat("for i in ", StrCat(PREFIX_STL,"_Bound"), "*.stl; do admesh -d -v $i -a $i; done");
    SystemCall "rm ./_tmpfile.stl";
    medium_idmax = Sprintf("%g", #SHAPES_LIST() - 1);
    SystemCall StrCat("for i in {0..",medium_idmax,"}; do mv ", StrCat(PREFIX_STL,"_Bound"),"$i.stl ", PREFIX_STL,"_Medium$i.stl ; done");
Return  
