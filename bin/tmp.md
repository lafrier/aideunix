---
title: 'PREAMBULE A L''ORGANISATION D''UN CLASSEMENT'
...

# ORGANISATION D'UN CLASSEMENT

## Notion d'arbre
						
Un *classement* s'organise sous la forme d'un arbre (analogie avec un arbre généalogique).

Le départ de cet arbre est représenté par un seul slash (*/*).
De ce / part une première *branche* qui donne naissance à une ou plusieurs autres branches etc.

Le point d'attache d'une branche à une autre est appelé un *noeud*

Chacune de ces branches conduit

- à *1 fichier*

- ou à *1 répertoire* qui pourra contenir à son tour à la fois des fichiers ou des répertoires.

## Fichiers et répertoires

Les fichiers sont des documents qui ne peuvent conduire à rien d'autre. L'image d'un fichier pourrait
être une *feuille* de l'arbre.
Un répertoire est un *noeud* de l'arbre. Ce répertoire peut conduire à d'autres répertoires qui seront
des *sous-répertoires* du précédent. Il peut aussi conduire à des "feuilles" qui sont des *fichiers*

Si RepK est un sous-répertoire de Rep1, RepK pourrait aussi conduire à des fichiers et des sous-répertoires de RepK.

Dans un classement les noms des fichiers et répertoires sont écrits *sans intervalle*
Ex: Si on décide d'appeler un répertoire "Affaire GIRARD George", on peut écrire ce nom

- soit *AffaireGirardGeorge*

- soit *AffaireGIRARDGeorge*

- soit *Affaire\_GIRARD\_George*

- soit d'autres combinaisons de ce type                  
			
De plus	ces noms ne doivent contenir ni virgule, ni point, ni accent etc. donc ils ne contiennent que des lettres
 minuscules ou majuscules et des _.

*Complément dans l'éciture d'un fichier*
A l'exception des commandes dont on parlera plus loin, le nom de chaque fichier a une *extension* du genre .doc, .md ou .pdf.   
Exemple: AffaireGIRARDGeorge.pdf 

Ces extensions permettent de savoir quelle *commande* utiliser pour lire le fichier.

# COMMANDES DE BASE

Lorsqu'on se trouve en présence d'un arbre on dispose de *commandes* qui permettent de

- _Gérer_ fichiers et répertoires existants.

    - *pwd* pour connaître le Répertoire dans lequel on se trouve sur l'arbre.

    - *ls* pour lister le contenu d'un Répertoire.

    - *cd* pour changer de Répertoire.

    - *mv* pour déplacer un fichier ou répertoire dans un autre répertoire ou changer leur nom.

- _Créer_ un nouveau Répertoire.

    - *mkdir*

* _Touver le chemin_ qu'on doit suivre pour trouver un "fichier ou un répertoire perdu".

    - *find*

Toutes ces commandes
 
- sont écrites en minuscules

- sont des fichiers sans extension

- sont utilisées suivies d'un "blanc" si nécessaire.


Pour chacune il y aura *une page man* qui expliquera son utilisation.

# OUTILS de CREATION D'UN FICHIER TEXTE 

Ces "OUTILS" sont aussi des "COMMANDES" avec les mêmes propriétés que les précédentes 
mais elles permettent d'*écrire des fichiers-textes*, de les *éditer* et de les *lire*

- _Ecrire_ un fichier à un format choisi parmis plusieurs. 
Il a été choisi ici le fomat "Mark Down" d'où l'extension du fichier .md

*nano*	Après le "blanc" sera écrit le nom du fichier qu'on veut écrire.
Exemple: _nano FacturesEntretienAppart2019.md_ .
Ne pas oublier l'extension .md dans nano

- _Editer une page man_ de ce fichier

Pour le moment le fichier écrit est seulement dans l'ordinateur du créateur. 

*mkman* Permet de transmettre ce fichier au "système" de cet ordinateur.
Exemple: _mkman FacturesEntretienAppart2019_.Il ne faut pas écrire l'extension.

*man 9*	Le numéro 9 est la "page" du "manuel" choisie ici pour l'écriture.
Exemple: _man 9 FacturesEntretienAppart2019_. Permet de "lire" la "page man de FacturesEntretienAppart2019",
interprétée au format "Mark Down". Ne pas écrire ici non plus l'extension.

- _Editer le fichier en .pdf_ .Jusqu'à pésent le fichier a l'extension .md.

*mkpdf*	Transforme le fichier écrit en .md avec "nano" en fichier en .pdf. Exemple: mkpdf FacturesEntretienAppart2019.pdf.

*evince* Permet de lire ce fichier.pdf

##NOM

	cd (change directory)

	
##FONCTION


La commande cd permet de _changer de répertoire_


##UTILISATION

On se déplace sur l'arbre jusqu'au répertoire voulu _en suivant son chemin_ depuis l'endroit où on est (donné par la commande pwd)

Ce _chemin doit être écrit après la commande cd_s'il est nécessaire de l'écrire depuis le départ de l'arbre

ou de le compléter à partir de l'endroit où on se trouve. 

En reprenant l'exemple choisi dans le texte sur la commande pwd, le chemin du répertoire courant est /...Racine/Rep1/Rep2/

(Rappel: les 3 points après le */* de départ représentent des répertoires dont on ne parle parle pas ici)

On sera donc dans les _2 cas suivants_ dans le Répertoire Rep2

1° cas: on veut aller dans un sous-répertoire Rep3 de Rep2 puis de là dans un sous répertoire Rep4 de Rep3

	L'écriture de cd sera *cd Rep3/Rep4/* 		Il n'est pas nécessaire d'écrire ici un chemin à suivre

						 	puisqu'on est dans Rep2.

2° cas: On veut atteindre un autre sous-répertoire Repa de Rep1 puis un sous-répertoire Repb de Repa _on doit revenir_ à Rep1

	L'écriture de cd sera *cd ../Repa/Repb/*    	*../* est l'écriture d'une remontée de 1 niveau puisqu'on part de Rep2

							et qu'on doit être dans Rep1 pour rétablir le chemin.

	De même, si on veut atteindre un autre sous-répertoire RepA de Racine puis un sous-répertoire RepB de RepA

	en partant toujours de Rep2,_on doit remonter de 2 niveaux_pour se retrouver dans Racine et rétablir le chemin

	de RepA.

L écriture de cd sera *cd ../../RepA/RepB/*
##NOM

		mv	(mouvment)


##FONCTION

		La commande mv permet soit de changer le nom d'un ficher ou celui d'un répertoire
				      soit de changer le répertoire auquel appartient le fichier choisi ou celui auquel appartient
				      le sous-répertoire choisi


##UTILISATION

		On considère qu'on a suivi sur l'arbre le chemin suivant:

			/.../Racine/RepA/RepB/		RepB/ est le répertoire courant		

							/.../Racine/RepA/RepB/ s'appelle "CHEMIN ABSOLU" du répertoire RepB/

							Ce chemin nous a conduit devant RepB/

			fichx.doc est dans RepB/


		1° Changement de nom du fichier fichx.doc en fichy.doc sans changer le sous-répertoire auquel il appartient (ici RepB/)

			mv  ./fichx.doc  ./fichy.doc			./ (Répertoire courant) peut ne pas être écrit, on aura:

			mv  fichx.doc  fichy.doc		

		2° Changement de répertoire dans lequel se situe fichx.doc		

		
			fichx.doc est actuellement dans RepB/. 


			On veut que fichx.doc soit un fichier de Repb/		Repb/ est un sous-répertoire de Repa/ qui lui même est un
									        sous-répertoire de Racine/
									       
										Il faut donc revenir à Racine/ et suivre la branche qui
									        nous conduira à Repb/
										

			mv  fichx.doc  ../../Repa/Repb/				../../Repa/Repb/ s'appelle "CHEMIN RELATIF" du répertoire
									        qui a pour "NOM" Repb/
											

		3° Changement de nom d'un répertoire

			On veut changer le nom de RepB/ en RepK/

			On est toujours dans le cas où on a suivi le chemin absolu de RepB/ qui est /.../Racine/RepA/RepB/ et qui nous a
			conduit devant RepB/

			Il faut donc se placer devant RepA/ pour pouvoir changer le nom du sous répertoire RepB/ de RepA/ en RepK/

			cd  ../				

 			mv  ./RepB/  ./RepK/		( ou	 mv  RepB/  RepK/ )  


							Le chemin absolu de RepK/ est alors 	/.../Racine/RepA/RepK/


		4° Changement de chemin d'un répertoire

			On veut que RepB/ qui est un sous-répertoire de RepA/ soit un sous-répertoire de Racine/

			mv  /.../Racine/RepA/RepB/  /.../Racine/RepB/ 	 	Le changement de chemin du Répertoire RepB/ a été réalisé
									        ici avec l'écriture des chemins absolus. 

										Il peut être écrit aussi à l'aide des chemins relatifs.
							 			Cette possibilité dépend du répertoire courant du départ.
	





				



