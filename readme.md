# Introduction générale

`unix` est déjà très bien documenté, à tous les niveaux de formation. Ici, il est seulement question de partager des pratiques.

Au niveau 1, il s'agit de personnes ayant rencontré l'informatique depuis longtemps, mais ayant toujours ressenti que l'informatique est une "magie". Quand la magie fonctionne, tout va bien. Quand elle ne fonctionne plus, même pour une raison de détail, à la moindre modification d'une interface par exemple, c'est une souffrance. Se pose alors la question d'initier un acte d'éducation, de tenter collectivement de définir un espace informatique plus modeste (un ordinateur qui fait "moins de choses"), mais dans cet espace de poser *toutes* les bases, une à une et en profondeur, de façon à construire une confiance.

Au niveau 2, il s'agit de personnes ayant déjà cette confiance, mais souhaitant continuer à mutualiser une pratique minimum. Le but est de laisser de côté les petites préférences individuelles ("je prèfère `vim` à `emacs`", ou bien "l'interface de `mupdf` est môche", ou encore "pourquoi pas `jupyter notebook` plutôt que `markdown` ?") au profit du partage d'une couche d'`unix` couvrant tous les domaines, assurant une entrée dans tous les concepts essentiels. Encore une fois, quand on a réussi à entrer au contact du concept, la documentation existe et est bien faite. Ce sont les premières étapes qui sont difficiles. On essaie donc de construire un exemple de parcours possible pour chacune de ces "premières expériences".

La suite de cette introduction n'est accessible qu'aux personnes intéressées par le niveau 2. Celles intéressées par le niveau 1 auront besoin de l'aide de quelqu'un déjà expérimenté pour mettre en place les conditions informatiques dont il est question.

## Disposer d'un système `unix`

Actuellement, pour installer facilement un système d'exploitation de type `unix` on peut essentiellement retenir trois possibilités : installer une version de Linux sur une machine dédiée (préférable), utiliser le sous-système Windows pour Linux (si vous devez travailler sur une machine avec Windows 10), utiliser `UserLAnd` sur `Android` (si vous n'avez accès qu'à un téléphone ou une tablette).

### Installer un `unix` sur une machine dédiée

Si vous avez une machine que vous pouvez dédier à `unix`, vous pouvez par exemple suivre un totoriel d'installation d'une quelconque distribution de `Linux`. Nous ferons parfois référence à `Ubuntu` et à `Gentoo`, deux distributions très différentes, mais chcune très utile pour des raisons différentes (`Ubuntu` pour une installation facile, `Gentoo` pour une vraie formation à la gestion d'un système `unix`).

Sinon, si vous n'avez pas de machine disponible, une façon simple et peu coûteuse est d'acheter un `raspberry pi` et d'installer `raspbian`. 

### Installer un `unix` sur `Windows 10`

### Installer un `unix` sur `Android`

Vous pouvez installer `UserLAnd` et sélectionner `ubuntu` lorsqu'on vous demandera de choisir une distribution de `GNU/Linux`. Ensuite vous rentrez votre mot de passe et vous lancez les deux commandes suivantes :

    sudo apt-get update
    sudo apt-get upgrade

A ce stade, vous avez déjà un `unix` opérationnel et vous pouvez commencer à vous entrainer.

Vous pouvez ensuite installer `xsdl` (via le playstore).

## Télécharger `aideunix`

Choisir le répertoire où vous souhaitez installer (par exemple `/home/lafrier`) puis cloner `aideunix` avec la commande `git` :

    cd /home/lafrier
    git clone git@gitlab.com:lafrier/aideunix.git

Ensuite, créer une variable d'environnement `AIDEUNIX` qui pointe vers le répertoire `aideunix`, rajouter `$AIDEUNIX` au `MANPATH` et rajouter `$AIDEUNIX/bin` au `PATH`. Par exemple dans le fichier `~/.bashrc`, rajouter

    export AIDEUNIX=~/home/lafrier/aideunix
    export MANPATH=$MANPATH:$AIDEUNIX
    export PATH=$PATH:$AIDEUNIX/bin


