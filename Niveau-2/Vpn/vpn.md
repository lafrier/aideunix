# NOM

vpn

# Utilisation

Cartes trouvées à l'adresse suivante :

    https://freevpn.me/accounts/

Mettre dans un même répertoire le fichier ovpn et les fichiers contenus dans le répertoire `cert`. Créer un fichier `auth` dans le même répertoire et y mettre le login et le mot de passe :

    lafrier$  cd /home/lafrier/<nom-du-repertoire>
    lafrier$  echo "<username>" > auth
    lafrier$  echo "<password>" >> auth

Modifier le fichier ovpn en donnant l'adresse du fichier `auth` :

    lafrier$  cd /home/lafrier/<nom-du-repertoire>
    lafrier$  nano <nom-du-fichier>.ovpn
    nano:
      auth-user-pass /home/lafrier/<nom-du-repertoire>/auth

Lancer le vpn en tant que root et en allant dans le répertoire :

    root#  cd /home/lafrier/<nom-du-repertoire>
    root#  openvpn <nom-du-fichier>.ovpn

# Comprendre ce qui se passe

    lafrier$  curl ifconfig.co/json
    lafrier$  wget -qO- ifconfig.me/ip
    lafrier$  wget -qO- http://ipecho.net/plain
    lafrier$  curl http://ipecho.net/plain

Essayer de comprendre `tun0` tel que vu par `ifconfig -a` ...

Dans une navigateur, en allant sur http://test-ipv6.com/ on obtient des informations sur IPv4 et IPv6.

# Pour trouver des cartes vpn

    https://www.vpngate.net/en/do_openvpn.aspx?fqdn=vpn209962533.opengw.net&ip=50.39.112.244&tcp=1432&udp=1795&sid=1615736330763&hid=10174199
