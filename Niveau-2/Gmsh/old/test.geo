N=5;
R=3; // radius
r=2;  // radius
h=1/N;
// 1/ Points :

Point(1) = {0,0,0,h};
Point(2) = {R,0,0,h};
Point(3) = {0,R,0,h};
Point(4) = {0,0,R,h};

Point(11) = {r,0,0,h};
Point(12) = {0,r,0,h};
Point(13) = {0,0,r,h};
Circle(1) = {3, 1, 4};
Circle(2) = {3, 1, 2};
Circle(3) = {4, 1, 2};

Line Loop(4) = {1, 3, -2};
Surface(5) = {4};

