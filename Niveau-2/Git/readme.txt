Dans le fichier config de ssh :
Host gitlab
 IdentityFile ~/.ssh/id_rsa_gitlab
 Hostname gitlab.com
 User git
 IdentitiesOnly yes

Ensuite après avoir créé cours-bio sur le site de gitlab, la commande suivante crée le répertoire cours-bio :
git clone git@gitlab:lafrier/cours-bio.git

Pour enlever un fichier de la liste des fichiers indexés :
git rm --cached fichier




git clone git@gitlab.com:lafrier/radiation_in_clouds.git

git add README.md
(git status)
git commit -m "modification README.md"
(git log)
(git remote
  --> origin)
(git branch
  --> master)
git push origin master

git fetch origin
  
git commit --amend (commit en écrasant le dernier commit)




Avec Christophe, reprise à zero de star-engine (avec git) pour ensuite installer la librairie de gestion mémoire pour les raies -

Sous EDSTAR, renomer star-engine pour sauvegarde
git clone https://gitlab.com/meso-star/star-engine.git
cd star-engine/
mkdir build
cd build/
cmake ../cmake/
make


Sous un répertoire vierge :
git clone git@gitlab.com:meso-star/star_trdb.git
cd star_trdb/
mkdir build
cd build/
cmake ../cmake/ -DCMAKE_PREFIX_PATH="/home/lafrier/Transfert-local/Cora/EDSTAR/star-engine/local/lib/cmake/;/home/lafrier/Transfert-local/Cora/EDSTAR/star-engine/local/"
make

git pull




Hahah ben je vois deux solutions :

tu créés un nouveau dossier et tu clones de nouveau le projet de 0 :
> git clone git@gitlab.com:lafrier/radiation_in_clouds.git
tu récupèreras la dernière version pushée (la mienne donc)
tu peux ajouter ton debugage à la main aux fichiers que tu auras ainsi récupéré

ou

dans ton dossier où tu as debugué tu crées une branche git et tu te mets dessus :
> git checkout -b debug_trace
(équivalent aux deux commandes successives
> git branch debug_trace
> git checkout debug_trace
)
tu add et tu commit ta version débuguée
> git add src/*
> git commit -m "version debuguee"
et tu la push sur gitlab
> git push origin debug_trace
ça sauve l'état de ton projet sur le web
tu peux donc récupérer tranquillement ma version
d'abord tu te places dans la branche master locale (principale)
> git checkout master
puis tu pull le projet
> git pull origin master
la tu as récupéré ma version ; ta version est dans la branche debug_trace
et tu peux essayer de merger ta version débuguée sur ma version
> git merge debug_trace
mais tu vas avoir des conflits car les deux versions ont été modifiées par rapport à la version de base (ton dernier push avant que je fasse le mien)
que tu devras sans doute résoudre à la main

Bravo pour le debug en tout cas !
Naj


Comparaison de deux fichier :
meld file1 file2



git branch RF_branch
git checkout RF_branch
==> a partir de là tout se passe dans cette branche
git push


plus tard :
git checkout master
git merge RF_branch
git commit
git push
git 




