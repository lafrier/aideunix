# Installation du serveur

Voir l'installation de mariadb sur gentoo

# Création d'une base de données

lafrier$  mysql -u root -p -h localhost

mariadb>  create database lafrier;

mariadb>  show databases;

mariadb>  use lafrier;

mariadb>  create table film(

  id INT NOT NULL AUTO_INCREMENT,

  titreFrancais VARCHAR(255) NOT NULL,

  titreOriginal VARCHAR(255),

  nomAuteur VARCHAR(255) NOT NULL,

  prenomAuteur VARCHAR(255),

  annee INT(4),

  PRIMARY KEY (id));

mariadb>  show tables;

mariadb>  desc film;

mariadb>  alter table film MODIFY annee INT(4) NOT NULL;

mariadb>  desc film;

mariadb>  insert into film

  (titreFrancais, nomAuteur, prenomAuteur, annee)

    values

  ('Les Horizons morts', 'Demy', 'Jacques', 1952);

mariadb>  select * from film;

mariadb>  delete from film where id=1;

mariadb>  alter table film ADD COLUMN format VARCHAR(255) NOT NULL;

# Sauvegarde de la base de données

root#  mysqldump -u root -p lafrier > /home/lafrier/Transfert-local/Informatique/Sauvegardes/Sql/lafrier-20191102.sql
