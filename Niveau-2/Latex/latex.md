# Nom

`latex`

# Exemples

## Citer des lignes de code

Dans le texte : `\verb|code à citer|`

A la ligne :

    \begin{verbatim}
    première code à citer
    deuxième code à citer
    troisième code à citer
    \end{verbatim}

# Correction orthographique

    aspell --mode=tex -c -l en intro.tex
