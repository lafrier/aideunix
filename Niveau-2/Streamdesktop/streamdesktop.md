# Name

srtreamdesktop

# Configuration

    lafrier$  nano ~/.ssh/config
    nano:
      Host vps_meso-star
      Hostname 91.234.195.167
      User edstar
      IdentityFile ~/.ssh/id_rsa_nastar

# Utilisation

## Diffusion

Dans un premier shell :

    lafrier$  ssh -L 8000:localhost:8000 vps_meso-star

Dans un autre shell :

    lafrier$  ./streamdesktop lafrier

## Lecture

Serveur web : https://stream.meso-star.com

    lafrier$  mplayer https://stream.meso-star.com/lafrier.webm
