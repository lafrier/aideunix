# Nom

ssh

# Les clés ssh

Une machine AAA doit se connecter à une machine BBB.

Sur la machine AAA on lance la commande

    ssh-keygen

et on donne comme nom de fichier `/home/lafrier/.ssh/id_rsa_BBB` (avec un mot de passe). Cette commande produit deux fichiers : `id_rsa_BBB` et `id_rsa_BBB.pub`

Toujours sur la machine AAA, on édite le fichier `/home/lafrier/.ssh/config` et on ajoute

    Host BBB
    Hostname BBB.adresse.labo.lafrier.fr
    User lafrier
    Protocol 2
    ForwardAgent yes
    ForwardX11 yes
    IdentityFile ~/.ssh/id_rsa_BBB

Sur la machine BBB on recopie le contenu de `id_rsa_BBB.pub` dans le fichier `/home/lafrier/.ssh/authorized_keys`

Ensuite la machine AAA peut se connecter à la machine BBB avec la commande

    ssh BBB

# Rentrer le mot de passe une seule fois

exec ssh-agent bash

ssh-add ~/.ssh/id_rsa_BBB

# `sshfs`

Permet à un utilisateur (sur le client, e.g. AAA) de monter un répertoire distant (sur le serveur, e.g. BBB).

Commencer par ouvrir le fichier `/etc/fuse.conf` sur AAA et décommenter la ligne

    user_allow_other

Sur la machine AAA, créer un répertoire où sera monté le répertoire distant, par exemple

    lafrier$  mkdir /home/lafrier/tmp/montage

On suppose que sur le serveur, le répertoire que l'on souhaite monter est `/home/lafrier/test` et que l'ID et le GID de l'utilisateur `lafrier` et de son groupe principal sont repsectivement `3156` et `2000`. On crée alors deux fichiers sur la machine AAA :

    lafrier$  nano /home/lafrier/.ssh/uidfile-BBB
    nano:
    root:0
    lafrier:3156
    lafrier$  nano /home/lafrier/.ssh/gidfile-BBB
    nano:
    root:0
    lafrier:2000

On peut bien sûr rajouter d'autre utilisateurs ou groupes distants.

La commande de montage est alors (executée sur la machine AAA)

    lafrier$  sshfs BBB:/home/lafrier/test /home/lafrier/tmp/montage \
    -o idmap=file,uidfile=/home/lafrier/.ssh/uidfile-BBB,gidfile=/home/lafrier/.ssh/gidfile-BBB \
    -o allow_other,sshfs_sync,sync_readdir,no_readahead,cache=no

La commande de démontage est

    lafrier$  fusermount -u /home/lafrier/tmp/montage

ATTENTION ! Si un fichier distant est propriété de root, une fois monté il est bien propriété de root (grace au uidfile), mais l'utilisateur peut effacer ce fichier comme si il en était propriétaires.

# `sshfs` dans `/etc/fstab`

    root#  mkdir /mnt/test-BBB
    root#  chown lafrier /mnt/test-BBB
    root#  chgrp lafrier /mnt/test-BBB
    root#  nano /etc/fstab
    nano:
    BBB:/home/lafrier/test                            /mnt/test-BBB       fuse.sshfs            defaults,noauto,user,idmap=file,uidfile=/home/lafrier/.ssh/uidfile-BBB,gidfile=/home/lafrier/.ssh/gidfile-BBB,noatime,allow_other,sshfs_sync,sync_readdir,no_readahead,cache=no,default_permissions,_netdev     0 0

