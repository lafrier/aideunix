#!/bin/bash

set -e

if [ "$BASH_VERSION" = "" ];then
  echo "This sctipt must be run with Bash, try 'bash $0'";
  exit 1 
fi 

#    Copyright (C) 2020  Ruben Rodriguez <ruben@gnu.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# A simple script to capture your desktop and microphone into a file and an icecast2 streaming server

# Streamer side dependencies: wmctrl gstreamer1.0-tools gstreamer1.0-plugins-good gstreamer1.0-pulseaudio pulseaudio ffmpeg
# sudo apt-get install wmctrl gstreamer1.0-tools gstreamer1.0-plugins-good gstreamer1.0-pulseaudio pulseaudio ffmpeg

# Server side dependencies: icecast2

if ! [ -f config ]; then
  echo Copy ./config.example into ./config, and edit it to set up your server, password, and other parameters
  exit 1
fi
. config

DATE=$(date +%Y-%m-%d-%H_%M_%S)
MOUNT=$1
[ "$MOUNT" = "" ] && MOUNT=recording

cat << EOF
Usage: $0 [title]
The title parameter will be used for the name of the mount, and the name of the file saved:
  http://$SERVER/$MOUNT.webm
  File saved locally to ./$MOUNT-${DATE}.webm
  Log saved to $MOUNT-${DATE}.log

AUDIO INPUT: this script will record and stream the audio from the default input selection in Pulseaudio. Click on the volume icon in your desktop environment, go to settings, then input, and select the appropriate device and input level.

SCREEN SELECTION. Note: broadcasting a single window may crash if you minimize or resize the screen after the streaming starts! If you have issues with this, select 0 to broadcast the full screen, but remember to close or hide any privacy-sensitive windows." If the stream is just an empty black square, make sure your desktop is running X11, not Wayland or others. 

0: Capture FULL SCREEN
EOF

# Window selector
WINDOWLIST="$(wmctrl -l)"
i=1
echo "$WINDOWLIST" |while read line
do
  echo -n "$i: "
  name=$(echo $line| cut -d' ' -f 4-)
  echo $name
  i=$(expr $i + 1)
done

echo
echo "Select the window to capture, or press enter to capture the whole screen"
read -s -n1 SELECTION

XID=$(echo "$WINDOWLIST" | sed -n "${SELECTION}p" | sed 's/ .*//')

if [ "$SELECTION" = 0 ] || [ "$SELECTION" = "" ] ; then
  XPARAM=""
else
  XPARAM="xid=$XID"
fi


# The video needs to be processed after recording
trap ctrl_c INT
ctrl_c() {
  echo
  echo -n "Remuxing streamed video... "
  mv $MOUNT-${DATE}.webm $MOUNT-${DATE}.webm.bak
  ffmpeg -i $MOUNT-${DATE}.webm.bak -acodec copy -vcodec copy $MOUNT-${DATE}.webm >> $MOUNT-${DATE}.log 2>&1
  echo done.
  echo
  echo Use $MOUNT-${DATE}.webm for publishing, you can delete $MOUNT-${DATE}.webm.bak
  echo
  echo "You can trim the beginning and end of the video with this command (replace the timestamps, HH:MM:SS):"
  echo   ffmpeg -i $MOUNT-${DATE}.webm -acodec copy -vcodec copy -ss 00:19:27 -to 02:18:51 trimmed.webm
}


echo STARTING STREAM in 2 seconds. Press CTRL+C to end the session.
sleep 2
echo Streaming at http://$SERVER/recording.webm
echo

# GSTREAMER 1.0 pipeline for capture, save to file, and stream to icecast2
#gst-launch-1.0 --eos-on-shutdown ximagesrc use-damage=false $XPARAM show-pointer=$SHOWPOINTER \
#! videorate ! video/x-raw,framerate=$FRAMERATE/1 \
#! videoscale ! video/x-raw, width=$WIDTH, height=$HEIGHT \
#! videoconvert ! queue !  vp8enc target_bitrate=$BITRATE cpu-used=0 deadline=1 threads=4 keyframe-max-dist=15 ! queue ! mux. \
#pulsesrc ! vorbisenc  quality=$SOUNDQUALITY ! queue ! mux. \
#webmmux streamable=true name=mux ! tee name=stream ! queue ! filesink location=$MOUNT-${DATE}.webm sync=false \
#stream. ! queue max-size-time=0 max-size-bytes=135528448 max-size-buffers=0 ! shout2send ip=$SERVER port=$PORT mount=$MOUNT.webm password=$PASSWORD sync=false \
#2>&1 | tee $MOUNT-${DATE}.log

gst-launch-1.0 --eos-on-shutdown ximagesrc use-damage=false $XPARAM show-pointer=$SHOWPOINTER \
! videorate ! video/x-raw,framerate=$FRAMERATE/1 \
! videoscale ! video/x-raw, width=$WIDTH, height=$HEIGHT \
! videoconvert ! queue !  vp8enc target_bitrate=$BITRATE cpu-used=0 deadline=1 threads=4 keyframe-max-dist=15 ! queue ! mux. \
webmmux streamable=true name=mux ! tee name=stream ! queue ! filesink location=$MOUNT-${DATE}.webm sync=false \
stream. ! queue max-size-time=0 max-size-bytes=135528448 max-size-buffers=0 ! shout2send ip=$SERVER port=$PORT mount=$MOUNT.webm password=$PASSWORD sync=false protocol=http \
2>&1 | tee $MOUNT-${DATE}.log

