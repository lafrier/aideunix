# x11vnc

Serveur vnc : pour prendre la main sur un serveur x distant.

# Uilisation

Créer un mot de passe (`toto`) :

    lafrier$  x11vnc -storepasswd "toto" ~/.vnc_passwd

Lancer le serveur :

    lafrier$  x11vnc -many -rfbauth ~/.vnc_passwd -xkb
