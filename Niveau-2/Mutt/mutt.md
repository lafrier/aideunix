# NAME

mutt

# Lecture des mails

## Aller à la fin

\*

## Créer un alias

a

## Changer de répertoire

c

## Ne voir que les mails dont l'auteur contient toto

l toto

l All

===========================
~A              tous les messages
~b EXPR         messages contenant EXPR dans leur corps
~B EXPR         messages contenant EXPR (intégralité du message)
~c USER         messages avec une copie carbone (cc:) pour USER
~C EXPR         message est destiné à (to:) ou en copie (cc:) pour EXPR
~D              messages effacés
~d [MIN]-[MAX]  messages avec ``date-d-envoi'' dans l'intervalle Date
~E              messages arrivés à expiration
~e EXPR         message contenant EXPR dans le champ `'Sender''
~F              messages marqués important
~f USER         messages provenant de USER
~g              messages signés (PGP)
~G              messages chiffrés (PGP)
~h EXPR         messages contenant EXPR dans leurs en-têtes
~k              message contenant une clé PGP
~i ID           message avec ID dans le champ ``Message-ID''
~L EXPR         message provenant de ou reçu par EXPR
~l              message adressé à une liste de diffusion connue
~m [MIN]-[MAX]  message dans l'intervalle MIN à MAX *)
~n [MIN]-[MAX]  messages avec un score dans l'intervalle MIN à MAX *)
~N              nouveaux messages
~O              anciens messages
~p              message qui vous est adressé (voir $alternates)
~P              message que vous avez envoyé (voir $alternates)
~Q              messages auxquels vous avez répondu
~R              messages lus
~r [MIN]-[MAX]  messages avec `'date-reception'' dans l'intervalle Date
~S              messages remplacés (supersede)
~s SUBJECT      messages ayant SUBJECT dans l'objet (champ ``Subject'').
~T              messages marqués (tag)
~t USER         messages adressés à USER
~U              messages non lus
~v              message faisant partie d'une enfilade repliée
~x EXPR         messages contenant EXPR dans le champ `References'
~y EXPR         messages contenant EXPR dans le champ `X-Label'
~z [MIN]-[MAX]  messages avec une taille dans l'intervalle MIN à MAX *)
~=              doublons (voir $duplicate_threads)
===========================

## Ecrire un mail

m

## Répondre à un mail

r

## Répondre à tous

g

## Transférer un mail

f

# Archiver des mails

## Archiver un seul mail

s

et saisir le nom du dossier après le signe =

## Archiver plusieurs mails

tagger chaque mail avec

t

puis

;s

et saisir le nom du dossier après le signe =

