# NAME

git

# CONFIGURATION

## COMPTES MULTIPLES

Premier compte gitlab : lafrier@gmail.com avec la clé ssh id_rsa_gitlab 

Exemple de clone premier compte : git clone git@gitlab:lafrier/spheres.git

Configuration après clone premier compte :

  git config user.name lafrier@gmail.com

  git config user.email lafrier@gmail.com

  git remote set-url origin git@gitlab:lafrier/spheres.git

Second compte gitlab : lafrier.bis@gmail.com avec la clé ssh id_rsa_gitlab_google 

Exemple de clone second compte : git clone git@gitlab_google:lafrier.bis/partage.git

Configuration après clone second compte :

  git config user.name lafrier.bis@gmail.com

  git config user.email lafrier.bis@gmail.com

  git remote set-url origin git@gitlab_google:lafrier.bis/partage.git

# UTILISATION

## Ecraser la branche toto et repartir avec le contenu de master

git checkout toto

git fetch

git pull --rebase origin master

git commit -m 'après le rebase'

git push origin toto

# EXEMPLES


git status

git add fichier_a_ajouter

git add --all

pour tout ajouter

git commit -m 'description'

git push origin master

Pour voir les branches existantes :

git branch

Pour créer une branche toto et passer dessus :

git branch toto

git checkout toto

Pour récupérer les informations du serveur sans changer le répertoire de travail :

git fetch

Pour merger avec la branche origin/toto :

git merge origin/toto

Pour régler les conflits du merge :

git mergetool

Pour définir les outils de merge et de diff (par exemple kdiff3) :

git config --global --add merge.tool kdiff3

git config --global --add diff.guitool kdiff3

pour cloner un projet existant :

git clone git@gitlab.com:proprietaire/projet_existant.git

pour récupérer le contenu de la branche origin/toto en local sur toto

git checkout toto

git pull origin toto

pour l'exemple Clément

git fetch

git merge

git push origin master


