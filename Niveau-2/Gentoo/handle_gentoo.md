<!-- =================================
     Gestion d'une machine sous gentoo
     ================================= -->

# Avant toute chose ...

## Installation de `aideunix`

    root#  cd
    root#  git clone https://gitlab.com/lafrier/aideunix.git
    root#  mv aideunix /usr/share/.
    root#  nano ~/.bashrc 
    nano:
      export AIDEUNIX=/usr/share/aideunix
      export MANPATH=$MANPATH:$AIDEUNIX
      export PATH=$PATH:$AIDEUNIX/bin

RQ: La présente documentation est sous `$AIDEUNIX/Niveau-2/Gentoo/handle_gentoo.md`.

## Mise à jour de `aideunix`

    root#  cd $AIDEUNIX
    root#  git fetch
    root#  git pull
    root#  source /root/.bashrc

## Accès des users à `aideunix`

    lafrier$  nano ~/.bashrc 
    nano:
      export AIDEUNIX=/usr/share/aideunix
      export MANPATH=$MANPATH:$AIDEUNIX
      export PATH=$PATH:$AIDEUNIX/bin
    lafrier$  source ~/.bashrc

<!-- ====================================================================== -->

# openrc

`openrc` est le système d'initialisation des services utilisé par `gentoo`. Les caractéristiques de ce systèmes d'initialisation, notamment sa simplicité, sont parmis les principales raisons pour lesquelles nous choisissons d'utiliser la distribution `gentoo`.

## Lister les services

    lafrier$  rc-update show -v
    lafrier$  rc-status --servicelist

<!-- ====================================================================== -->

# Gestion du noyau

## Archiver le noyau courant

    root#  cp /usr/src/linux/.config $KERNELS/config-`uname -r`-`date +%Y%m%d%H%M%S`

## Enlever genkernel

Si on a utilisé `genkernel` pour l'installation, ce qui n'est pas recommandé, on commence par l'enlève. 

    root#  emerge --unmerge -av sys-kernel/genkernel
    root#  emerge -av --depclean

## Le fichier de configuration du noyau

Le fichier définissant le noyau est `/usr/src/linux/.config`. Quand ce fichier a été modifié, il faut executer les commandes suivantes :

    root#  cd /usr/src/linux
    root#  make menuconfig

RQ: Ne rien changer dans `menuconfig`

    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

RQ: Il est parfois utile de commencer par `make clean` sous `/usr/src/linux`

## Comparer deux noyaux

    root#  /usr/src/linux/scripts/diffconfig $KERNELS/config-<reférence 1> $KERNELS/config-<reférence 2>

## Repartir avec un noyau archivé

    root#  cp /usr/src/linux/.config $KERNELS/config-`uname -r`-`date +%Y%m%d%H%M%S`
    root#  cp $KERNELS/config-<reférence choisie> /usr/src/linux/.config
    root#  cd /usr/src/linux
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

RQ: Il est parfois utile de commencer par `make clean` sous `/usr/src/linux`

## Comparer le noyau actuel avec le noyau par défaut

Archiver le noyau courant.

    root#  cp /usr/src/linux/.config $KERNELS/config-`uname -r`-`date +%Y%m%d%H%M%S`

Déplacer temporairement le noyau courant.

    root#  mv /usr/src/linux/.config /usr/src/linux/.config-actuel

Créer le noyau par défaut.

    root#  cd /usr/src/linux
    root#  make defconfig

Archiver le noyau par défaut.

    root#  cp /usr/src/linux/.config $KERNELS/config-`uname -r`-default-`date +%Y%m%d%H%M%S`


    root#  /usr/src/linux/scripts/diffconfig  /usr/src/linux/.config /usr/src/linux/.config-actuel

Restaurer le noyau courant.

    root#  mv /usr/src/linux/.config-actuel /usr/src/linux/.config

## Activation d'une nouvelle version du noyau après une mise à jour système 

Lorsque portage a mis à jour le noyau, commencer par archiver le fichier `.config` de l'ancien noyau. Ensuite

    root#  eselect kernel list
    root#  eselect kernel set numero_du_noyau
    root#  cd linux
    root#  make menuconfig
    menuconfig:
      exit

Cela est équivalent à faire

    root#  cd /usr/src
    root#  rm linux
    root#  ln -s <nouveau noyau> linux
    root#  make menuconfig
    menuconfig:
      exit

RQ: Faire le `eselect kernel set numero_du_noyau` même si il n'y en a qu'un seul, donc `eselect kernel set 1`.

Recompiler (et rebooter pour tester) :

    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

Si besoin, mettre à jour nvidia :

    root#  emerge -av nvidia-drivers

<!-- ====================================================================== -->

# portage

Si besoin, mettre à jour portage lui-même :

    root#  emerge -av --oneshot sys-apps/portage

## Gerer les problemes de `accept_keyword`, `license` et `use`

Quand une modification de `accept_keyword`, `license` ou `use` est suggérée lors de l'installation d'un paquet :

- refuser la modification (répondre non),

- créer un fichier portant le nom du paquet dans celui de ces trois répertoires qui est concerné (si le fichier existe le compléter),

- recopier dans ce fichier ce qui est suggéré,

- relancer l'installation du paquet.

Si on doute de la syntaxe, accepter la modification. Un fichier est alors créé qui porte par exemple le nom `._cfg0000_zz-autounmask`. Prendre les lignes qui sont dans ce fichier pour créér des fichiers portant le nom de chacun des paquets concernés. Ensuite effacer `._cfg0000_zz-autounmask`. Bien sûr, comme au dessus, il faut ensuite relancer l'installation.

## Langue française

    root#  nano /etc/portage/make.conf
    nano:
      L10N="fr"

## Le profile

Pour voir le `profile` utilisé :

    lafrier$  eselect profile list

et on regarde le `profile` marqué d'une étoile. Typiquement, on obtient

    [1]   default/linux/amd64/17.1 (stable) *

ou bien

    [5]   default/linux/amd64/17.1/desktop (stable) *

Pour changer le `profile` :

    root#  eselect profile set numéro_du_profile

## Mise à jour du système

    root#  emerge --sync
    root#  emerge -uvaDUN @world

Si un nouveau noyau est disponible, basculer dessus :

    root#  eselect kernel list
    root#  eselect kernel set numero_du_noyau
    root#  cd linux
    root#  make menuconfig
    menuconfig:
      exit
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

Puis enlever les paquets qui ne sont plus utilisés :

    root#  emerge -av --depclean

## Installation d'un paquet

    root#  emerge -av <nom de la famille>/<nom du paquet>

## Mettre à jour les fichiers de configuration

Lorsqu'on essaye d'installer un paquet, `portage` renvoie parfois un message du type `"IMPORTANT: 5 config files in /etc need updating"`. On utilise alors `etc-update` pour mettre à jour les fichiers de configuration.

    root#  etc-update
    etc-update:
    <Entrer un numéro de fichier de config>
    <Lire les diff et sortir avec q>
    <Choisir dans la liste des actions de mise à jour possibles>
    <!! Refuser les - qui correspondent à des configurations que l'on avait choisi de faire précédemment (par exemple en recherchant dans le présent fichier et dans le fichier d'install)>

## Quel use est utilié par un paquet ?

    root#  equery uses paquet

## Quand on change les use

    root#  emerge --update --changed-use --deep --ask --verbose @world

## Les news (c'est grave !)

    root#  eselect news list

Ne jamais purger les news. On garde toute l'histoire. La convention est que si une news a été lue, alors est est traitée. Donc si on a lu une news et que l'on n'a pas eu le temps de la traiter, alors il faut la retransformer en "non lue".

Pour lire une news :

    root#  eselect news read numéro_de_la_news

Pour marquer une new comme "non lue" quand elle est importante :

    root#  eselect news unread numéro_de_la_news

## Chercher un paquet dont le nom contient toto

    lafrier$  emerge --search toto

## Chercher le paquet auquel appartient le fichier toto

    lafrier$  equery belongs -e toto

## Tester si toto/titi est installé

    root#  emerge -p toto/titi

## Voir si toto/tata est utilisé par un autre paquet

    lafrier$  equery d toto/tata

## Enlever toto/tata

    root#  emerge --unmerge -av toto/tata
    root#  emerge -av --depclean

## Gerer les problèmes de paquets masqués

A REVOIR !!

Si une allerte est faite lors d'une mise à jour, disant qu'un certain paquet est masqué, alors faire la mise à jour et ensuite le désinstaller.

Les paquets suivants ont été désinstallé de cette façon :
- emerge --ask --verbose --depclean virtual/pam (pas réussi)

<!-- ====================================================================== -->

# user (gestion des utilisateurs)

    root#  useradd -m -G users,wheel,audio,video -s /bin/bash lafrier
    root#  passwd lafrier

## Ajouter `lafrier` à groupe_toto

    root#  gpasswd -a lafrier groupe_toto

## Lister les groupes de `lafrier`

    lafrier$  groups lafrier

# sudo

    root#  emerge -av app-admin/sudo
    root#  hostname

Ici, `hostname` renvoie `cpa155`

    root#  visudo
    visudo:
      lafrier cpa155 = /sbin/shutdown -h now, /sbin/reboot

RQ: Ne pas avoir peur du fait que c'est sudoers.tmp qui est édité. Sauvegarder normalement en laissant ce nom et ce sera bien sudoers qui sera modifié.

<!-- ====================================================================== -->

# usb

## Montage d'une clé par un user

    root#  gpasswd -a lafrier usb

Regarder quelles lettres sont utilisées par défaut (sans clé) après `sd` dans `/dev` :

    root#  ls /dev/sd*

Si seulement `sda` est utilisé, alors le système installera la première clé usb sous `/dev/sdb`, la deuxième sous `/dev/sdc`, etc. Si `sda` et `sdb` sont utilisés, alors le système installera la première clé usb sous `/dev/sdc`, la deuxième sous `/dev/sdd`, etc.

Préparer les répertoire dont vous avez besoin pour monter les clés. Par exemple :

    root#  mkdir /mnt/sda1
    root#  mkdir /mnt/sdb1
    root#  mkdir /mnt/sdc1
    root#  nano /etc/fstab
    nano:
      /dev/sda1   /mnt/sda1   auto   noauto,user   0 0
      /dev/sdb1   /mnt/sdb1   auto   noauto,user   0 0
      /dev/sdc1   /mnt/sdc1   auto   noauto,user   0 0

La commande `mount-sdc1` (voir `aideunix`) montera sous `/mnt/sdc1` la première partition de la clé placéee sous `/dev/sdc` si il y en a une.

# sdcard

## Trouver le lecteur

    root#  lspci | grep -i card

Dans mon cas :

    3c:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS525A PCI Express Card Reader (rev 01)

## Noyau

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        Misc devices  --->
	  <*> Realtek PCI-E card reader
        <*> MMC/SD/SDIO card support  --->
	  <*>   Realtek PCI-E SD/MMC Card Interface Driver
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

## Détecter la carte

Sans insérer la carte :

    root#  lsblk -p

Après avoir inséré la carte :

    root#  lsblk -p

La différence entre les deux permet de connaitre le nom de la carte. Typiquement :

    /dev/mmcblk0                     179:0    0    58G  0 disk  
    └─/dev/mmcblk0p1                 179:1    0    58G  0 part  

<!-- ====================================================================== -->

# cifs

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      File systems  ---> 
        [*] Network File Systems  ---> 
          <*>   SMB3 and CIFS support (advanced network filesystem)
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

# Diffie-Hellman

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Security options  --->
        -*- Enable access key retention support
        [*]   Diffie-Hellman operations on retained keys
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

# ipv6

Il est parfois utile de pouvoir désactiver `IPv6` pour être certain de n'utiliser que `IPv4`. Pour cela il faut d'abord s'assurer que le noyau est compilé avec `IPv6` comme module :

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Networking Support --->
        Networking Options --->
          <M>The IPv6 protocol --->
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg

Ensuite, on modifie le fichier `/etc/modprobe.d/aliases.conf`, ou plutôt on en fait deux pour avec et sans `IPv6` et on utilise un lien pour choisir ce qu'on veut :

    root#  cd /etc/modprobe.d/
    root#  cp aliases.conf aliases.conf-avec-ipv6
    root#  cp aliases.conf aliases.conf-sans-ipv6
    root#  nano aliases.conf-sans-ipv6
    nano:
      # Uncomment the network protocols you don't want loaded:
      # alias net-pf-1 off		# Unix
      # alias net-pf-2 off		# IPv4
      # alias net-pf-3 off		# Amateur Radio AX.25
      # alias net-pf-4 off		# IPX
      # alias net-pf-5 off		# DDP / appletalk
      # alias net-pf-6 off		# Amateur Radio NET/ROM
      # alias net-pf-9 off		# X.25
      alias net-pf-10 off		# IPv6
      alias ipv6 off
      # alias net-pf-11 off		# ROSE / Amateur Radio X.25 PLP
      # alias net-pf-19 off		# Acorn Econet
    root#  rm aliases.conf
    root#  ln -s aliases.conf-sans-ipv6 aliases.conf
    root#  reboot

# wifi

## Nouvelle connexion wifi

Dans l'exemple qui suit, `0` sera remplacé par le numéro de la nouvelle connexion wifi.

    root#  wpa_cli
    wpa_cli:
      > scan
      OK
      <3>CTRL-EVENT-SCAN-RESULTS
      > scan_results
      bssid / frequency / signal level / flags / ssid
      00:00:00:00:00:00 2462 -49 [WPA2-PSK-CCMP][ESS] MYSSID
      11:11:11:11:11:11 2437 -64 [WPA2-PSK-CCMP][ESS] ANOTHERSSID
      > add_network
      0
      > set_network 0 ssid "MYSSID"
      > set_network 0 psk "passphrase"
      > enable_network 0
      <2>CTRL-EVENT-CONNECTED - Connection to 00:00:00:00:00:00 completed (reauth) [id=0 id_str=]
      > save_config
      OK
      > quit

    root#  rc-service wpa_supplicant restart
    root#  ping gnu.org

<!-- ====================================================================== -->

# xorg

## xinput

Pour voir les input :

    root#  xinput list --long

# scrot

Pour les captures d'écran

    root#  emerge -av media-gfx/scrot

# openbox

    root#  cp /usr/share/applications/thunar.desktop /usr/share/applications/thunar-tmp.desktop
    root#  nano /usr/share/applications/thunar-tmp.desktop
    nano:
      Exec=thunar %F ~/tmp
      Icon=preferences-desktop-locale

Exemple de modification du fichier de menu :

    lafrier$  mv ~/.config/openbox/menu.xml ~/.config/openbox/menu.xml.orig
    lafrier$  cp $AIDEUNIX/Niveau-2/Openbox/menu.xml ~/.config/openbox/.

RQ: Après modification des fichiers de configuration, executer openbox --reconfigure

Enlever l'accès à l'interface de configuration graphique :

    lafrier$  nano ~/.config/tint2/tint2rc
    nano:
      #launcher_item_app = tint2conf.desktop

Configurer les lanceurs :

    lafrier$  nano ~/.config/tint2/tint2rc
    nano:
      #-------------------------------------  
      # Launcher
      launcher_icon_theme = areao43
      launcher_padding = 2 2
      launcher_background_id = 1
      launcher_icon_size = 20
      launcher_item_app = /usr/share/applications/xfce4-terminal.desktop
      launcher_item_app = /usr/share/applications/thunar.desktop
      launcher_item_app = /usr/share/applications/thunar-tmp.desktop
      launcher_item_app = /usr/share/applications/simple-scan.desktop
      launcher_item_app = /usr/share/applications/firefox-bin.desktop
      launcher_item_app = /usr/share/applications/thunderbird-bin.desktop
      launcher_item_app = /usr/share/applications/libreoffice-startcenter.desktop
      launcher_item_app = /usr/share/applications/vlc.desktop

Exectuer `tint2` pour voir l'effet d'une modification de `tint2rc`.

... thunar /home/lafrier/tmp ...
... xfdesktop ...
... est-ce qu'on garde pcmanfm au dessus ? ...

RQ: Peut-être regarder xscreensaver en lançant `xscreensaver -no-splash &` dans autostart

<!-- ====================================================================== -->

# ssh

## Le serveur

    root#  nano /etc/ssh/sshd_config
    nano:
      Port 2222
      PermitRootLogin no
      PasswordAuthentication no
      UsePAM yes
      PrintMotd no
      PrintLastLog no
      Subsystem       sftp    /usr/lib64/misc/sftp-server
      AcceptEnv LANG LC_ALL LC_COLLATE LC_CTYPE LC_MESSAGES LC_MONETARY LC_NUMERIC LC_TIME LANGUAGE LC_ADDRESS LC_IDENTIFICATION LC_MEASUREMENT LC_NAME LC_PAPER LC_TELEPHONE
      AcceptEnv COLORTERM
      AllowUsers lafrier@nom-machine lafrier@192.168.1.16		
    root#  rc-service sshd start
    root#  rc-update add sshd default

RQ: Changer 2222 par ce que vous voulez.

Si on a changé le port, la commande `ssh` devient (pour se connecter `machine-lafrier` depuis une machine distante) :

    lafrier$  ssh lafrier@machine-lafrier -p2222

## Configurer une connexion

L'utilisateur `lafrierclient` sur la machine `machineclient` (192.168.1.16) souhaite se connecter en tant que `lafrierserveur` sur la machine `machineserveur` (192.168.1.32). 

`lafrierclient` crée un jeu de clés ssh :

    lafrierclient$  ssh-keygen
    ssh-keygen:
      /home/lafrierclient/.ssh/id_rsa_lafrierclient_machineclient_vers_lafrierserveur_machineserveur

`lafrierserveur` récupère la clé publique `id_rsa_lafrierclient_machineclient_vers_lafrierserveur_machineserveur.pub` et la recopie sous `/home/lafrierserveur/.ssh`. Puis il active cette clé :

    lafrierserveur$  cd ~/.ssh
    lafrierserveur$  cat id_rsa_lafrierclient_machineclient_vers_lafrierserveur_machineserveur.pub >> authorized_keys
    lafrierserveur$  nano config
    lafrierserveur$  chmod 600 id_rsa_lafrierclient_machineclient_vers_lafrierserveur_machineserveur.pub
    lafrierserveur$  chmod 600 authorized_keys

`lafrierclient` définit la connexion sur `machineclient` :

    lafrierclient$  cd ~/.ssh
    lafrierclient$  nano config
    nano:
      host machineserveur
        hostname 192.168.1.36
        user lafrierserveur
        port 2222
        protocol 2
	forwardagent yes
	forwardx11 yes
	forwardx11trusted yes
	identityfile ~/.ssh/id_rsa_lafrierclient_machineclient_vers_lafrierserveur_machineserveur
    lafrierclient$  chmod 600 config

Lorsque `lafrierclient` veut se connecter :

    lafrierclient$  ssh machineserveur

<!-- ====================================================================== -->

# vpn

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        [*] Network device support  --->
          [*] Network core driver support
	    <*>   Universal TUN/TAP device driver support
    root#  echo "net-vpn/openvpn lz4" >> /etc/portage/package.use/openvpn
    root#  emerge -av net-vpn/openvpn

# transmission

    root#  emerge -av net-p2p/transmission
    root#  emerge -av net-p2p/transmission-remote-gtk

# qbittorrent

    root#  emerge -av net-p2p/qbittorrent

<!-- ====================================================================== -->

# audio général

    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} alsa jack -pulseaudio midi mp3 ffmpeg"
    root#  emerge -av --update --changed-use --deep @world

# loopback

    Activer `SND_ALOOP` dans le noyau

# echo cancel

    root#  cd /usr/local/share
    root#  git clone https://github.com/voice-engine/ec.git
    root#  cd ec
    root#  make
    root#  git clone https://github.com/voice-engine/alsa_plugin_fifo.git
    root#  cd alsa_plugin_fifo
    root#  make
    root#  make install

# jack

    root#  echo "media-sound/jack2 libsamplerate" >> /etc/portage/package.use/jack2
    root#  emerge -av media-sound/jack2
    root#  emerge -av media-sound/qjackctl
    root#  emerge -av media-sound/patchage

# jack_mixer

    root#  cd /opt
    root#  mkdir jack_mixer
    root#  cd jack_mixer
    root#  wget https://rdio.space/jackmixer/tarballs/jack_mixer-15.tar.xz
    root#  tar -xJf jack_mixer-15.tar.xz
    root#  cd jack_mixer-15
    root#  meson builddir --prefix=/usr --buildtype=release
    root#  meson compile -C builddir
    root#  meson install -C builddir

# ffmpeg

    root#  nano /etc/portage/package.use/ffmpeg
    nano:
      media-video/ffmpeg amr amrenc bluray bs2b cdio chromaprint codec2 cpudetection fdk flite fontconfig frei0r fribidi gcrypt gme gmp gsm iec61883 ieee1394 jpeg2k kvazaar ladspa libaom libaribb24 libass libcaca libdrm libsoxr libtesseract lv2 lzma modplug mp3 openal opencl opengl openh264 openssl opus rav1e rubberband samba sdl snappy speex srt ssh svg theora truetype twolame v4l vaapi vdpau vidstab vorbis vpx vulkan wavpack webp x264 x265 xvid zeromq zimg zvbi
    root#  emerge -av media-video/ffmpeg

# gstreamer

    root#  emerge -av media-libs/gstreamer
    root#  emerge -av media-libs/gst-plugins-base
    root#  emerge -av media-libs/gst-plugins-bad
    root#  emerge -av media-libs/gst-plugins-good
    root#  emerge -av media-libs/gst-plugins-ugly
    root#  emerge -av media-plugins/gst-plugins-jack
    root#  echo "media-plugins/gst-plugins-webrtc ~amd64" >> /etc/portage/package.accept_keywords/gst-plugins-webrtc
    root#  emerge -av media-plugins/gst-plugins-webrtc 
    root#  emerge -av 
    root#  emerge -av 
    root#  emerge -av 
    root#  emerge -av 
    root#  emerge -av 
    root#  emerge -av 

# midi

    root#  emerge -av media-sound/timidity++
    root#  emerge -av media-sound/timidity-freepats
    root#  emerge -av media-sound/fluid-soundfont
    root#  emerge -av media-sound/fluidsynth
    root#  eselect timidity list
    root#  eselect timidity set 1
    lafrier$  nano ~/.asoundrc
    nano:
      ++defaults.pcm.dmix.rate 48000

Pour lancer le démon :

    lafrier$  timidity -iAD -B2,8 -Os -EFreverb=0

Pour voir les ports midi :

    lafrier$  aconnect -l

Exemple d'utilisation (le premier port de `timidity`) :

    lafrier$  aplaymidi -p 128:0 fichier.midi

Exemple de connexion :

    lafrier$  aconnect $input:0 $output:0

Voir la connexion :

    lafrier$  aconnect -l

Conversion vers wav

    lafrier$  fluidsynth --fast-render=fichier.wav /usr/share/sounds/sf2/FluidR3_GM.sf2 fichier.midi

# vlc

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers --->
        Input device support --->
	  -*- Generic input layer (needed for keyboard, mouse, ...)
	  [*]   Miscellaneous devices  --->
            <*>   User level driver support
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  nano /etc/portage/package.use/vlc
    nano:
      media-video/vlc libass
      media-video/vlc qt5 gnutls live lua matroska rtsp theora upnp vcdx truetype flac gstreamer bluray dvd mp3 mpeg png srt udev v4l vorbis a52 aom archive aribsub bidi cddb chromaprint dav1d dc1394 dts faad fdk fluidsynth fontconfig gme ieee1394 kate libcaca libnotify libtiger linsys lirc mad modplug mtp musepack nfs ogg omxil opus projectm samba sdl-image sftp shout sid skins soxr speex svg taglib tremor twolame vaapi vdpau vnc vpx wayland x264 x265 xml zeroconf zvbi
    root#  emerge -av media-video/vlc

# audacity

    root#  emerge -av media-sound/audacity

# ardour

    root#  emerge -av media-sound/ardour

# mplayer

    root#  emerge -av media-video/mplayer

# Rip a CD

    root#  emerge -av media-sound/abcde
    root#  emerge -av media-libs/musicbrainz
    root#  emerge -av dev-perl/MusicBrainz-DiscID
    root#  emerge -av dev-perl/WebService-MusicBrainz

# Burn a CD

    root#  nano /etc/portage/package.use/k3b
    nano:
      >=dev-qt/qtcore-5.15.1-r1 icu
      kde-apps/k3b dvd encode ffmpeg flac mad mp3 musepack sndfile sox taglib vcd vorbis
      >=kde-frameworks/kfilemetadata-5.74.0 taglib
      >=media-video/transcode-1.1.7-r5 dvd
    root#  emerge -av kde-apps/k3b

# bluetooth

## Configuration

Attention ! Bien mettre le `Bluetooth subsystem support` en module.

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      [*] Networking support --->
        <M>   Bluetooth subsystem support  --->
	  [*]   Bluetooth Classic (BR/EDR) features (NEW)
	  <*>     RFCOMM protocol support
	  <*>     HIDP protocol support
	  [*]     Bluetooth High Speed (HS) features (NEW)
	  [*]   Bluetooth Low Energy (LE) features (NEW)
	  [*]   Export Bluetooth internals in debugfs (NEW)
	  Bluetooth device drivers  --->
	    <M> HCI USB driver
	    <M> HCI UART driver
	<*>   RF switch subsystem support  ----
      Device Drivers  --->
        HID support  --->
	  <*>   User-space I/O driver support for HID subsystem
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  emerge -av --noreplace sys-kernel/linux-firmware
    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} bluetooth"
    root#  emerge -av --changed-use --deep @world
    root#  emerge -av net-wireless/bluez
    root#  rc-service bluetooth start
    root#  rc-update add bluetooth default
    root#  gpasswd -a lafrier plugdev

RQ: `plugdev` peut ne pas exister. Ca fonctionne sans que l'utilisateur soit membre du groupe. On peut donc peut-être enlever cette étape.

## Configuration d'une enceinte ou d'écouteurs `bluetooth` avec `alsa`

    root#  echo "media-sound/bluez-alsa ~amd64" >> /etc/portage/package.accept_keywords/bluez-alsa
    root#  emerge -av media-sound/bluez-alsa
    root#  rc-service bluealsa start
    root#  rc-update add bluealsa default

## Se connecter à un périphérique `bluetooth`

    lafrier$  bluetoothctl
    bluetoothctl:
      list
      power on
      agent on
      default-agent
      discoverable on
      pairable on
      scan on
      devices

Par exemple, `devices` renvoie `88:C6:26:44:0C:EA UE BOOM`. On continue alors sous `bluetoothctl` avec

    bluetoothctl:
      pair 88:C6:26:44:0C:EA
      trust 88:C6:26:44:0C:EA
      connect 88:C6:26:44:0C:EA
      info 88:C6:26:44:0C:EA
      quit

## Se connecter à un périphérique `bluetooth` audio

Verifier que l'interface `bluetooth` est bien `hci0` :

    root#  dmesg | grep -i blue

Modifier le fichier de configuration `alsa` de l'utilisateur :

    lafrier$  nano ~/.asoundrc
    nano:
      defaults.bluealsa {
        interface "hci0"            # host Bluetooth adapter
	device "88:C6:26:44:0C:EA"  # Bluetooth headset MAC address
	profile "a2dp"
      }

Pour tester le son :

    lafrier$  alsamixer -D bluealsa
    lafrier$  aplay -D bluealsa <exemple-de-fichier>.wav

## Basculer du périphérique audio usuel au périphérique audio `bluetooth`

Une fois que le périphérique a été connecté une fois, il reste appairé et il suffit de la connecter ou de le déconnecter. Si on souhaite que le périphérique audio `bluetooth` devienne le périphérique audio par défaut lorsqu'on le connecte, il faut aussi modifier le fichier `.asoundrc`. On commence donc par créer deux fichiers `.asoundrc` : celui de référence et celui du périphérique audio `bluetooth`. Par exemple,

    lafrier$  nano ~/.asoundrc.interne
    nano:
      defaults.pcm.!card "PCH";
      defaults.ctl.!card "PCH";
    lafrier$  nano ~/.asoundrc.bluetooth
    nano:
      pcm.!default {
        type plug
	slave.pcm {
	  type bluealsa
	  device "88:C6:26:44:0C:EA"
	  profile "a2dp"
	}
      }
      ctl.!default {
        type bluealsa
      }

Les commandes pour se connecter sont alors

     lafrier$  bluetoothctl -- connect 88:C6:26:44:0C:EA
     lafrier$  \rm ~/.asoundrc
     lafrier$  ln -s /home/lafrier/.asoundrc.bluetooth /home/lafrier/.asoundrc

et celles pour se déconnecter sont

     lafrier$  bluetoothctl -- disconnect 88:C6:26:44:0C:EA
     lafrier$  \rm ~/.asoundrc
     lafrier$  ln -s /home/lafrier/.asoundrc.interne /home/lafrier/.asoundrc

RQ: Ces modifications ne sont prises en compte par les logiciels utilisant de l'audio que lorsque ces logiciels sont relancés.

On peut automatiser cela dans `openbox` :

     lafrier$  nano ~/.config/openbox/menu.xml
     nano:
       <separator />
       <item label="Connecter le périphérique audio bluetooth">
         <action name="Execute">
           <command>xfce4-terminal --fullscreen -x bash -c 'bluetoothctl -- connect 88:C6:26:44:0C:EA ; \rm ~/.asoundrc ; ln -s /home/lafrier/.asoundrc.bluetooth /home/lafrier/.asoundrc'</command>
           <startupnotify>
             <enabled>yes</enabled>
           </startupnotify>
         </action>
       </item>
       <separator />
       <item label="Déconnecter le périphérique audio bluetooth">
         <action name="Execute">
           <command>xfce4-terminal --fullscreen -x bash -c 'bluetoothctl -- disconnect 88:C6:26:44:0C:EA ; \rm ~/.asoundrc ; ln -s /home/lafrier/.asoundrc.interne /home/lafrier/.asoundrc'</command>
           <startupnotify>
             <enabled>yes</enabled>
           </startupnotify>
         </action>
       </item>



<!-- ====================================================================== -->

# web

    root#  echo "www-apps/hugo ~amd64" >> /etc/portage/package.accept_keywords/hugo
    root#  emerge -av www-apps/hugo

# youtube

    root#  emerge -av net-misc/youtube-dl

Utilisation : chercher l'url du lien youtube

    lafrier$  youtube-dl <URL>

et cela crée un fichier `mp4` correspondant à la vidéo.

<!-- ====================================================================== -->

# wacom

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device drivers --->
        HID support --->
          HID bus support --->
	    Special HID drivers --->
              <*> Wacom Intuos/Graphire tablet support (USB)
      Device drivers --->
        Input device support --->
          [*] Tablets --->
	    <*> Wacom protocol 4 serial tablet support--->
          [*] Touchscreens --->
	    <*> Wacom W8001 penabled serial touchscreen
	    <*> Wacom Tablet support (I2C)
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  nano /etc/portage/make.conf
    nano:
      INPUT_DEVICES="${INPUT_DEVICES} wacom"
    root#  emerge -av uDUN @world

<!-- ====================================================================== -->

# Imprimante et scanner

## cups

    root#  echo "net-print/cups zeroconf" >> /etc/portage/package.use/cups
    root#  emerge -av net-print/cups
    root#  emerge -av net-dns/avahi
    root#  emerge -av sys-auth/nss-mdns
    root#  rc-update add avahi-daemon default
    root#  rc-service avahi-daemon start
    root#  gpasswd -a lafrier lp
    root#  gpasswd -a lafrier lpadmin
    root#  rc-service cupsd start
    root#  rc-update add cupsd default 
    root#  emerge -av net-print/cups-filters
    root#  emerge -av net-print/gutenprint

## HP ENVY 7640

    root#  nano /etc/portage/package.use/hplip
    nano:
      net-print/hplip scanner
    root#  emerge -av net-print/hplip
    root#  hp-setup -i 192.168.1.17
    root#  rc-service cupsd restart

## Imprimante

Avec un navigateur web, aller à `http://localhost:631/` puis sous administration se logger en root et déclarer l'imprimante. Penser à la définir comme imprimante par défaut pour le serveur. Ensuite, tester avec

    lafrier$  echo coucou | lp
    
## Scanner

    root#  emerge -av media-gfx/xsane
    root#  emerge -av media-gfx/simple-scan
    root#  gpasswd -a lafrier scanner

<!-- ====================================================================== -->

# libreoffice

    root#  emerge -av app-office/libreoffice-bin

# firefox

Si on veut seulement le binaire (mais ne fonctionne pas avec `jack`) :

    root#  emerge -av www-client/firefox-bin

Préférable :

    root#  emerge -av www-client/firefox

# thunderbird

    root#  emerge -av mail-client/thunderbird-bin

Sous thunderbird, ajouter les modules suivants : `lightning` et `provider for google`. Puis relancer thunderbird et cliquer sur le calendrier (lightning), faire un click droit dans la fenêtre de gauche, et ajouter un nouveau calcendrier en passant par le réseau (pour avoir google calendar par exemple).

<!-- ====================================================================== -->

# mutt

    root#  emerge -av mail-client/mutt

# xournal

    root#  emerge -av app-text/xournal

Penser à aller dans le menu `Options` et dans `Ecran tactile et stylet` pour activer la sensibilité à la pression (penser à enregistrer les préférences).

# pdf

    root#  emerge -av app-text/mupdf 
    root#  emerge -av app-text/evince
    root#  emerge -av app-text/pdfjam

# gimp

    root#  emerge -av media-gfx/gimp

# openboard

    root#  emerge -av dev-qt/qtwebkit
    root#  emerge -av dev-qt/qtxmlpatterns
    root#  emerge -av dev-qt/qtscript
    root#  emerge -av dev-qt/designer
    root#  echo "dev-qt/qtmutimedia widgets" >> /etc/portage/package.use/qtmutimedia
    root#  emerge -av dev-qt/qtmutimedia
    root#  emerge -av dev-qt/qtsvg
    root#  emerge -av media-libs/libvpx
    root#  emerge -av media-libs/libtheora
    root#  emerge -av x11-libs/libva
    root#  echo "media-libs/fdk-aac FraunhoferFDK" >> /etc/portage/package.license/fdk-aac
    root#  emerge -av media-libs/fdk-aac
    root#  emerge -av media-libs/libsdl
    root#  emerge -av media-libs/x264
    root#  emerge -av media-libs/opus
    root#  emerge -av media-sound/lame
    root#  emerge -av media-libs/libass
    root#  cd /opt
    root#  mkdir Openboard
    root#  cd Openboard
    root#  git clone https://github.com/OpenBoard-Org/OpenBoard-ThirdParty.git
    root#  cd OpenBoard-ThirdParty/freetype
    root#  qmake freetype.pro -spec linux-g++
    root#  make
    root#  cd ../quazip
    root#  qmake quazip.pro -spec linux-g++
    root#  make
    root#  cd ../xpdf/xpdf-3.04
    root#  ./configure --with-freetype2-library="../../freetype/lib/linux" --with-freetype2-includes="../../freetype/freetype-2.6.1/include"
    root#  cd ..
    root#  qmake xpdf.pro -spec linux-g++
    root#  make
    root#  cd ../..
    root#  git clone https://github.com/OpenBoard-Org/OpenBoard-Importer.git
    root#  cd OpenBoard-Importer
    root#  qmake OpenBoardImporter.pro -spec linux-g++
    root#  make
    root#  cd ..
    root#  git clone https://github.com/OpenBoard-Org/OpenBoard.git
    root#  cd OpenBoard
    root#  qmake OpenBoard.pro -spec linux-g++-64
    root#  make 

    lafrier$  nano ~/.bashrc
    nano:
      alias openboard='/opt/Openboard/OpenBoard/build/linux/release/product/OpenBoard'

# mumble

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers -->
        Sound card support -->
          Advanced Linux Sound Architecture -->
            USB sound devices -->
              <*> USB Audio/MIDI driver
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  echo "media-sound/mumble ~amd64" >> /etc/portage/package.accept_keywords/mumble
    root#  emerge -av media-sound/mumble
      
# tmate

    root#  echo "app-misc/tmate ~amd64" >> /etc/portage/package.accept_keywords/tmate
    root#  emerge -va app-misc/tmate 

# drawpile

    root#  emerge -av kde-frameworks/extra-cmake-modules
    root#  emerge -av kde-frameworks/karchive
    root#  emerge -av net-libs/miniupnpc
    root#  emerge -av dev-libs/libsodium
    root#  emerge -av kde-frameworks/kdnssd
    root#  emerge -av dev-qt/qtwidgets
    root#  emerge -av dev-libs/qtkeychain
    root#  emerge -av net-libs/libmicrohttpd

    root#  cd /opt
    root#  wget https://drawpile.net/files/src/drawpile-2.1.17.tar.gz
    root#  tar -zxvf drawpile-2.1.17.tar.gz
    root#  rm drawpile-2.1.17.tar.gz
    root#  cd drawpile-2.1.17
    root#  mkdir build
    root#  cd build
    root#  cmake ..
    root#  make
    root#  cmake -DCMAKE_INSTALL_PREFIX=/usr/local/ ../
    root#  make install

<!-- ====================================================================== -->

# Edition général

    root#  cd /etc/portage
    root#  nano make.conf
    nano:
      USE="${USE} latex postscript dvi pdf"
    root#  emerge -av --update --changed-use --deep @world

# latex

    root#  emerge -av dev-texlive/texlive-fontsrecommended
    root#  emerge -av dev-texlive/texlive-fontsextra
    root#  emerge -av dev-texlive/texlive-latex
    root#  emerge -av dev-texlive/texlive-latexextra
    root#  emerge -av dev-texlive/texlive-langfrench
    root#  emerge -av dev-texlive/texlive-latexrecommended
    root#  emerge -av dev-texlive/texlive-mathscience
    root#  emerge -av dev-texlive/texlive-xetex
    root#  emerge -av dev-tex/latex-beamer
    root#  emerge -av dev-texlive/texlive-bibtexextra
    root#  emerge -av dev-tex/biblatex
    root#  emerge -av dev-texlive/texlive-publishers
    root#  emerge -av app-text/aspell
    root#  echo "dev-tex/latex2html gif png" >> /etc/portage/package.use/latex2html
    root#  emerge -av dev-tex/latex2html

# noweb

    root#  cd /usr/local/share
    root#  git clone https://github.com/nrnrnr/noweb.git
    root#  cd noweb/src
    root#  make boot
    root#  make all
    root#  ./awkname gawk
    root#  make install
    lafrier$  echo "export PATH+=:/usr/local/noweb" >> ~/.bashrc
    lafrier$  echo "export MANPATH+=:/usr/local/noweb/man" >> ~/.bashrc
    lafrier$  echo "export TEXINPUTS+=:/usr/local/tex/inputs" >> ~/.bashrc

# asciidoc

    root#  emerge -av app-text/asciidoc
    root#  emerge -av app-text/dblatex
    root#  emerge -av dev-ruby/asciidoctor
    root#  gem install asciidoctor-pdf --pre
    root#  gem install asciidoctor-latex --pre
    root#  gem install asciidoctor-mathematical --pre

# irssi

    root#  emerge -av net-irc/irssi
ssh -L 6667:localhost:6667 vps_meso-star -fN
ps ux | grep ssh
irssi
/help
/help connect
/connect localhost
/join #stardis
alt numero
/who


    ???
    port 6667
    tunel ssh mesostar

# pandoc

    root#  cd /opt
    root#  wget https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-linux-amd64.tar.gz
    root#  tar -zxvf pandoc-2.10.1-linux-amd64.tar.gz
    root#  rm pandoc-2.10.1-linux-amd64.tar.gz

    lafrier$  nano ~.bashrc
    nano:
      alias pandoc='/opt/pandoc-2.10.1/bin/pandoc' 

<!-- ====================================================================== -->

# arduino

    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        [*] USB support --->
	  <*> USB Serial Converter support --->
	    <*> USB FTDI Single Port Serial Driver
	    <*> USB Winchiphead CH341 Single Port Serial Driver
	  <*> USB Modem (CDC ACM) support
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  gpasswd -a lafrier dialout

Télécharger le fichier `arduino-1.8.13-linux64.tar.xz` sur le site officiel (ou plus récent) et le placer sous /opt.² 

    root#  cd /opt
    root#  tar -xJf arduino-1.8.13-linux64.tar.xz
    root#  rm arduino-1.8.13-linux64.tar.xz
    root#  cd arduino-1.8.13
    root#  ./install.sh

RQ : Pour émuler un arduino facilement et tester des premiers programmes, voir https://www.tinkercad.com/.

<!-- ====================================================================== -->

# zoom

    root#  cd /opt
    root#  wget https://zoom.us/client/latest/zoom_x86_64.tar.xz
    root#  tar -xf zoom_x86_64.tar.xz
    root#  rm zoom_x86_64.tar.xz

RQ: L'exécutable est `/opt/zoom/zoom.sh`. Ne pas le lancer en fond.

<!-- ====================================================================== -->

# Sauvegarde incrémentale cryptée

    root#  emerge -av app-backup/duplicity

<!-- ====================================================================== -->

# Calcul scientifique

    root#  emerge -av sci-libs/libcerf
    root#  emerge -av sci-libs/netcdf
    root#  emerge -av sys-cluster/openmpi
    root#  emerge -av dev-lang/R

# maxima

    root#  emerge -av sci-mathematics/maxima

    lafrier$
    nano ~/.bashrc
    nano:
      alias maxima='rlwrap maxima'

# gmsh

    root#  cd /opt
    root#  wget https://gmsh.info/bin/Linux/gmsh-4.6.0-Linux64.tgz
    root#  tar -zxvf gmsh-4.6.0-Linux64.tgz
    root#  rm gmsh-4.6.0-Linux64.tgz

    lafrier$  nano ~/.bashrc
    nano:
      alias gmsh='/opt/gmsh-4.6.0-Linux64/bin/gmsh'

# gnuplot

    root#  emerge -av sci-visualization/gnuplot

# streamdesktop

    root#  emerge -av media-plugins/gst-plugins-ximagesrc
    root#  emerge -av media-libs/gstreamer
    root#  emerge -av media-plugins/gst-plugins-vpx
    root#  emerge -av media-plugins/gst-plugins-shout2

# paraview

Aller chercher `ParaView-5.8.1-MPI-Linux-Python3.7-64bit.tar.gz` sur le site de paraview.

    root#  mv ParaView-5.8.1-MPI-Linux-Python3.7-64bit.tar.gz /opt/.
    root#  cd /opt
    root#  tar -zxvf ParaView-5.8.1-MPI-Linux-Python3.7-64bit.tar.gz
    root#  rm ParaView-5.8.1-MPI-Linux-Python3.7-64bit.tar.gz
    root#  nano ~/.bashrc
    nano:
      alias paraview='/opt/ParaView-5.8.1-MPI-Linux-Python3.7-64bit/bin/paraview'

# qalculate

    root#  emerge -av sci-calculators/qalculate-gtk

<!-- ====================================================================== -->

# sql

    root#  emerge -av dev-db/mariadb
    root#  emerge --config dev-db/mariadb
    root#  rc-update add mysql default
    root#  rc-service mysql start

<!-- ====================================================================== -->

# CD/DVD

    root#  make menuconfig
    menuconfig:
      Device Drivers -->
        <*> Serial ATA and Parallel ATA drivers (libata)  --->
          <*>   Platform AHCI SATA support
      File systems  --->
        CD-ROM/DVD Filesystems  --->
          <*> ISO 9660 CDROM file system support
	  [*]   Microsoft Joliet CDROM extensions
	  [*]   Transparent decompression extension
          <*> UDF file system support
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  emerge -av app-cdr/dvd+rw-tools
    root#  emerge -av sys-fs/udftools
    root#  gpasswd -a lafrier cdrom

<!-- ====================================================================== -->

# imagemagick

    root#  emerge -av media-gfx/imagemagick

<!-- ====================================================================== -->

# discord

## via les dépots de gentoo

    root#  echo "net-im/discord-bin ~amd64" >> /etc/portage/package.accept_keywords/discord
    root#  echo ">=net-im/discord-bin-0.0.12 all-rights-reserved" >> /etc/portage/package.license/discord
    root#  emerge -av net-im/discord-bin

Pour lancer discord :

    lafrier$  apulse discord

## via le site de discord

Sur le site de `discord`, aller chercher le ficher `tar.gz` pour `linux` (actuellement `discord-0.0.13.tar.gz`).

    root#  mv discord-0.0.13.tar.gz /opt/.
    root#  cd /opt/
    root#  tar -zxvf discord-0.0.13.tar.gz
    root#  cd Discord
    root#  ./postinst.sh

Pour lancer discord :

    lafrier$  apulse /opt/Discord/Discord

<!-- ====================================================================== -->

# rclone

## Installation

    root#  cd /opt
    root#  mkdir rclone
    root#  cd rclone
    root#  wget https://rclone.org/install.sh
    root#  bash install.sh

## Configuration

lafrier$  rclone config
n/s/q> n
name> drivelaplace
Storage> 13
client_id>
client_secret>
scope> 1
root_folder_id>
service_account_file>
y/n> n
y/n> y
y/n> n
y/e/d> y
e/n/d/r/c/s/q> q
lafrier$  rclone lsd drivelaplace:
lafrier$  mkdir /mnt/data/lafrier/Cloud/drivelaplace

<!-- ====================================================================== -->

# crossdev

    root#  emerge -av sys-devel/crossdev

Créer un `layer` pour `crossdev`

    root#  mkdir -p /var/db/repos/localrepo-crossdev/{profiles,metadata}
    root#  echo 'crossdev' > /var/db/repos/localrepo-crossdev/profiles/repo_name
    root#  echo 'masters = gentoo' > /var/db/repos/localrepo-crossdev/metadata/layout.conf
    root#  chown -R portage:portage /var/db/repos/localrepo-crossdev 

Si le répertoire `/etc/portage/repos.conf` n'existe pas, le créer et y placer le fichier de configuration des dépots par défaut :

    root#  mkdir /etc/portage/repos.conf
    root#  cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf

Rajouter le layer à la liste des dépots :

    root#  nano /etc/portage/repos.conf/crossdev.conf
    nano:
      [crossdev]
      location = /var/db/repos/localrepo-crossdev
      priority = 10
      masters = gentoo
      auto-sync = no

# raspberry pi

    lafrier$  cd /data/lafrier
    lafrier$  mkdir raspberrypi
    lafrier$  cd raspberrypi
    lafrier$  git clone -b stable --depth=1 https://github.com/raspberrypi/firmware
    lafrier$  git clone https://github.com/raspberrypi/linux
    lafrier$  cd linux
    lafrier$  git checkout rpi-5.9.y
    lafrier$  ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make bcmrpi3_defconfig
    lafrier$  ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make

Ensuite on passe à la carte SD. D'abord sans insérer la carte

    root#  lsblk -p

puis après avoir inséré la carte

    root#  lsblk -p

La différence indique où est la carte (dans mon cas `/dev/mmcblk0`). ATTENTION ! Changer `/dev/mmcblk0` par ce qu'il faut dans les exemples qui suivent.

## Partitionnement

Partitionnement de la carte (boot 128Mb amorçable ; swap 2G ; root (/) le reste) :

    root#  fdisk /dev/mmcblk0
    fdisk:
      o
      n
      p
      1
      (entrée)
      +128M
      n
      p
      2
      (entrée)
      +2G
      n
      p
      3
      (entrée)
      (entrée)
      p
      a
      1
      t
      1
      c
      t
      2
      82
      p
      w
    root#  mkfs -t vfat -F 32 /dev/mmcblk0p1
    root#  mkswap /dev/mmcblk0p2
    root#  mkfs -i 8192 -t ext4 /dev/mmcblk0p3 

## Installation

    root#  mkdir /mnt/gentoo
    root#  mount /dev/mmcblk0p3 /mnt/gentoo
    root#  cd /mnt/gentoo
    root#  wget http://distfiles.gentoo.org/experimental/arm64/stage3-arm64-20200609.tar.bz2
    root#  tar xpvf stage3-arm64-20200609.tar.bz2 --xattrs-include='*.*' --numeric-owner
    root#  rm -rf /mnt/gentoo/tmp/*
    root#  cd ~
    root#  wget http://distfiles.gentoo.org/snapshots/portage-latest.tar.bz2
    root#  mkdir /mnt/gentoo/var/db/repos
    root#  mkdir /mnt/gentoo/var/db/repos/gentoo
    root#  tar xvpf portage-latest.tar.bz2 --strip-components=1 -C /mnt/gentoo/var/db/repos/gentoo
    root#  mount /dev/mmcblk0p1 /mnt/gentoo/boot
    root#  cp -rv /data/lafrier/raspberrypi/firmware/boot/* /mnt/gentoo/boot
    root#  cp /data/lafrier/raspberrypi/linux/arch/arm64/boot/Image /mnt/gentoo/boot/kernel8.img
    root#  mv /mnt/gentoo/boot/bcm2710-rpi-3-b-plus.dtb /mnt/gentoo/boot/bcm2710-rpi-3-b-plus.dtb_32
    root#  mv /mnt/gentoo/boot/bcm2710-rpi-3-b.dtb /mnt/gentoo/boot/bcm2710-rpi-3-b.dtb_32

Pour Raspberry Pi 3B :

    root#  cp /data/lafrier/raspberrypi/linux/arch/arm64/boot/dts/broadcom/bcm2710-rpi-3-b.dtb /mnt/gentoo/boot

Pour Raspberry Pi 3B+ :

    root#  cp /data/lafrier/raspberrypi/linux/arch/arm64/boot/dts/broadcom/bcm2710-rpi-3-b-plus.dtb /mnt/gentoo/boot

## Suite installation

    root#  cd /data/lafrier/raspberrypi/linux
    root#  ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnu- make modules_install INSTALL_MOD_PATH=/mnt/gentoo
    root#  nano -w /mnt/gentoo/etc/inittab
    nano:
      # Architecture specific features
      #f0:12345:respawn:/sbin/agetty 9600 ttyAMA0 vt100
    root#  nano -w /mnt/gentoo/etc/udev/rules.d/99-com.rules
    nano:
      SUBSYSTEM=="input", GROUP="input", MODE="0660"
      SUBSYSTEM=="i2c-dev", GROUP="i2c", MODE="0660"
      SUBSYSTEM=="spidev", GROUP="spi", MODE="0660"
      SUBSYSTEM=="bcm2835-gpiomem", GROUP="gpio", MODE="0660"

      SUBSYSTEM=="gpio*", PROGRAM="/bin/sh -c '\
              chown -R root:gpio /sys/class/gpio && chmod -R 770 /sys/class/gpio;\
              chown -R root:gpio /sys/devices/virtual/gpio && chmod -R 770 /sys/devices/virtual/gpio;\
              chown -R root:gpio /sys$devpath && chmod -R 770 /sys$devpath\
'"

      KERNEL=="ttyAMA[01]", GROUP="dialout", PROGRAM="/bin/sh -c '\
              ALIASES=/proc/device-tree/aliases; \
              if cmp -s $ALIASES/uart0 $ALIASES/serial0; then \
                      echo 0;\
              elif cmp -s $ALIASES/uart0 $ALIASES/serial1; then \
                      echo 1; \
              else \
                      exit 1; \
              fi\
      '", SYMLINK+="serial%c"

      KERNEL=="ttyS0", GROUP="dialout", PROGRAM="/bin/sh -c '\
              ALIASES=/proc/device-tree/aliases; \
              if cmp -s $ALIASES/uart1 $ALIASES/serial0; then \
                      echo 0; \
              elif cmp -s $ALIASES/uart1 $ALIASES/serial1; then \
                      echo 1; \
              else \
                      exit 1; \
              fi \
      '", SYMLINK+="serial%c"      

## Firmware wifi

    root#  mkdir /mnt/gentoo/lib/firmware

Pour raspberry pi 3B, aller chercher les fichiers `brcmfmac43430-sdio.txt` et `brcmfmac43430-sdio.bin` et les recopier dans `/mnt/gentoo/lib/firmware/brcm`. On peut aussi recopier l'ensemble du répertoire `/lib/firmware/brcm` sur une raspbian qui fonctionne.

Pour raspberry pi 3B+, aller chercher les fichiers `brcmfmac43455-sdio.txt` et `brcmfmac43455-sdio.bin` et les recopier dans `/mnt/gentoo/lib/firmware/brcm`. On peut aussi recopier l'ensemble du répertoire `/lib/firmware/brcm` sur une raspbian qui fonctionne.

## Firmware bluetooth

Pour raspberry pi 3B,

    root#  wget -P /mnt/gentoo/lib/firmware/brcm https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/master/broadcom/BCM43430A1.hcd

Pour raspberry pi 3B+

    root#  wget -P /mnt/gentoo/lib/firmware/brcm https://raw.githubusercontent.com/RPi-Distro/bluez-firmware/master/broadcom/BCM4345C0.hcd

## Préparation pour le boot

    root#  nano /mnt/gentoo/etc/shadow
    nano:
      root:$6$xxPVR/Td5iP$/7Asdgq0ux2sgNkklnndcG4g3493kUYfrrdenBXjxBxEsoLneJpDAwOyX/kkpFB4pU5dlhHEyN0SK4eh/WpmO0::0:99999:7:::
    root#  nano /mnt/gentoo/etc/fstab
    nano:
      /dev/mmcblk0p1          /boot           vfat            noauto,noatime  1 2
      /dev/mmcblk0p2          none            swap            sw              0 0
      /dev/mmcblk0p3          /               ext4            noatime         0 1
    root#  nano -w /mnt/gentoo/boot/config.txt
    nano:
      # For more options and information see
      # http://rpf.io/configtxt
      # Some settings may impact device functionality. See link above for details

      # uncomment if you get no picture on HDMI for a default "safe" mode
      #hdmi_safe=1

      # uncomment this if your display has a black border of unused pixels visible
      # and your display can output without overscan
      #disable_overscan=1

      # uncomment the following to adjust overscan. Use positive numbers if console
      # goes off screen, and negative if there is too much border
      #overscan_left=16
      #overscan_right=16
      #overscan_top=16
      #overscan_bottom=16

      # uncomment to force a console size. By default it will be display's size minus
      # overscan.
      #framebuffer_width=1280
      #framebuffer_height=720

      # uncomment if hdmi display is not detected and composite is being output
      #hdmi_force_hotplug=1

      # uncomment to force a specific HDMI mode (this will force VGA)
      #hdmi_group=1
      #hdmi_mode=1

      # uncomment to force a HDMI mode rather than DVI. This can make audio work in
      # DMT (computer monitor) modes
      #hdmi_drive=2

      # uncomment to increase signal to HDMI, if you have interference, blanking, or
      # no display
      #config_hdmi_boost=4

      # uncomment for composite PAL
      #sdtv_mode=2

      #uncomment to overclock the arm. 700 MHz is the default.
      #arm_freq=800

      # Uncomment some or all of these to enable the optional hardware interfaces
      #dtparam=i2c_arm=on
      #dtparam=i2s=on
      #dtparam=spi=on

      # Uncomment this to enable infrared communication.
      #dtoverlay=gpio-ir,gpio_pin=17
      #dtoverlay=gpio-ir-tx,gpio_pin=18

      # Additional overlays and parameters are documented /boot/overlays/README

      # Enable audio (loads snd_bcm2835)
      dtparam=audio=on

      [pi4]
      # Enable DRM VC4 V3D driver on top of the dispmanx display stack
      dtoverlay=vc4-fkms-v3d
      max_framebuffers=2

      [all]
      #dtoverlay=vc4-fkms-v3d

      # Ecran:
      if 7inch HDMI LCD (C)-(1024 600)
      max_usb_current=1
      hdmi_group=2
      hdmi_mode=1
      hdmi_mode=87
      hdmi_cvt 1024 600 60 6 0 0 0
      hdmi_drive=1
    root#  nano -w /mnt/gentoo/boot/cmdline.txt
    nano:
      console=serial0,115200 console=tty1 root=PARTUUID=2ebef4cd-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles
    root#  nano /mnt/gentoo/etc/conf.d/keymaps
    nano:
      keymap="fr-pc"
      
## Boot the raspberry py to test

    root#  umount /mnt/gentoo/boot
    root#  umount /mnt/gentoo
























# opencascade

    root#  echo "sci-libs/opencascade ~amd64" >> /etc/portage/package.accept_keywords/opencascade
    root#  echo "app-eselect/eselect-opencascade ~amd64" >> /etc/portage/package.accept_keywords/opencascade
    root#  echo "sci-libs/vtk ~amd64" >> /etc/portage/package.accept_keywords/opencascade
    root#  echo "sci-libs/vtk rendering" >> /etc/portage/package.use/opencascade
    root#  emerge -av sci-libs/opencascade

# freecad

Télécharger `FreeCAD_0.18-16146-Linux-Conda_Py3Qt5_glibc2.12-x86_64.AppImage` sous `/opt/FreeCad`, puis changer les droits (c'est l'exécutable) :

    root#  chmod 755 /opt/FreeCAD/FreeCAD_0.18-16146-Linux-Conda_Py3Qt5_glibc2.12-x86_64.AppImage

# admesh

    root#  cd /opt
    root#  wget https://github.com/admesh/admesh/releases/download/v0.98.4/admesh-0.98.4.tar.gz
    root#  tar -zxvf admesh-0.98.4.tar.gz
    root#  rm admesh-0.98.4.tar.gz
    root#  cd admesh-0.98.4
    root#  ./configure
    root#  make
    root#  make install 

# blender

    root#  echo "media-video/ffmpeg theora" >> /etc/portage/package.use/blender
    root#  nano /etc/portage/make.conf
    nano:
      USE="ffmpeg X opengl tiff openal jack sdl fftw openexr expat"
    root#  echo "media-gfx/blender python_single_target_python3_5 cycles boost openexr tiff openimageio player game-engine bullet fftw openal jemalloc opensubdiv openvdb openvdb-compression" >> /etc/portage/package.use/blender
    root#  echo "media-libs/opencv cuda opencl" >> /etc/portage/package.use/blender
    root#  echo "media-libs/openimageio opencv" >> /etc/portage/package.use/blender
    root#  echo "media-libs/opensubdiv cuda opencl ptex tbb" >> /etc/portage/package.use/blender
    root#  echo ">=media-libs/opensubdiv-3.3.3 -opencl" >> /etc/portage/package.use/blender
    root#  echo ">=media-libs/opensubdiv-3.3.3 -opencl" >> /etc/portage/package.use/blender
    root#  echo ">=media-libs/mesa-19.2.8 opencl" >> /etc/portage/package.use/blender
    root#  echo ">=media-gfx/openvdb-4.0.2-r3 -abi3-compat" >> /etc/portage/package.use/blender
    root#  emerge --update --changed-use --deep --ask --verbose @world
    root#  emerge -av media-gfx/blender

# openscad

    root#  echo "media-gfx/openscad ~amd64" >> /etc/portage/package.accept_keywords/openscad
    root#  echo "media-gfx/opencsg ~amd64" >> /etc/portage/package.accept_keywords/openscad
    root#  emerge -av media-gfx/openscad










# x11vnc

    root#  emerge -av x11-misc/x11vnc
    
# tigervnc

    root#  echo "net-misc/tigervnc server" >> /etc/portage/package.use/tigervnc
    root#  emerge --update --changed-use --deep --ask --verbose @world
    root#  emerge -av net-misc/tigervnc

Serveur :

    lafrier$  vncpasswd
    Would you like to enter a view-only password (y/n)? n
    lafrier$  vncserver

Arrêter le serveur :

    lafrier$  pkill Xvnc

# freerdp

    root#  emerge -av net-misc/freerdp

# remmina

    root#  echo "net-misc/remmina vnc" >> /etc/portage/package.use/remmina
    root#  echo "net-misc/remmina rdp" >> /etc/portage/package.use/remmina
    root#  echo "net-misc/remmina ssh" >> /etc/portage/package.use/remmina
    root#  emerge -av net-misc/remmina

RQ: Pour se connecter à une machine distante avec le protocole xrdp, il faut créer un serveur xrdp sur cette machine. Ensuite, on lance remmina et on donne le nom et le mot de passe.










# minecraft

    root#  echo "games-server/minecraft-server Mojang" >> /etc/portage/package.license/minecraft
    root#  emerge -av games-server/minecraft-server

Ensuite on lance `minecraft-server` ce qui crée deux fichiers : `server.properties` et `eual.txt` que l'on doit modifier.

    lafrier$  minecraft-server
    lafrier$  nano server.properties
    nano:
      rcon.port=25575
      rcon.password=strong-password
      enable-rcon=true
    lafrier$  nano eula.txt
    nano:
      eula=true
    root#  gpasswd -a lafrier minecraft
    root#  cd /opt
    root#  wget https://launcher.mojang.com/download/Minecraft.tar.gz
    root#  tar -zxvf Minecraft.tar.gz
    root#  rm Minecraft.tar.gz
    root#  emerge -av gnome-base/gconf
    root#  emerge -av dev-python/pycurl
    root#  emerge -av net-misc/curl

# joystick usb x-box 360

Kernel!

    Device Drivers --->
    Input device support --->
    <*>   Joystick interface
    <*>   Event interface
    <*>   X-Box gamepad support
    [*]   Joysticks/Gamepads  --->
    <*>   X-Box gamepad support
    [*]     X-Box gamepad rumble support
    [*]     LED Support for Xbox360 controller 'BigX' LED
    Device Drivers --->
    HID support --->
    [*]   Battery level reporting for HID devices
    [*]   /dev/hidraw raw HID device support
    <*>   Generic HID driver
    
    root#  nano /etc/portage/make.conf
    nano:
      INPUT_DEVICES="synaptics libinput keyboard mouse evdev joystick"
    root#  emerge --ask --changed-use --deep @world
    root#  emerge -av x11-drivers/xf86-input-joystick
    root#  echo "games-util/joystick ~amd64" >> /etc/portage/package.accept_keywords/joystick
    root#  emerge -av games-util/joystick
    root#  echo "games-util/qjoypad ~amd64" >> /etc/portage/package.accept_keywords/joystick
    root#  emerge -av games-util/qjoypad
    root#  nano /etc/X11/xorg.conf
    nano:
      Section "InputDevice"
          Identifier     "X_Box-360"
	  Driver         "joystick"
	  Option         "Device"    "/dev/input/js0"
          Option         "SendCoreEvents"    "true"
      EndSection
    root#  gpasswd -a lafrier input

# antimicro

    root#  echo "games-util/antimicro ~amd64" >> /etc/portage/package.accept_keywords/antimicro
    root#  emerge -av games-util/antimicro


   







# samba

Kernel !

    File Systems --->
      [*] Network File Systems --->
        <*>   SMB3 and CIFS support (advanced network filesystem) 
        [*] Extended Statistics
        [*] CIFS Extended Attributes
	[*] CIFS POSIX Extentions

    root#  echo "net-fs/samba cups" >> /etc/portage/package.use/samba
    root#  nano /etc/portage/make.conf
    nano:
      USE="bluetooth pulseaudio X ffmpeg opengl tiff openal jack sdl fftw openexr expat samba"
    root#  emerge --ask --changed-use --deep @world


















# OUTILS CLOUD


## drive

root#  emerge -av net-misc/drive

lafrier$  mkdir ~/drivelafrier
lafrier$  drive help
lafrier$  drive ls

## grive

root#  emerge -av net-misc/grive





# OUTILS SHELL


## star-engine

Téléchargé les sources et les binaires sous
  /opt/Star-Engine-0.8.1-Sources
et
  /opt/Star-Engine-0.8.1-GNU-Linux64
  export MANPATH=$MANPATH:/opt/Star-Engine-0.8.1-GNU-Linux64/share/man
  export CPATH=$CPATH:/opt/Star-Engine-0.8.1-GNU-Linux64/include
  export LIBRARY_PATH=$LIBRARY_PATH:/opt/Star-Engine-0.8.1-GNU-Linux64/lib

## 4v_s

lafrier$  cd Transfert-local/Cora/Edstar
lafrier$  git clone https://gitlab.com/meso-star/star-4v_s.git
lafrier$  cd star-4v_s
lafrier$  mkdir build
lafrier$  cd build
lafrier$  cmake -DCMAKE_PREFIX_PATH=/opt/Star-Engine-0.8.1-GNU-Linux64/ ../cmake/
lafrier$  make
lafrier$  ./s4vs ~/Gros-volumes-local/Geometries/bunny.obj 10000

## meso-liptp

lafrier$  cd Transfert-local/Cora/Edstar 
lafrier$  git clone git@gitlab:meso-star/meso-litpt.git















<!-- ====================================================================== -->

# Les vieiles choses conservées pour mémoire

## GESTION DU RESEAU

### NetworkManager

! Kernel

    root#  emerge --ask --changed-use --deep @world
    root#  emerge --ask net-misc/networkmanager

Redémarrer en root :

    root#  rc-service NetworkManager restart

Redémarrer en user :

    lafrier$  nmcli networking off
    lafrier$  nmcli networking on

### wifi

    root#  emerge -av net-wireless/iw
    root#  emerge -av net-wireless/wpa_supplicant
    root#  visudo
    visudo:
      lafrier cpat20 = /sbin/shutdown -h now, /sbin/reboot, /usr/bin/nmcli
    nano /etc/polkit-1/rules.d/50-org.freedesktop.NetworkManager.rules
    nano:
      polkit.addRule(function(action, subject) {
          if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("plugdev")) {
              return polkit.Result.YES;
	  }
      });
    root#  reboot
    root#  nmtui

## elogind et/ou consolekit

### ATTENTION : pour elogind

dans ~/.xinitrc

exec dbus-launch --exit-with-session <WINDOW_MANAGER>

où WINDOW_MANAGER in the above example needs to be replaced by a window manager or a single application.

### CE QUI ETAIT FAIT AVANT ELOGIND

    root#  emerge -av sys-auth/consolekit 
    root#  rc-service consolekit status
    root#  /etc/init.d/consolekit start
    root#  rc-update add consolekit default

RQ: Il faudrait peut-être remplacer `consolekit` par `elogind`. Par exemple avec

    root#  nano /etc/portage/make.conf
    nano:
        USE="${USE} -consolekit elogind"
    root#  emerge -avDU @world 

Mais quand je l'ai fait j'ai perdu la gestion du laptop (freeze après fermeture de l'écran). Donc je suis revenu en arrière.

## vim

    root#  emerge -av app-vim/vim-r

## qtox

    root#  echo "=net-libs/tox-0.2.9-r1 ~amd64" >> /etc/portage/package.accept_keywords/tox
    root#  echo "=net-im/qtox-1.16.3-r1 ~amd64" >> /etc/portage/package.accept_keywords/tox
    root#  echo "media-video/ffmpeg v4l webp" >> /etc/portage/package.use/ffmpeg
    root#  emerge -av net-im/qtox

## son

Pour voir les micros disponibles (`*` devant celui qui est actif) :

    lafrier$  pacmd list-sources

Pour définir le micro par défaut (celui qui sera utilisé par firefox) :

    root#  nano /etc/pulse/default.pa
    nano:
    set-default-source alsa_input.usb-Focusrite_Scarlett_2i2_USB_Y8F9TCP9B27FD4-00.analog-stereo

où `alsa_input.usb-Focusrite_Scarlett_2i2_USB_Y8F9TCP9B27FD4-00.analog-stereo` est mon micro usb.

Pour définir le micro par défaut de façon temporaire :

    lafrier$  pacmd "set-default-source alsa_input.usb-Focusrite_Scarlett_2i2_USB_Y8F9TCP9B27FD4-00.analog-stereo"

Pour voir les sorties disponibles (`*` devant celle qui est active) :

    lafrier$  pacmd list-sinks

Pour définir la sortie par défaut (celle qui sera utilisée par firefox) :

    root#  nano /etc/pulse/default.pa
    nano:
    set-default-sink alsa_output.pci-0000_00_1b.0.analog-stereo

où `alsa_output.pci-0000_00_1b.0.analog-stereo` est la sortie casque de mon ordi.

Pour définir la sortie par défaut de façon temporaire :

    lafrier$  pacmd "set-default-sink alsa_output.pci-0000_00_1b.0.analog-stereo"

Et ensuite rebooter.

Pour mémoire :

    arecord -f cd -D hw:2,0 -d 2 test.wav
    arecord -l
    arecord -L

ffmpeg

    pacmd list-sources
    ffmpeg -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo out.wav
    ffmpeg -f pulse -i 3 out.wav
    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor out.wav
    ffmpeg -f pulse -i 2 out.wav
    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 0 out.wav -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo -map 1 in.wav
    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -map 0 out.wav -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo -map 1 in.wav -filter_complex "[0][1]amix[out]" -map "[out]" mix.wav

## Son

### Carte son

    root#  lspci | grep -i audio

Ex:

    00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 04)
    01:00.1 Audio device: NVIDIA Corporation GK104 HDMI Audio Controller (rev a1)

! Kernel

    SND_DYNAMIC_MINORS n -> y
    SND_HDA_CODEC_ANALOG n -> y
    SND_HDA_CODEC_CA0110 n -> y
    SND_HDA_CODEC_CA0132 n -> y
    SND_HDA_CODEC_CIRRUS n -> y
    SND_HDA_CODEC_CMEDIA n -> y
    SND_HDA_CODEC_CONEXANT n -> y
    SND_HDA_CODEC_HDMI n -> y 
    SND_HDA_CODEC_REALTEK n -> y 
    SND_HDA_CODEC_SI3054 n -> y
    SND_HDA_CODEC_SIGMATEL n -> y
    SND_HDA_CODEC_VIA n -> y
    SND_HDA_GENERIC n -> y
    SND_HDA_INPUT_BEEP n -> y
    SND_HDA_PATCH_LOADER n -> y
    SND_HDA_PREALLOC_SIZE 64 -> 2048
    SND_HDA_RECONFIG n -> y
    +SND_HDA_CODEC_CA0132_DSP y
    +SND_HDA_DSP_LOADER y
    +SND_HDA_INPUT_BEEP_MODE 1
    +SND_MAX_CARDS 32

### Gestion des sorties et des volumes

    root#  emerge -av media-sound/pavucontrol


## android

    root#  emerge -av sys-apps/usbutils sys-fs/mtpfs
    root#  mkdir /mnt/android
    root#  chown lafrier android
    root#  chgrp lafrier android
    root#  nano /etc/fstab
    nano:
      mtpfs /mnt/android fuse user,noauto,allow_other   0 0
    root#  nano /etc/fuse.conf
    nano:
      user_allow_other

### pour monter le téléphone ou la tablette

    lafrier$  mount mtpfs

### pour démonter le téléphone ou la tablette

    lafrier$  fusermount -u /mnt/android

## camera

Vérifier `dmesg` qui ne donnera rien :

    root#  dmesg | grep -i camera

Aller voir le matériel en usb :

    root#  lsusb -v | grep -i camera

Kernel !

Device Drivers > Multimedia support >
  Cameras/video grabbers support
  Media USB Adapters >
    USB Video Class (UVC)

Chercher UVC et tout prendre, notamment `USB webcam function` sous `USB gadget through configfs`

A la fin, on a un `/dev/video0`.

## wifi

### Carte wifi

    root#  lspci -k

Repérer la carte wifi. Pour moi :

    3d:00.0 Network controller: Intel Corporation Wireless 7260 (rev 6b)
    Subsystem: Intel Corporation Dual Band Wireless-AC 7260

RQ: Pour les Intel, c'est souvent le module IWLWIFI que l'on doit utiliser avec MVM.

RQ: Attention ! Mettre IWLWIFI (et ceux d'en dessous) en modules (et non pas compilés dans le noyau).

! Kernel

    CFG80211_WEXT n -> y
    CRYPTO_AES_NI_INTEL n -> y
    CRYPTO_CRYPTD n -> y
    IWLWIFI n -> m
    +COMPAT_NETLINK_MESSAGES y
    +CRYPTO_GLUE_HELPER_X86 y
    +CRYPTO_SIMD y
    +DEV_COREDUMP y
    +IWLDVM m
    +IWLMVM m
    +IWLWIFI_BCAST_FILTERING n
    +IWLWIFI_DEBUG n
    +IWLWIFI_DEVICE_TRACING y
    +IWLWIFI_LEDS y
    +IWLWIFI_OPMODE_MODULAR y
    +WANT_DEV_COREDUMP y
    +WEXT_CORE y
    +WEXT_PROC y

### Configurer une connection

    root#  nmtui

## clavier

    root#  emerge -av x11-apps/setxkbmap
    root#  nano /etc/conf.d/keymaps
    nano:
      keymap="fr-pc"
    root#  nano /etc/X11/xorg.conf
    nano:
      Section "InputDevice"
      Identifier     "Keyboard0"
      Driver         "kbd"
      Option         "XkbLayout"    "fr-pc"
      EndSection

## DATE

! Kernel pour le "in kernel" dans la gestion du "hardware clock"

root#  rc-update delete hwclock boot
root#  emerge -av net-misc/openntp
root#  nano /etc/ntpd.conf
nano:
  servers ntp.univ-tlse3.fr
root#  rc-service ntpd start
root#  ntpd -d -s
root#  rc-update add ntpd default
root#  hwclock --systohc

## CONTROLE THERMIQUE

### lm_sensors

! Kernel

root#  emerge -av sys-apps/lm-sensors
root#  sensors-detect
root#  rc-service lm_sensors start
root#  rc-update add lm_sensors default

## BATTERIE

! Kernel

### Nouvelle version

    root#  emerge -av sys-power/thermald
    root#  rc-config add thermald
    root#  emerge -av app-laptop/laptop-mode-tools
    root#  rc-service laptop_mode start
    root#  rc-update add laptop_mode default
    root#  nano /etc/laptop-mode/conf.d/ethernet.conf
    nano:
      #ETHERNET_DEVICES="eth0"
      ETHERNET_DEVICES="enp0s25 wlp61s0"
    root#  nano /etc/elogind/logind.conf
    nano:
      HandleLidSwitch=ignore

### Ancienne version

root#  emerge -av app-laptop/laptop-mode-tools
root#  emerge -av sys-power/thermald
root#  rc-config add thermald
root#  emerge -av sys-apps/hdparm
root#  vim /etc/laptop-mode/conf.d/ethernet.conf
vim:
  #ETHERNET_DEVICES="eth0"
  ETHERNET_DEVICES="enp0s25 wlp61s0"
root#  rc-service laptop_mode start
root#  rc-update add laptop_mode default 
root#  echo "sys-libs/libselinux python" >> /etc/portage/package.use/selinux
root#  echo "sys-libs/libsemanage python" >> /etc/portage/package.use/selinux
root#  emerge -av sys-power/acpid
root#  emerge -av sec-policy/selinux-shutdown
root#  /etc/init.d/acpid start
root#  rc-update add acpid default

### Ancienne version

RQ: J'avais fait ça à un moment ...

root#  vim /etc/laptop-mode/conf.d/cpufreq.conf
vim:
  CONTROL_CPU_FREQUENCY=0
  BATT_CPU_GOVERNOR=performance
  LM_AC_CPU_GOVERNOR=performance
  NOLM_AC_CPU_GOVERNOR=performance

RQ: Name of the active CPUFreq governor is available in: /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_governor

## GESTION DU SERVEUR GRAPHIQUE

Pour intel (Vincent) :

root#  ifconfig -a
root#  echo "ifconfig enp59s0f1 hw ether 38:C9:89:14:42:40" > upd_mac.sh
root#  sh upd_mac.sh
root#  /etc/init.d/net.lo restart
root#  /etc/init.d/enp59s0f1 restart
root#  rc-status -a
root#  /etc/init.d/wpa_supplicant start

Recompiler le noyau et rebooter

root#  emerge -s wireless-regdb
root#  emerge -pv wireless-regdb
root#  emerge -av wireless-regdb
root#  ifconfig -a
root#  lsmod
root#  modprobe iwlmvm
root#  lsmod
root#  modprobe iwlwifi
root#  lspci -nnkv
root#  dmesg | grep 40:00
root#  less /etc/fstab
root#  lspci -nnkv
root#  dmesg | grep 8086:2723
root#  ifconfig
root#  dmesg
root#  modprobe iwlmvm
root#  ifconfig
root#  lsmpr
root#  lspro
root#  lsmod
root#  udev
root#  /etc/init.d/udev start
root#  /etc/init.d/udev restart
root#  dmesg
root#  equery y gentoo-sources
root#  echo "=sys-kernel/gentoo-sources-5.4.10 ~amd64"
root#  echo "=sys-kernel/gentoo-sources-5.4.10 ~amd64" >> /etc/portage/package.accept_keywords/kernel
root#  emerge -av gentoo-sources
root#  make menuconfig

Recompiler le noyau et redémarer

root#  setxkbmap | me
root#  glxinfo | more
root#  ls /dev/
root#  sh upd_mac.sh
root#  /etc/init.d/enp59s0f1 restart
root#  lspci
root#  less /proc/cpuinfo
root#  make menuconfig
root#  ifconfig
root#  dmesg
root#  ifconfig -a
root#  clear
root#  ifconfig
root#  eselect kernel list
root#  eselect kernel set 2
root#  less /etc/portage/package.accept_keywords/kernel
root#  equery y
root#  equery y gentoo-sources
root#  less upd_mac.sh
root#  ifoncfig -a
root#  emerge -av mesa-progs
root#  lspci -nnvk | more
root#  vim /etc/portage/make.conf
vim:
?????
root#  emerge @preserved-rebuild
root#  emerge -s libdrm
root#  emerge --changed-use -av @world
root#  emerge -pv mesa
root#  emerge -pv libdrm
root#  emerge @preserved-rebuild
root#  #emerge x11-drivers/xf86-video-nouveau
root#  emerge --depclean -av
root#  emerge -D -av mesa

## python

root#  eselect python list
root#  eselect python set 2
root#  emerge -av dev-python/pip
root#  emerge -av dev-lang/python:3.7
root#  eselect python set 3
root#  emerge -av --update --newuse --deep @world
root#  emerge -av dev-python/matplotlib
root#  emerge -av dev-python/PyQt5
root#  emerge -av dev-python/pyxdg

## anaconda

root#  emerge -av x11-libs/libXau x11-libs/libxcb x11-libs/libX11 x11-libs/libXext x11-libs/libXfixes x11-libs/libXrender x11-libs/libXi x11-libs/libXcomposite x11-libs/libXrandr x11-libs/libXcursor x11-libs/libXdamage x11-libs/libXScrnSaver x11-libs/libXtst media-libs/mesa

Télécharger l'instalateur pour python3.7 (Anaconda3-2019.10-Linux-x86_64.sh) sur https://www.anaconda.com/distribution/#linux

lafrier$  sha256sum Anaconda3-2019.10-Linux-x86_64.sh
lafrier$  bash Anaconda3-2019.10-Linux-x86_64.sh
installation>
  répertoire : /home/lafrier/Gros-volumes-local/Python/anaconda3
  Do you wish the installer to initialize Anaconda3 by running conda init? [yes|no] : yes
lafrier$  source ~/.bashrc
lafrier$  conda install netcdf4 h5py jinja2 pip sqlalchemy basemap matplotlib numpy scipy
lafrier$  conda create --name python2 python=2.7
lafrier$  conda create --name python3 python=3.7

lafrier$  conda create -n FERRET -c conda-forge pyferret ferret_datasets --yes

lafrier$  vim ~/.bashrc
vim>
  conda deactivate

Remarque : Pour activer conda
  lafrier$  conda activate
et pour activer ferret via conda
  lafrier$  conda activate FERRET

## jupyter notebook

root#  emerge -av dev-python/notebook  ???? Ne marche pas

## lmdz

lafrier$  cd ~/Gros-volumes-local/LMDZ
lafrier$  wget http://www.lmd.jussieu.fr/~lmdz/pub/install_lmdz.sh
lafrier$  chmod +x install_lmdz.sh
lafrier$  ./install_lmdz.sh

lafrier$  cd LMDZtrunk/modipsl/modeles/LMDZ/BENCH32x32x39
lafrier$  conda activate FERRET
lafrier$  ferret
ferret>
  yes? use histday.nc
  yes? shade tsol ; go land

## Xfce

root#  eselect profile set default/linux/amd64/17.0/desktop

root#  vim /etc/portage/package.use/xfce

vim:
  app-text/poppler -qt5
root#  emerge --ask xfce-base/xfce4-meta xfce-extra/xfce4-notifyd
root#  emerge --ask --deselect=y xfce-extra/xfce4-notifyd
root#  emerge --ask x11-themes/gtk-engines-xfce
root#  emerge --ask xfce-extra/xfce4-battery-plugin
root#  emerge --ask xfce-extra/xfce4-verve-plugin
root#  emerge --ask xfce-extra/xfce4-sensors-plugin
root#  emerge --ask xfce-extra/xfce4-power-manager
root#  emerge -av xfce-extra/xfce4-alsa-plugin

lafrier$  startxfce4

## Debugs de vincent

### 1

root # emerge --unmerge -a eselect-opengl
root # emerge -av nvidia-drivers 

==>

!!! Error: Can't load module opengl
exiting
!!! File Not Found: '/etc/env.d/51opencascade'

### 2

penser à env update

