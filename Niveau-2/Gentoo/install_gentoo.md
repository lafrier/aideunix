# Préparer une clé bootable

## Telecharger le cd d'installation minimale

A partir d'une autre machine :

    root#  cd /root/tmp
    root#  wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20201222T005811Z/install-amd64-minimal-20201222T005811Z.iso

## Ecriture sur une clé usb

Après avoir inséré une clé, en suppossant que *la clé est sur /dev/sdc* :

    root#  dd if=/root/install-amd64-minimal-20200920T214503Z.iso of=/dev/sdc bs=8192k 
    root#  sync

## Apres le boot sur la clé

Pendant le boot, on peut choisir le clavier : fr

On se retrouve directemet sous root et on commence par tester le réseau :

    livecd# ping gnu.org

RQ: Si le ping ne fonctionne pas, utiliser `nmtui` pour activer une connexion wifi.

# Partitionnement

## Effacement du partitionnement précédent

Si le disque à effacer est `/dev/sda` :

    root#  parted -a optimal /dev/sda
    parted:
      mklabel gpt
      unit mib

## Création de la partition grub

    parted:
      mkpart primary 1 3
      name 1 grub
      set 1 bios_grub on
      print

## Création de la partition boot EFI

    parted:
      mkpart primary 3 515
      name 2 boot
      set 2 boot on                                                    
      print  

## Creation de la partition swap

    parted:
      mkpart primary 515 40000
      name 3 swap
      print

## Création de la partion `/` sur le reste du disque

    parted:
      mkpart primary 40000 -1
      name 4 rootfs
      print
      quit

# Formatage

## Premiere partition (probablement pour rien)

    root#  mkfs.ext4 /dev/sda1

## Partition boot EFI

    root#  mkfs.fat -F 32 /dev/sda2

## Partition swap

    root#  mkswap /dev/sda3

## Partition root

    root#  mkfs.ext4 /dev/sda4

# Montage

## Partition swap

    root#  swapoff -v /dev/sda3
    root#  swapon -v /dev/sda3

## Partition root

RQ: Si besoin, faire `mkdir /mnt/gentoo`

    root#  mount /dev/sda4 /mnt/gentoo

# Réglage de la date

    root#  ntpd -q -g

# Téléchargement de l'archive

RQ: Aller chercher la bonne adresse pour le stage3 sur le site de gentoo.

    root#  cd /mnt/gentoo
    root#  wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/20201222T005811Z/stage3-amd64-20201222T005811Z.tar.xz
    root#  tar xpvf stage3-amd64-20200923T214503Z.tar.xz --xattrs-include='*.*' --numeric-owner

# `chroot`

## Choisir le miroir et les dépots

    root#  mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf

RQ: En partant sur une clé ubuntu, `mirrorselect` n'existe pas. J'ai directement édité le `make.conf` pour y mettre les mirroirs français recommandés sur le site de gentoo. Par exemple : `GENTOO_MIRRORS="http://ftp.free.fr/mirrors/ftp.gentoo.org/ http://gentoo.modulix.net/gentoo/ http://gentoo.mirrors.ovh.net/gentoo-distfiles/ http://mirrors.soeasyto.com/distfiles.gentoo.org/ ftp://ftp.wh2.tu-dresden.de/pub/mirrors/gentoo http://ftp.halifax.rwth-aachen.de/gentoo/ http://ftp.fau.de/gentoo"`

    root#  mkdir --parents /mnt/gentoo/etc/portage/repos.conf
    root#  cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

## Le DNS

    root#  cp --dereference /etc/resolv.conf /mnt/gentoo/etc/

## Montages

    root#  mount --types proc /proc /mnt/gentoo/proc
    root#  mount --rbind /sys /mnt/gentoo/sys
    root#  mount --make-rslave /mnt/gentoo/sys
    root#  mount --rbind /dev /mnt/gentoo/dev
    root#  mount --make-rslave /mnt/gentoo/dev 

## Entree dans le nouvel environnement

    root#  chroot /mnt/gentoo /bin/bash
    root#  source /etc/profile
    root#  export PS1="(chroot) ${PS1}"
    root#  ping gnu.org

## Montage de la partition boot

    root#  mount /dev/sda2 /boot

## Configuration de Portage

    root#  emerge-webrsync
    root#  emerge --sync
    root#  eselect news list
    root#  eselect news read

RQ: Suite au news, dans le `etc/portage/make.conf` on rajoute la ligne `L10N="fr"`

    root#  eselect profile list
    root#  emerge --ask --verbose --update --deep --newuse @world

## Timezone

    root#  echo "Europe/Paris" > /etc/timezone
    root#  emerge --config sys-libs/timezone-data

## Locales

Remplacer le contenu du fichier locale.gen et faire la génération :

    root#  nano /etc/locale.gen
    nano:
      fr_FR.UTF-8 UTF-8
    root#  locale-gen
    root#  eselect locale list
    root#  eselect locale set 4
    root#  source /etc/profile
    root#  nano /etc/env.d/02locale
    nano:
      LC_COLLATE="C"
    root#  env-update && source /etc/profile && export PS1="(chroot) ${PS1}"

# Configuration du montage des disques

RQ: Si on souhaite mettre les UUID dans le fstab, executer la commande blkid

    root#  nano /etc/fstab
    nano:
      /dev/sda2    /boot   vfat   noatime           0 2
      /dev/sda3    none    swap   sw                0 0
      /dev/sda4    /       ext4   noatime,discard   0 1

# Configuration du noyau

## Installation des sources

    root#  emerge --ask sys-kernel/gentoo-sources

## Installation des outils systeme

    root#  emerge --ask sys-apps/pciutils
    root#  emerge --ask sys-apps/usbutils
    root#  mkdir /etc/portage/package.license
    root#  nano -w /etc/portage/package.license/linux-firmware
    nano:
      sys-kernel/linux-firmware linux-fw-redistributable no-source-code
    root#  emerge --ask sys-kernel/linux-firmware 

Pour mémoire, mais n'est plus nécessaire :

    root#  cd /etc/portage
    root#  mv ._cfg0000_package.license cfg0000_package.license
    root#  emerge --ask sys-kernel/linux-firmware

## Recupperer les informations sur le matériel et les drivers tels que vus par la cle bootable

    root#  lspci -k
    root#  lsmod

## La configuration manuelle

    root#  ls -l /usr/src/linux
    root#  cd /usr/src/linux
    root#  make menuconfig

RQ: Sortir sans rien changer, sauf si les informations sur le matériel nécessient quelque chose de particulier dès ce stade. Mais en général, tous les problèmes matériel (carte réseau, carte graphique, etc) peuvent être gérées ultérieurement. Il est donc préférable de ne rien toucher par rapport à la configuration qui arrive par défaut. 

    root#  cd /usr/src/linux
    root#  make
    root#  make modules_install
    root#  make install

## Les disques NVM

Lorsque les disque portent un nom du type /dev/nvme... il faut rajouter le driver correspondant.

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        <*> NVM Express block device
    root#  make
    root#  make modules_install
    root#  make install

RQ: Penser à refaire le grub si c'est après une erreur.

## Les carte ethernet par connexion USB Gigabytes

Se débrouiller pour trouver le nom du materiel, dans mon cas Realtek-USB-GBE-Ethernet-Controller qui se pilote avec R8512.

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers --->
        <*> USB Network Adapters --->
	   <*> R8512
    root#  make
    root#  make modules_install
    root#  make install

RQ: Penser à refaire le grub si c'est après une erreur.

# Configuration système

## Les paramètres réseau

    root#  nano -w /etc/conf.d/hostname
    nano:
      hostname="nom-machine"
    root#  emerge --ask --noreplace net-misc/netifrc
    root#  emerge --ask net-misc/dhcpcd
    root#  nano -w /etc/conf.d/net
    nano:
      config_eth0="dhcp"
    root#  cd /etc/init.d
    root#  ln -s net.lo net.eth0
    root#  rc-update add net.eth0 default

## Password de root

   root#  passwd

## Clavier

RQ: J'ai mis `fr-pc` mais `fr` est possible.

    root#  nano -w /etc/conf.d/keymaps
    nano:
    keymap="fr-pc"

## Heure systeme

Je n'ai rien fait de ce qui suit (si ça fonctionne, supprimer cette section)

    root#  nano -w /etc/conf.d/hwclock

RQ: Je n'ai rien changé.

    root#  rc-update delete hwclock boot

RQ: Peut-être ne pas faire ce delete

## System logger

    root#  emerge --ask app-admin/sysklogd
    root#  rc-update add sysklogd default

## Cron

    root#  emerge --ask sys-process/cronie
    root#  rc-update add cronie default

## File indexing

    root#  emerge --ask sys-apps/mlocate

## Filesystem tools

    root#  emerge --ask sys-fs/e2fsprogs
    root#  emerge --ask sys-fs/xfsprogs
    root#  emerge --ask sys-fs/reiserfsprogs
    root#  emerge --ask sys-fs/jfsutils
    root#  emerge --ask sys-fs/dosfstools
    root#  emerge --ask sys-fs/btrfs-progs

## Client DHCP

    root#  emerge --ask net-misc/dhcpcd

## Client PPPoE

    root#  emerge --ask net-dialup/ppp

## Wifi

    root#  emerge --ask net-wireless/iw net-wireless/wpa_supplicant

# Configurer le bootloader

## Monter `/boot`

    root#  mount /boot

## Grub2

    root#  nano /etc/portage/make.conf
    nano:
      GRUB_PLATFORMS="efi-64"
    root#  emerge --ask --verbose sys-boot/grub:2
    root#  grub-install --target=x86_64-efi --efi-directory=/boot --removable
    root#  grub-mkconfig -o /boot/grub/grub.cfg

## Reboot

    root#  exit
    root#  cd
    root#  umount -l /mnt/gentoo/dev{/shm,/pts,}
    root#  umount -R /mnt/gentoo
    root#  reboot

<!-- ====================================================================== -->

# Réseau ethernet

    root#  ifconfig -a

RQ: Noter le nom de la carte réseau (ici enp0s25).

    root#  mv /etc/init.d/net.eth0 /etc/init.d/net.enp0s25
    root#  /etc/init.d/net.enp0s25 start

# Police de caractères

Lorsque l'écran est de très haute résolution, la police utilisée par défaut pour le terminal TTY est trop petite. On télécharge alors les polices `terminus` et on choisi `ter-v32b` :

    root#  emerge -av media-fonts/terminus-font
    root#  nano /etc/conf.d/consolefont
    nano:
      consolefont="ter-v32b"
    root#  rc-update add consolefont boot
    root#  reboot

# Points de montage

    root#  mkdir /mnt/key

# Portage

    root#  emerge -av app-portage/gentoolkit

Si ils n'existent pas, creer les repertoires `/etc/portage/package.accept_keyword` `/etc/portage/package.license` et `/etc/portage/package.use`

    root#  mkdir /etc/portage/package.accept_keywords 
    root#  mkdir /etc/portage/package.license
    root#  mkdir /etc/portage/package.use

# Définir le répertoire de sauvegarde des noyaux et faire une sauvegarde

    root#  mkdir /root/kernels
    root#  nano /root/.bashrc
    nano:
      export KERNELS=/root/kernels

RQ: Nous faisons ici une première sauvegarde. Elle est à refaire dans ce qui suit après chaque modification du noyau réussie.

    root#  cp /usr/src/linux/.config $KERNELS/config-`uname -r`-`date +%Y%m%d%H%M%S`

# elogind

Pour gérer ce qui ne fonctionnerait pas sans systemd.

    root#  nano /etc/portage/make.conf
    nano:
      USE="elogind -consolekit -systemd"
    root#  emerge --ask --changed-use --deep @world

RQ: Accepter les USE Changes et recopier `/etc/portage/package.use/._cfg0000_zz-auounmask` en un fichier portant le nom du paquet concerné. Par exemple `/etc/portage/package.use/python`. Si plusieurs paquets sont mentionnés dans le fichier, créer un fichier par paquet. Puis relancer le emerge.

    root#  emerge --ask --changed-use --deep @world
    root#  rc-update add elogind boot

RQ: Attention ! Voir ce qu'il faut mettre dans le `.xinitrc` des users et voir les questions d'hibernation dans la doc gentoo de elogind.

# dbus

Pour les communications entre services depuis les applications.

    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} dbus"
    root#  emerge --ask --changed-use --deep @world
    root#  /etc/init.d/dbus start
    root#  rc-update add dbus default

# Les bases

    root#  emerge -av sys-kernel/linux-firmware
    root#  emerge -av app-editors/nano
    root#  emerge -av net-dns/bind-tools
    root#  emerge -av app-editors/vim
    root#  emerge -av dev-vcs/git
    root#  emerge -av sys-devel/gdb
    root#  emerge -av app-shells/bash-completion
    root#  emerge -av sys-apps/lshw
    root#  emerge -av app-crypt/gnupg
    root#  emerge -av dev-vcs/tig
    root#  emerge -av dev-vcs/git-lfs
    root#  emerge -av app-misc/rlwrap
    root#  emerge -av app-text/tree
    root#  emerge -av dev-util/oprofile
    root#  emerge -av virtual/jdk
    root#  emerge -av virtual/jre
    root#  emerge -av dev-util/cmake
    root#  emerge -av sys-fs/fuse-exfat
    root#  emerge -av sys-fs/exfat-utils
    root#  cp /etc/skel/.bashrc /root/.
    root#  cp /etc/skel/.bash_profile /root/.
    root#  cp /etc/skel/.bash_logout /root/.
    root#  nano /root/.bashrc
    nano:
      export KERNELS=/root/kernels

# Créer un user

    root#  useradd -m -G users,wheel,audio,video -s /bin/bash lafrier
    root#  passwd lafrier

# Cryptage

## dm-crypt

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers --->
        [*] Multiple devices driver support (RAID and LVM) --->
	  <*>   Crypt target support
      [*] Cryptographic API --->
        <*> XTS support
        <*> SHA224 and SHA256 digest algorithm
	<*> AES cipher algorithms
        <*> User-space interface for hash algorithms
	<*> User-space interface for symmetric key cipher algorithms
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot
    root#  emerge -av sys-fs/cryptsetup

## Cryptage de `/home/lafrier` (300G)

    root#  mv /home/lafrier /home/lafrier.orig
    root#  mkdir /home/lafrier
    root#  chown lafrier /home/lafrier
    root#  chgrp lafrier /home/lafrier
    root#  dd if=/dev/zero of=/home/lafrier.crypted bs=1G count=300
    root#  cryptsetup -y luksFormat /home/lafrier.crypted
    root#  cryptsetup luksOpen /home/lafrier.crypted encrypted_part_lafrier
    root#  mkfs.ext4 -j /dev/mapper/encrypted_part_lafrier
    root#  mount /dev/mapper/encrypted_part_lafrier /home/lafrier
    root#  chown lafrier /home/lafrier
    root#  chgrp lafrier /home/lafrier
    root#  umount /home/lafrier
    root#  cryptsetup luksClose /dev/mapper/encrypted_part_lafrier
    root#  cryptsetup luksHeaderBackup /home/lafrier.crypted --header-backup-file /home/lafrier.luks-header

Rq : Le fichier /home/lafrier.luks-header est une sauvegarde de l'entête du fichier crypté. Si jamais cette entête était corrompue, il ne serait plus possible de décrypter le fichier. On le sauvegarde pour pouvoir le rétablir si besoin. Il faut donc sauvegarder ce fichier d'entête en un endroit sûr. Par exemple :

    root#  mkdir /home/Headers
    root#  cp /home/lafrier.luks-header /home/Headers/.

### Ouverture et montage du home

    root#  cryptsetup luksOpen /home/lafrier.crypted encrypted_part_lafrier
    root#  mount /dev/mapper/encrypted_part_lafrier /home/lafrier

### Recopie du contenu de `/home/lafrier.orig`

    root#  ls -a /home/lafrier.orig
    root#  su - lafrier
    lafrier$  mv /home/lafrier.orig/.bash* /home/lafrier/.
    lafrier$  mv /home/lafrier.orig/.ssh /home/lafrier/.
    lafrier$  mv /home/lafrier.orig/.viminfo /home/lafrier/.
    lafrier$  Ctrl-d
    root#  rm -r /home/lafrier.orig

### Demontage du home

    root#  umount /home/lafrier
    root#  cryptsetup luksClose /dev/mapper/encrypted_part_lafrier

### Monter automatiquement le home au démarrage

RQ: Dans `/etc/conf.d/dmcrypt`, écrire après `/home with passphrase` et non pas à la fin du fichier.

    root#  nano /etc/conf.d/dmcrypt
    nano:
      rc_need="fsck root keymaps"
      target=encrypted_part_lafrier
      source=/home/lafrier.crypted
    root#  nano /etc/conf.d/localmount
    nano:
      rc_need="dmcrypt"
    root#  nano /etc/fstab
    nano:
      /dev/mapper/encrypted_part_lafrier   /home/lafrier   ext4   noatime   0 2
    root#  rc-update add dmcrypt boot
    root#  reboot

<!-- ====================================================================== -->

# wifi

## Trouver sa carte wifi

Chercher `Wireless` dans la sortie de la commande suivante :

    root#  lspci -k

## intel

RQ: Attention ! Dans ce qui suit, bien penser à laisser `iwlwifi` en module (`<M>` et non pas `<*>`).

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        [*] Network device support  --->
	  [*]   Wireless LAN  --->
	    [*]   Intel devices
	      <M>     Intel Wireless WiFi Next Gen AGN - Wireless-N/Advanced-N/Ultimate-N (iwlwifi)
	      <M>       Intel Wireless WiFi DVM Firmware support
	      <M>       Intel Wireless WiFi MVM Firmware support
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot

Pour vérifier que le module `iwlwifi` est bien associé à la carte :

    root#  lspci -k

## Trouver le nom de son interface

    root#  ls /sys/class/net

Noter le nom commençant par `wlp`. Dans ce qui suit, j'utilise `wlp50s0`.

## Activer `wpa_supplicant`

    root#  rc-update add dhcpcd default
    root#  rc-service dhcpcd restart
    root#  nano /etc/wpa_supplicant/wpa_supplicant.conf
    nano:
      ctrl_interface=/var/run/wpa_supplicant
      ctrl_interface_group=wheel
      update_config=1
      ap_scan=1
    root#  wpa_supplicant -B -i wlp59s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
    root#  wpa_cli
    wpa_cli:
      > scan
      OK
      <3>CTRL-EVENT-SCAN-RESULTS
      > scan_results
      bssid / frequency / signal level / flags / ssid
      00:00:00:00:00:00 2462 -49 [WPA2-PSK-CCMP][ESS] MYSSID
      11:11:11:11:11:11 2437 -64 [WPA2-PSK-CCMP][ESS] ANOTHERSSID
      > add_network
      0
      > set_network 0 ssid "MYSSID"
      > set_network 0 psk "passphrase"
      > enable_network 0
      <2>CTRL-EVENT-CONNECTED - Connection to 00:00:00:00:00:00 completed (reauth) [id=0 id_str=]
      > save_config
      OK
      > quit

Enlever le cable et tester la connexion avec

    root#  ping gnu.org

Ensuite installer le service :

    root#  reboot
    root#  rc-service wpa_supplicant start
    root#  ping gnu.org
    root#  rc-update add wpa_supplicant default
    root#  reboot
    root#  ping gnu.org

<!-- ====================================================================== -->

# Graphique

## nvidia

Si vous avez une carte nvidia :

    root#  echo "x11-drivers/nvidia-drivers -abi_x86_32" >> /etc/portage/package.use/nvidia-drivers
    root#  emerge -av x11-drivers/nvidia-drivers
    root#  nano /etc/portage/make.conf
    nano:
      VIDEO_CARDS="nvidia"
    root#  emerge --ask --changed-use --deep @world

## intel

Si vous avez une carte intel :

RQ: J'ai mis i965, mais il existe aussi i915 ...

    root#  nano /etc/portage/make.conf
    nano:
      VIDEO_CARDS="intel i965"
    root#  emerge --ask --changed-use --deep @world

## inputs

    root#  nano /etc/portage/make.conf
    nano:
      INPUT_DEVICES="synaptics libinput keyboard mouse evdev"
    root#  emerge --ask --changed-use --deep @world

## xorg

    root#  emerge -av x11-base/xorg-server

RQ: Accepter les USE Changes et recopier `/etc/portage/package.use/._cfg0000_zz-auounmask` en un fichier portant le nom du paquet concerné. Par exemple `/etc/portage/package.use/python`. Si plusieurs paquets sont mentionnés dans le fichier, créer un fichier par paquet. Puis relancer le emerge.

    root#  emerge -av x11-base/xorg-server
    root#  emerge -av x11-apps/setxkbmap
    root#  emerge -av x11-apps/xinput
    root#  emerge -av x11-misc/xdotool
    root#  emerge -av x11-misc/wmctrl
    root#  env-update
    root#  source /etc/profile
    root#  emerge -av x11-vm/twm
    root#  emerge -av x11-terms/xterm
    root#  emerge -av x11-terms/xfce4-terminal
    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} X"
    root#  emerge --ask --changed-use --deep @world
    root#  reboot
    root#  startx
    root#  setxkbmap fr

RQ: Cela permet juste de tester que le graphique, la souris, le touchpad et le clavier fonctionnent. On ferme ensuite avec `Ctrl-Alt-F1` et `Ctrl-c`.

## xrandr

    root#  emerge -av x11-apps/xrandr
    root#  startx
    root#  xrandr

Cela renvoie le nom de l'écran suivi d'une liste, par exemple

    Screen 0: ...
    eDP-1 connected primary ...
    1920x1080 ...
    ...

Dans la liste, choisir la résolution (par exemple du full HD) et basculer dans cette résolution :

    root#  xrandr --output eDP-1 --mode 1920x1080

<!-- ====================================================================== -->

# openbox

    root#  emerge -av x11-wm/openbox
    root#  emerge -av x11-misc/menumaker
    root#  emerge -av x11-misc/obconf
    root#  emerge -av xfce-base/thunar
    root#  emerge -av x11-misc/pcmanfm
    root#  nano /etc/portage/package.use/tint2 
    nano:
      x11-misc/tint2 tint2conf
    root#  emerge -av x11-misc/tint2

RQ: Accepter les USE Changes et recopier `/etc/portage/package.use/._cfg0000_zz-auounmask` en un fichier portant le nom du paquet concerné. Par exemple `/etc/portage/package.use/x11`. Si plusieurs paquets sont mentionnés dans le fichier, créer un fichier par paquet. Puis relancer le emerge.

    root#  emerge -av x11-misc/pcmanfm
    root#  emerge -av media-gfx/feh

Passer sous le user pour faire les tests.

    lafrier$  nano ~/.xinitrc
    nano:
      #!/bin/bash
      xrandr --output eDP-1 --mode 1920x1080
      exec openbox-session
    lafrier$  startx

Sortir de openbox en utilisant le menu via le bouton de droite de la souris.

    lafrier$  cp -r /etc/xdg/openbox ~/.config/openbox
    lafrier$  mv ~/.config/openbox/menu.xml ~/.config/openbox/menu.xml.orig
    lafrier$  mmaker -v OpenBox3
    lafrier$  vim ~/.config/openbox/autostart
    nano:
      source ~/.bashrc
      tint2 &
      setxkbmap fr
      wallpaper="/home/lafrier/.config/Fonds-ecran/image.jpg"
      feh --bg-scale ${wallpaper} &
    lafrier$  openbox --reconfigure

RQ: Cette dernière commande renvoie une erreur concernant la variable `DISPLAY`. Ce n'est pas grave.

    lafrier$  tint2 &

<!-- ====================================================================== -->

# alsa

## Trouver sa carte son

    root#  lspci -k

Chercher `Audio` et vérifier qu'un driver est bien associé à la carte (ici `snd_hda_intel`).

## Configurer le noyau

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers --->
        <*> Sound card support
	  <*> Advanced Linux Sound Architecture --->
	    [*] Enable OSS Emulation
	    <*>     OSS Mixer API
	    <*>     OSS PCM (digital audio) API
	    [*]       OSS PCM (digital audio) API - Include plugin system (NEW)
            [*] PCI sound devices  --->
	    [*] mettre les drivers qui correspondent à la carte
	    [*] pour intel j'ai mis les deux intel disponibles
    menuconfig:
      Device Drivers --->
        <*> Sound card support
	  <*> Advanced Linux Sound Architecture --->
            HD-Audio  --->
              [*] les prendre tous
	      [*] les prendre tous
	      [*] les prendre tous
	      ...
	    Generic devices --->
	      <*> Virtual MIDI soundcard
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot

## Définir `alsa` comme default

    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} alsa -pulseaudio"
    root#  emerge --ask --changed-use --deep @world
    root#  emerge -av media-libs/alsa-lib
    root#  emerge -av media-libs/libsamplerate
    root#  emerge -av media-sound/alsa-utils
    root#  emerge -av media-sound/apulse
    root#  emerge -av media-sound/alsa-tools
    root#  emerge -av sys-firmware/alsa-firmware
    root#  rc-service alsasound start
    root#  rc-update add alsasound boot

## Connaître le nom des cartes vues par le noyau

    lafrier$  cat /sys/class/sound/card*/id

ou

    lafrier$  ls -l /dev/snd/

ou encore

    lafrier$  cat /proc/asound/cards

On obtient par exemple :

    0 [VirMIDI        ]: VirMIDI - VirMIDI
                         Virtual MIDI Card 1
    1 [PCH            ]: HDA-Intel - HDA Intel PCH
	   	         HDA Intel PCH at 0xed518000 irq 149

Dans cet exemple, on voit que la première carte vue est une carte MIDI virtuelle (label `VirMIDI`), puis la carte interne qui est une carte intel (label `PCH`).

## Régler les niveaux pour le test

Lancer `alsamixer` :

    lafrier$  alsamixer

Appuyer sur la touche `F6` et sélectionner la carte son interne (typiquement `PCH`) et avec les touches `F3` et `F4` visiter la partie lecture et la partie capture (Attention ! une partie du réglage des micros est dans la partie lecture). En appuyant sur la touche `M` démutter toutes les lignes importantes. En utilisant les flèches du clavier régler les niveaux sonores.

Dans le doute, tout activer et mettre les niveaux au maximum dans un premier temps.

## Tester la lecture

Pour tester la carte numéro 1 (ici la carte interne `PCH`) :

    lafrier$  speaker-test -c2 --device="hw:1,0"

Vous devez entendre un souffle passant de gauche à droite dans les enceintes de façade (pour un portable) ou dans le casque (si vous le branchez).

## Tester la capture

Pour tester la carte numéro 1 (ici la carte interne `PCH`) :

    lafrier$  arecord -vv -f dat --device="hw:1,0" /dev/null

Vous devez voir la barre inférieure évoluer en fonction du niveau de capture.

ATTENTION ! Dans `alsamixer` penser à sélectionner `Capture` et appuyer sur la barre d'espace pour que `G CAPTURE D` apparaisse sous `Capture`.

## Test capture - lecture

Pour tester a carte numéro 1 (ici la carte interne `PCH`) on lance un enregistrement de 10 secondes :

    lafrier$  arecord -c2 -d 10 --device="hw:1,0" /tmp/test-mic.wav

Cela renvoie la liste des formats disponibles, typiquement

    S16_LE
    S32_LE

On choisit par exemple `S32_LE` avec la fréquence d'échantillonnage des DVD :

    lafrier$  arecord -c2 -f S32_LE -r48000 -d 10 --device="hw:1,0" /tmp/test-mic.wav

## Mettre la carte interne par défaut

Si la carte son interne a le labe `PCH` :

    lafrier$  nano ~/.asoundrc
    nano:
      defaults.pcm.!card "PCH";
      defaults.ctl.!card "PCH";
    root#  rc-service alsasound restart

<!-- ====================================================================== -->

# camera

    root#  cd /usr/src/linux
    root#  make menuconfig
    menuconfig:
      Device Drivers  --->
        <*> Multimedia support  --->
          [*]   Cameras/video grabbers support
          [*]   Media USB Adapters  --->
            <*>   USB Video Class (UVC)  
	    [*]     UVC input events device support (NEW)
    root#  make
    root#  make modules_install
    root#  make install
    root#  grub-mkconfig -o /boot/grub/grub.cfg
    root#  reboot

Vérifier que `/dev/video0` existe.

<!-- ====================================================================== -->

# images

    root#  nano /etc/portage/make.conf
    nano:
      USE="${USE} jpeg"
    root#  emerge --ask --changed-use --deep @world





