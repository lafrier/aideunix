# NOM

vim

# CONFIGURATION

lafrier$  cp /etc/vim/vimrc /home/lafrier/Transfert-local/Informatique/Configuration/Vim/vimrc
lafrier$  ln -s /home/lafrier/Transfert-local/Informatique/Configuration/Vim/vimrc /home/lafrier/.vimrc
lafrier$  mkdir -p /home/lafrier/Transfert-local/Informatique/Configuration/Vim/vim/syntax/
lafrier$  ln -s /home/lafrier/Transfert-local/Informatique/Configuration/Vim/vim /home/lafrier/.vim
lafrier$  wget https://www.vim.org/scripts/download_script.php?src_id=13268 -O ~/.vim/syntax/noweb.vim
lafrier$  vim ~/.vimrc
vim>
  let noweb_language="c"
  let noweb_backend="tex"
  let noweb_fold_code=0
lafrier$  vim ~/.vim/filetype.vim
vim>
  if exists("did_load_filetypes")
      finish
  endif
  augroup filetypedetect
      au! BufRead,BufNewFile *.nw setfiletype noweb
  augroup END

Vérifier qu'il y a bien
  syntax on
dans ~/.vimrc



# TUTORIAL

vimtutor

# DEPLACEMENT CURSEUR

j : vers le bas

k : vers le haut

h : vers la gauche

l : vers la droite

w : d'un mot vers la droite

0 : début de ligne

$ : fin de ligne 

# EDITION

x : effacer un caractère

dw : effacer la fin d'un mot

# INSERTION

## Insérer un caractère

i : avant le caractère courant

a : après le caractère courant

## Insérer le contenu d'un fichier

:r nom_fichier

# COPIER-COLLER

## Sélection

v  puis les flèches

## Copie

y

## Coller après le curseur

p

## Coller avant le curseur

P

# SEARCH AND REPLACE

## Search and replace with confirmation

%s/ancien/nouveau/gc

# GESTION DES FICHIERS

| | |
| :- | :- |
| :q    | sortie
| :q!   | sortie sans sauvegarde
| :wq   | sortie avec sauvegarde
| :w    | sauvegarde

# MULTI-FENETRAGE

## Ouvrir un nouveau fichier verticalement

:vsp [nouveau_fichier]

## Passer d'une fenêtre à l'autre

CTRL w CTRL w

# MACROS

Tapper q suivi d'une lettre (par exemple "e" pour une equation latex).

Saisir un ensemble de commandes (par exemple begin et end equation, le label, etc).

Tapper q.

Pour executer ces commandes tapper ensuite @ suivi de la lettre (par exemple @e).

# MODE BLOC

Tapper `Ctrl v` pour entrer dans le mode, `Esc` pour en sortir.

Pour commenter un bloc de lignes, entrer dans le mode et sélectionner le bloc de lignes, puis tapper `I` et saisir le caractère de commentaire. Lorsqu'on sort avec `Esc` le caractère se mettra sur toutes les lignes du bloc.

# ENVIRONEMENT

## The terminal

Use gnome-terminal

# SPELL CHECKING

## To activate the spell checking

:set spell spelllang=fr

:set spell spelllang=en_gb

:set spell spelllang=en_us

:set nospell

## To move to a misspelled word

]s

[s

## For word correction

z=

