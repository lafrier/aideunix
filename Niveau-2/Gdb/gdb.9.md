# NAME

gdb

# EXEMPLES

    gdb -tui stardis

    list stardis-solid.c:1

    br 174

    run -V3 -M configtotal.txt -P 0,-0.0855,0,0 -n 1000 -e

    step

    next

    p solid_props->t0



# Vieux EXEMPLES avec cgdb

cgdb ./spheres

ESC pour remonter dans le code

/inconsistent pour search

ESPACE pour mettre un breakpoint à l'endoit du curser (numero de ligne en noir)

condition 2 *((int*)&path->x[1]) == -1128771092

disable 2
enable 2

continue

n

i pour retourner à (gdb)

print *path


run 1 geom.obj path.obj

up

print physical_properties_file
